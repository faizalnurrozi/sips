<?php 
session_start();
error_reporting(E_ALL ^ E_NOTICE);
$module = (isset($_REQUEST['module'])) ? $_REQUEST['module'] : '';
$component = (isset($_REQUEST['component'])) ? $_REQUEST['component'] : '';
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : '';
if($module==''){
	header("location:error.php");
}else{
	$_SESSION['path']="modules/".$module;
	if($component==''){
		$link="modules/".$module."/index.php";
		if(file_exists($link)) include $link; else header("location:error.php");
	}else{
		if($action==''){
			$link="modules/".$module."/components/".$component."/main.php";
			if(file_exists($link)){
				include $link;
			}else{
				header("location:error.php");
			}
		}else{
			$link="modules/".$module."/components/".$component."/".$action.".php";
			if(file_exists($link)){
				include $link;
			}else{
				header("location:error.php");
			}
		}
	}
}
?>
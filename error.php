<?php session_start(); ?>
<html>
	<head>
		<title>Error Page...</title>
		<link rel="icon" href="images/logo.png" type="image/x-icon" />
		<link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />
		<script type="text/javascript" src="includes/ajax.js"></script>
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/theme.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap-adds.css"></link>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="http-error">
					<h1>Oops!</h1>
					<p class="info">Halaman yang anda akses belum ada.</p>
					<p><image src="images/404.png" width="200"></p>
					<!--<p><i class="icon-home"></i></p>-->
					<!--<p><a href="#" onclick="javascript: window.location.href='content.php?<?php echo $_SESSION['s_adminPage']; ?>';">Kembali ke halaman utama</a></p>-->
				</div>
			</div>
		</div>
	</body>
</html>
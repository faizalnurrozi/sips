/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : akademik_db

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2018-07-16 23:04:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for _activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `_activity_logs`;
CREATE TABLE `_activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(30) NOT NULL,
  `field` varchar(30) NOT NULL,
  `id_trans` varchar(30) NOT NULL,
  `data_old` text,
  `data_new` text,
  `notes` text NOT NULL,
  `action` enum('CREATE','READ','UPDATE','DELETE') NOT NULL,
  `action_by` varchar(20) NOT NULL,
  `action_date` datetime NOT NULL,
  `action_ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _activity_logs
-- ----------------------------

-- ----------------------------
-- Table structure for _admin
-- ----------------------------
DROP TABLE IF EXISTS `_admin`;
CREATE TABLE `_admin` (
  `usernames` varchar(20) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `id_admin_group` varchar(10) NOT NULL,
  `passwords` varchar(100) NOT NULL,
  `superuser` enum('TRUE','FALSE') NOT NULL DEFAULT 'FALSE',
  `icon` varchar(100) NOT NULL,
  PRIMARY KEY (`usernames`,`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin
-- ----------------------------
INSERT INTO `_admin` VALUES ('123', '123', 'FO', 'caf1a3dfb505ffed0d024130f58c5cfa', 'FALSE', '');
INSERT INTO `_admin` VALUES ('admin', '1461505163', 'ADMIN', 'ee10c315eba2c75b403ea99136f5b48d', 'FALSE', '');

-- ----------------------------
-- Table structure for _admin_group
-- ----------------------------
DROP TABLE IF EXISTS `_admin_group`;
CREATE TABLE `_admin_group` (
  `id_admin_group` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id_admin_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin_group
-- ----------------------------
INSERT INTO `_admin_group` VALUES ('ADMIN', 'ADMINISTRATOR');

-- ----------------------------
-- Table structure for _admin_icon_home
-- ----------------------------
DROP TABLE IF EXISTS `_admin_icon_home`;
CREATE TABLE `_admin_icon_home` (
  `id_admin_icon_home` varchar(20) NOT NULL,
  `id_admin_menus` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `urutan` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_admin_icon_home`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin_icon_home
-- ----------------------------
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c340a8ce9', 'mnu-5448773a4c4b9', 'Mata Kuliah', 'modules/admin/icons/img592552fbc0c5f-icon-curriculum.png', '1');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c3428ca4b', 'mnu-54487863b34ac', 'Semester Ajaran', 'modules/admin/icons/img592553686d37f-icon-semester.png', '2');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c342b66e2', 'mnu-5448812baba99', 'Pendaftaran Mahasiswa Baru', 'modules/admin/icons/img57a812af72104-icon-score.png', '3');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c342f1536', 'mnu-54488161a4088', 'Hasil Test CBT', 'modules/admin/icons/img57a812c8dba4b-icon-score.png', '6');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c34329b55', 'mnu-5448818185841', 'Pendaftaran Mahasiswa Transfer', 'modules/admin/icons/img57a812dda01e6-icon-score.png', '5');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c3439a4cb', 'mnu-5448827631979', 'Konversi Mata Kuliah', 'modules/admin/icons/img5902d4d2a70a9-icon-konversi.png', '7');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c346c1c65', 'mnu-5448829df053e', 'Cetak Surat Keterangan Diterima (SKD)', 'modules/admin/icons/img57a81320c109e-icon-report.png', '7');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c347e6aa3', 'mnu-544882c94c4b8', 'Data Mahasiswa', 'modules/admin/icons/img57a8133660e7a-icon-mahasiswa.png', '8');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c348074f5', 'mnu-5448834790f5a', 'Data Dosen', 'modules/admin/icons/img57a8134692341-icon-dosen.png', '9');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c3489fc30', 'mnu-5448843f53ec9', 'Kinerja Dosen', 'modules/admin/icons/img57a813601744b-icon-schedule.png', '10');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c348c4108', 'mnu-54488478d1cf3', 'Kalender Akademik', 'modules/admin/icons/img57a81372bfbe0-icon-schedule.png', '11');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c348da74f', 'mnu-5448869df053b', 'Jadwal Kuliah', 'modules/admin/icons/img57a81383010cf-icon-schedule.png', '12');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c356335a8', 'mnu-54488870e8b2a', 'Input Kartu Rencana Studi (KRS)', 'modules/admin/icons/img57a8139ec01cc-icon-upload.png', '13');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c35658460', 'mnu-549eb7cad8577', 'Lihat Kartu Rencana Studi (KRS)', 'modules/admin/icons/img57a813b377674-icon-upload.png', '14');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c3567c320', 'mnu-5448cfdbc28d3', 'Input Penilaian', 'modules/admin/icons/img57a813c82db7c-icon-profile.png', '15');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c356ae5ff', 'mnu-5448ff2d66ff8', 'Input Presensi Mahasiswa', 'modules/admin/icons/img57a813daeaf1e-icon-profile.png', '16');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c356da57a', 'mnu-5448ff49501c3', 'Input Presensi Dosen', 'modules/admin/icons/img57a813ebb4bc6-icon-profile.png', '17');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c356f1b56', 'mnu-5448ff68ec833', 'Input Presensi Karyawan', 'modules/admin/icons/img57a813fc91d0a-icon-profile.png', '18');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c3574c9b9', 'mnu-5448d5e9ca2e4', 'Cetak KHS', 'modules/admin/icons/img57a81416f4158-editplus.png', '19');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c358070aa', 'mnu-5448d61acdfea', 'Cetak Transkrip Nilai', 'modules/admin/icons/img57a814262d525-editplus.png', '20');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-54c358297e5', 'mnu-5448d6a822556', 'Cetak Ijazah', 'modules/admin/icons/img57a81437b416a-editplus.png', '21');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-5902c34a158', 'mnu-54487718d59fd', 'Data Karyawan', 'modules/admin/icons/img5902c34a15f6e-icon-karyawan.png', '3');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-5902c3c0884', 'mnu-5813b09d9ceba', 'Pendaftaran Mahasiswa', 'modules/admin/icons/img5902c3c088a59-icon-registrasi.png', '4');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-5902d52d5cb', 'mnu-5813996d76f31', 'Test Kesehatan', 'modules/admin/icons/img5902d52d5d193-icon-kesehatan.png', '7');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-592551b3a47', 'mnu-5814103b13257', 'Presensi Test', 'modules/admin/icons/img592551b3a4e0b-icon-presensi.png', '5');
INSERT INTO `_admin_icon_home` VALUES ('iconmenu-5925521f4be', 'mnu-5822caafe30d3', 'Rekapitulasi Test', 'modules/admin/icons/img5925521f4c421-icon-rekapitulasi.png', '8');

-- ----------------------------
-- Table structure for _admin_logs
-- ----------------------------
DROP TABLE IF EXISTS `_admin_logs`;
CREATE TABLE `_admin_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `id_user` varchar(20) NOT NULL,
  `passwords` text NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `browser` varchar(200) NOT NULL,
  `status` enum('FAILED','SUCCESS') NOT NULL,
  `deskripsi` text NOT NULL,
  `country` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin_logs
-- ----------------------------

-- ----------------------------
-- Table structure for _admin_menus
-- ----------------------------
DROP TABLE IF EXISTS `_admin_menus`;
CREATE TABLE `_admin_menus` (
  `id_admin_menus` varchar(20) NOT NULL,
  `level` tinyint(3) NOT NULL,
  `id_admin_menus_parent` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `icon` varchar(50) NOT NULL,
  `urutan` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_admin_menus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin_menus
-- ----------------------------
INSERT INTO `_admin_menus` VALUES ('mnu-54441426c28cf', '2', 'mnu-544949d11ab43', 'Manajemen Menu', 'javascript: sendRequest(\\\'content.php?menu=mnu-54441426c28cf\\\', \\\'module=admin&component=admin_menus&add=false&sort=reset\\\', \\\'content\\\', \\\'div\\\');', '<i class=\\\"fa fa-folder-open-o\\\"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('mnu-54441588e8b29', '2', 'mnu-544949d11ab43', 'Group Admin', 'javascript: sendRequest(\'content.php?menu=mnu-54441588e8b29\', \'module=admin&component=admin_group&sort=reset\', \'content\', \'div\');', '<i class=\'fa fa-plug  \'></i>', '2');
INSERT INTO `_admin_menus` VALUES ('mnu-5448793b8d251', '0', '0', 'Pengaturan', 'javascript: void(null);', '<i class=\\\"fa fa-gears\\\"></i>', '7');
INSERT INTO `_admin_menus` VALUES ('mnu-544930c735687', '2', 'mnu-544949d11ab43', 'Icon Home', 'javascript: sendRequest(\\\'content.php?menu=mnu-544930c735687\\\', \\\'module=admin&component=admin_icon&add=false&sort=reset\\\', \\\'content\\\', \\\'div\\\');', '<i class=\\\'fa fa-home\\\'></i>', '3');
INSERT INTO `_admin_menus` VALUES ('mnu-544949d11ab43', '1', 'mnu-5448793b8d251', 'Manajemen Admin', 'javascript: void(null);', '<i class=\'fa fa-expeditedssl\'></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5AE9E0B444991', '1', '5AE9E067B0AFC', 'Jenis Bayar Umum', 'javascript: sendRequest(\'content.php?menu=5AE9E0B444991\', \'module=admin&component=jenis_bayar_umum&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-list\"></i>', '4');
INSERT INTO `_admin_menus` VALUES ('mnu-54494e3529f67', '2', 'mnu-54494b5d1e84d', 'Akses User', 'javascript: sendRequest(\\\'content.php?menu=mnu-54494e3529f67\\\', \\\'module=admin&component=mahasiswa_access&add=false&sort=reset\\\', \\\'content\\\', \\\'div\\\');', '<i class=\\\"fa fa-plug  \\\"></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5AE9E0CAA295F', '1', '5AE9E067B0AFC', 'Jenis Bayar Khusus', 'javascript: sendRequest(\'content.php?menu=5AE9E0CAA295F\', \'module=admin&component=jenis_bayar_khusus&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-list\"></i>', '5');
INSERT INTO `_admin_menus` VALUES ('5AE9E067B0AFC', '0', '0', 'Master Data', 'javascript: void(null);', '<i class=\"fa fa-list\"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('5AE9E07CC4149', '1', '5AE9E067B0AFC', 'Tingkat', 'javascript: sendRequest(\'content.php?menu=5AE9E07CC4149\', \'module=admin&component=tingkat&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-list\"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('5AE9E08B223FD', '1', '5AE9E067B0AFC', 'Kelas', 'javascript: sendRequest(\'content.php?menu=5AE9E08B223FD\', \'module=admin&component=kelas&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-list\"></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5AE9E09D77412', '1', '5AE9E067B0AFC', 'Siswa', 'javascript: sendRequest(\'content.php?menu=5AE9E09D77412\', \'module=admin&component=siswa&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-list\"></i>', '3');
INSERT INTO `_admin_menus` VALUES ('mnu-54a1f803a3133', '1', 'mnu-5448793b8d251', 'Pengaturan Umum', 'javascript: sendRequest(\\\'content.php?menu=mnu-54a1f803a3133\\\', \\\'module=admin&component=setting&add=true\\\', \\\'content\\\', \\\'div\\\');', '<i class=\\\"fa fa-gear\\\"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('5AEB026B4EFEA', '0', '0', 'Transaksi', 'javascript: void(null)', '<i class=\"fa fa-briefcase\"></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5AEB02842A2F8', '1', '5AEB026B4EFEA', 'Pembayaran', 'javascript: sendRequest(\'content.php?menu=5AEB02842A2F8\', \'module=admin&component=pembayaran&sort=reset&add=true\', \'content\', \'div\')', '<i class=\'fa fa-file-text \'></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5AF06EE3104B8', '1', '5AEB026B4EFEA', 'Jadwal Pembayaran', 'javascript: sendRequest(\'content.php?menu=5AF06EE3104B8\', \'module=admin&component=jadwal_bayar&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-calendar\"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('5AF83814887D9', '1', '5AEB026B4EFEA', 'Daftar Pembayaran', 'javascript: sendRequest(\'content.php?menu=5AF83814887D9\', \'module=admin&component=pembayaran&sort=reset&add=false\', \'content\', \'div\')', '<i class=\"fa fa-file-text \"></i>', '3');
INSERT INTO `_admin_menus` VALUES ('5B02F305DA98F', '0', '0', 'Laporan', 'javascript: void(null);', '<i class=\"fa fa-file-text \"></i>', '3');
INSERT INTO `_admin_menus` VALUES ('5B02F31980469', '1', '5B02F305DA98F', 'Pendapatan Per Kelas', 'javascript: sendRequest(\'content.php?menu=5B02F31980469\', \'module=admin&component=laporan_pendapatan&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-file-text \"></i>', '1');
INSERT INTO `_admin_menus` VALUES ('5B02F33B781F0', '1', '5B02F305DA98F', 'Tunggakan Siswa', 'javascript: sendRequest(\'content.php?menu=5B02F33B781F0\', \'module=admin&component=laporan_tunggakan_siswa&sort=reset\', \'content\', \'div\')', '<i class=\'fa fa-file-text \'></i>', '2');
INSERT INTO `_admin_menus` VALUES ('5B05A288938B1', '1', '5AE9E067B0AFC', 'Tahun Ajaran', 'javascript: sendRequest(\'content.php?menu=5B05A288938B1\', \'module=admin&component=semester_ajaran&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-bank\"></i>', '6');
INSERT INTO `_admin_menus` VALUES ('5B39A79221F9D', '1', '5AEB026B4EFEA', 'Naik Kelas', 'javascript: sendRequest(\'content.php?menu=5B39A79221F9D\', \'module=admin&component=naik_kelas&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-bank\"></i>', '4');
INSERT INTO `_admin_menus` VALUES ('5B4CB82FE3DE7', '1', '5B02F305DA98F', 'Pembayaran Siswa per Bulan', 'javascript: sendRequest(\'content.php?menu=5B4CB82FE3DE7\', \'module=admin&component=laporan_pembayaran_siswa&sort=reset\', \'content\', \'div\')', '<i class=\"fa fa-file-text \"></i>', '3');

-- ----------------------------
-- Table structure for _admin_menus_access
-- ----------------------------
DROP TABLE IF EXISTS `_admin_menus_access`;
CREATE TABLE `_admin_menus_access` (
  `id_admin_group` varchar(10) NOT NULL,
  `id_admin_menus` varchar(20) NOT NULL,
  `access` char(4) NOT NULL DEFAULT '1111'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _admin_menus_access
-- ----------------------------
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-54441588e8b29', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-54441426c28cf', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-544949d11ab43', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-5448793b8d251', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-54a1f803a3133', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B4CB82FE3DE7', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B02F33B781F0', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B02F31980469', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B02F305DA98F', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B39A79221F9D', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AF83814887D9', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AEB02842A2F8', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AF06EE3104B8', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AEB026B4EFEA', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5B05A288938B1', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E0CAA295F', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E0B444991', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E09D77412', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E08B223FD', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E07CC4149', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', '5AE9E067B0AFC', '1111');
INSERT INTO `_admin_menus_access` VALUES ('ADMIN', 'mnu-544930c735687', '1111');

-- ----------------------------
-- Table structure for _agama
-- ----------------------------
DROP TABLE IF EXISTS `_agama`;
CREATE TABLE `_agama` (
  `id_agama` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_agama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _agama
-- ----------------------------
INSERT INTO `_agama` VALUES ('1', 'Islam');
INSERT INTO `_agama` VALUES ('2', 'Kristen');
INSERT INTO `_agama` VALUES ('3', 'Katholik');
INSERT INTO `_agama` VALUES ('4', 'Hindu');
INSERT INTO `_agama` VALUES ('5', 'Budha');
INSERT INTO `_agama` VALUES ('6', 'Lain - lain');

-- ----------------------------
-- Table structure for _fa
-- ----------------------------
DROP TABLE IF EXISTS `_fa`;
CREATE TABLE `_fa` (
  `id_fa` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_fa`)
) ENGINE=MyISAM AUTO_INCREMENT=534 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _fa
-- ----------------------------
INSERT INTO `_fa` VALUES ('1', 'fa fa-check');
INSERT INTO `_fa` VALUES ('4', 'fa fa-adjust');
INSERT INTO `_fa` VALUES ('5', 'fa fa-anchor');
INSERT INTO `_fa` VALUES ('6', 'fa fa-archive');
INSERT INTO `_fa` VALUES ('7', 'fa fa-bank');
INSERT INTO `_fa` VALUES ('8', 'fa fa-binoculars ');
INSERT INTO `_fa` VALUES ('9', 'fa  fa-bus');
INSERT INTO `_fa` VALUES ('10', 'fa fa-calendar');
INSERT INTO `_fa` VALUES ('11', 'fa fa-cart-arrow-down');
INSERT INTO `_fa` VALUES ('12', ' fa fa-cab');
INSERT INTO `_fa` VALUES ('13', 'fa fa-trash');
INSERT INTO `_fa` VALUES ('14', ' fa fa-send');
INSERT INTO `_fa` VALUES ('15', 'fa fa-user');
INSERT INTO `_fa` VALUES ('421', 'fa fa-sitemap');
INSERT INTO `_fa` VALUES ('17', 'fa fa-briefcase');
INSERT INTO `_fa` VALUES ('18', '  fa fa-android');
INSERT INTO `_fa` VALUES ('19', 'fa fa-cart-plus ');
INSERT INTO `_fa` VALUES ('20', 'fa fa-cc-mastercard ');
INSERT INTO `_fa` VALUES ('21', 'fa fa-cc-paypal ');
INSERT INTO `_fa` VALUES ('22', 'fa fa-cc-stripe');
INSERT INTO `_fa` VALUES ('23', 'fa fa-cc-visa ');
INSERT INTO `_fa` VALUES ('24', 'fa fa-certificate ');
INSERT INTO `_fa` VALUES ('25', 'fa fa-chain');
INSERT INTO `_fa` VALUES ('26', 'fa fa-chain-broken');
INSERT INTO `_fa` VALUES ('27', 'fa fa-check ');
INSERT INTO `_fa` VALUES ('28', 'fa fa-check-circle  ');
INSERT INTO `_fa` VALUES ('29', 'fa fa-check-circle-o  ');
INSERT INTO `_fa` VALUES ('30', 'fa fa-check-square ');
INSERT INTO `_fa` VALUES ('31', 'fa fa-check-square-o ');
INSERT INTO `_fa` VALUES ('32', 'fa fa-chevron-circle-down');
INSERT INTO `_fa` VALUES ('33', 'fa fa-chevron-circle-left ');
INSERT INTO `_fa` VALUES ('34', ' fa fa-chevron-circle-right ');
INSERT INTO `_fa` VALUES ('35', 'fa fa-chevron-circle-up  ');
INSERT INTO `_fa` VALUES ('36', 'fa fa-chevron-down');
INSERT INTO `_fa` VALUES ('37', ' fa fa-chevron-left');
INSERT INTO `_fa` VALUES ('38', 'fa fa-chevron-right ');
INSERT INTO `_fa` VALUES ('39', 'fa fa-chevron-up ');
INSERT INTO `_fa` VALUES ('40', 'fa fa-child ');
INSERT INTO `_fa` VALUES ('41', 'fa fa-chrome');
INSERT INTO `_fa` VALUES ('42', 'fa fa-circle ');
INSERT INTO `_fa` VALUES ('43', 'fa fa-circle-o');
INSERT INTO `_fa` VALUES ('44', 'fa fa-circle-o-notch');
INSERT INTO `_fa` VALUES ('45', 'fa fa-circle-thin ');
INSERT INTO `_fa` VALUES ('46', 'fa fa-clipboard  ');
INSERT INTO `_fa` VALUES ('47', 'fa fa-clock-o ');
INSERT INTO `_fa` VALUES ('48', 'fa fa-clone  ');
INSERT INTO `_fa` VALUES ('49', 'fa fa-close ');
INSERT INTO `_fa` VALUES ('50', 'fa fa-cloud  ');
INSERT INTO `_fa` VALUES ('51', 'fa fa-cloud-download ');
INSERT INTO `_fa` VALUES ('52', 'fa fa-cloud-upload ');
INSERT INTO `_fa` VALUES ('53', 'fa fa-cny ');
INSERT INTO `_fa` VALUES ('54', 'fa fa-code');
INSERT INTO `_fa` VALUES ('55', 'fa fa-code-fork');
INSERT INTO `_fa` VALUES ('56', 'fa fa-codepen');
INSERT INTO `_fa` VALUES ('74', 'fa fa-creative-commons');
INSERT INTO `_fa` VALUES ('58', 'fa fa-coffee');
INSERT INTO `_fa` VALUES ('59', 'fa fa-cog ');
INSERT INTO `_fa` VALUES ('60', 'fa fa-cogs');
INSERT INTO `_fa` VALUES ('61', 'fa fa-columns');
INSERT INTO `_fa` VALUES ('62', 'fa fa-comment ');
INSERT INTO `_fa` VALUES ('63', 'fa fa-comment-o');
INSERT INTO `_fa` VALUES ('64', 'fa fa-commenting ');
INSERT INTO `_fa` VALUES ('65', 'fa fa-commenting-o ');
INSERT INTO `_fa` VALUES ('66', 'fa fa-comments ');
INSERT INTO `_fa` VALUES ('67', 'fa fa-comments-o ');
INSERT INTO `_fa` VALUES ('68', 'fa fa-compass ');
INSERT INTO `_fa` VALUES ('69', 'fa fa-compress');
INSERT INTO `_fa` VALUES ('70', ' fa fa-connectdevelop ');
INSERT INTO `_fa` VALUES ('71', 'fa fa-contao ');
INSERT INTO `_fa` VALUES ('72', 'fa fa-copy ');
INSERT INTO `_fa` VALUES ('73', 'fa fa-copyright');
INSERT INTO `_fa` VALUES ('76', 'fa fa-crop');
INSERT INTO `_fa` VALUES ('77', 'fa fa-crosshairs ');
INSERT INTO `_fa` VALUES ('78', 'fa fa-css3 ');
INSERT INTO `_fa` VALUES ('79', 'fa fa-cube');
INSERT INTO `_fa` VALUES ('80', 'fa fa-cubes ');
INSERT INTO `_fa` VALUES ('81', 'fa fa-cut ');
INSERT INTO `_fa` VALUES ('82', 'fa fa-cutlery');
INSERT INTO `_fa` VALUES ('83', 'fa fa-dashboard ');
INSERT INTO `_fa` VALUES ('84', 'fa fa-dashcube ');
INSERT INTO `_fa` VALUES ('85', 'fa fa-database ');
INSERT INTO `_fa` VALUES ('86', 'fa fa-dedent');
INSERT INTO `_fa` VALUES ('87', 'fa fa-delicious ');
INSERT INTO `_fa` VALUES ('88', 'fa fa-desktop ');
INSERT INTO `_fa` VALUES ('89', 'fa fa-deviantart ');
INSERT INTO `_fa` VALUES ('90', 'fa fa-diamond ');
INSERT INTO `_fa` VALUES ('91', 'fa fa-digg ');
INSERT INTO `_fa` VALUES ('92', 'fa fa-dollar');
INSERT INTO `_fa` VALUES ('93', 'fa fa-leaf');
INSERT INTO `_fa` VALUES ('94', 'fa fa-leanpub  ');
INSERT INTO `_fa` VALUES ('95', 'fa fa-legal ');
INSERT INTO `_fa` VALUES ('96', 'fa fa-lemon-o ');
INSERT INTO `_fa` VALUES ('97', 'fa fa-level-down ');
INSERT INTO `_fa` VALUES ('98', 'fa fa-level-up');
INSERT INTO `_fa` VALUES ('99', 'fa fa-life-bouy ');
INSERT INTO `_fa` VALUES ('100', 'fa fa-life-buoy ');
INSERT INTO `_fa` VALUES ('101', 'fa fa-life-ring ');
INSERT INTO `_fa` VALUES ('102', 'fa fa-life-saver ');
INSERT INTO `_fa` VALUES ('103', 'fa fa-lightbulb-o');
INSERT INTO `_fa` VALUES ('104', 'fa fa-line-chart ');
INSERT INTO `_fa` VALUES ('105', 'fa fa-link');
INSERT INTO `_fa` VALUES ('106', 'fa fa-linkedin ');
INSERT INTO `_fa` VALUES ('107', 'fa fa-linkedin-square ');
INSERT INTO `_fa` VALUES ('108', 'fa fa-linux ');
INSERT INTO `_fa` VALUES ('109', 'fa fa-list');
INSERT INTO `_fa` VALUES ('110', 'fa fa-list-alt');
INSERT INTO `_fa` VALUES ('142', 'fa fa-microphone ');
INSERT INTO `_fa` VALUES ('112', 'fa fa-list-ol ');
INSERT INTO `_fa` VALUES ('113', 'fa fa-list-ul ');
INSERT INTO `_fa` VALUES ('114', 'fa  fa-location-arrow ');
INSERT INTO `_fa` VALUES ('115', 'fa fa-lock');
INSERT INTO `_fa` VALUES ('116', 'fa fa-long-arrow-down');
INSERT INTO `_fa` VALUES ('117', 'fa fa-long-arrow-left ');
INSERT INTO `_fa` VALUES ('118', 'fa fa-long-arrow-right ');
INSERT INTO `_fa` VALUES ('119', 'fa fa-long-arrow-up ');
INSERT INTO `_fa` VALUES ('120', 'fa fa-magic ');
INSERT INTO `_fa` VALUES ('121', 'fa fa-magnet ');
INSERT INTO `_fa` VALUES ('122', 'fa fa-mail-forward ');
INSERT INTO `_fa` VALUES ('123', 'fa fa-mail-reply');
INSERT INTO `_fa` VALUES ('124', 'fa fa-mail-reply-all ');
INSERT INTO `_fa` VALUES ('125', 'fa fa-male');
INSERT INTO `_fa` VALUES ('126', 'fa fa-map ');
INSERT INTO `_fa` VALUES ('127', 'fa fa-map-marker');
INSERT INTO `_fa` VALUES ('128', 'fa fa-map-o');
INSERT INTO `_fa` VALUES ('129', 'fa fa-map-pin ');
INSERT INTO `_fa` VALUES ('130', 'fa fa-map-signs');
INSERT INTO `_fa` VALUES ('131', 'fa fa-mars');
INSERT INTO `_fa` VALUES ('132', 'fa fa-mars-double');
INSERT INTO `_fa` VALUES ('133', 'fa fa-mars-stroke');
INSERT INTO `_fa` VALUES ('134', 'fa fa-mars-stroke-h');
INSERT INTO `_fa` VALUES ('135', 'fa fa-mars-stroke-v');
INSERT INTO `_fa` VALUES ('136', 'fa fa-maxcdn');
INSERT INTO `_fa` VALUES ('137', 'fa fa-meanpath');
INSERT INTO `_fa` VALUES ('138', 'fa fa-medium');
INSERT INTO `_fa` VALUES ('139', 'fa fa-medkit ');
INSERT INTO `_fa` VALUES ('140', 'fa fa-meh-o ');
INSERT INTO `_fa` VALUES ('141', 'fa fa-mercury');
INSERT INTO `_fa` VALUES ('143', 'fa fa-microphone-slash ');
INSERT INTO `_fa` VALUES ('144', 'fa fa-minus ');
INSERT INTO `_fa` VALUES ('145', 'fa fa-minus-circle  ');
INSERT INTO `_fa` VALUES ('146', 'fa fa-minus-square');
INSERT INTO `_fa` VALUES ('150', 'fa fa-mortar-board');
INSERT INTO `_fa` VALUES ('148', 'fa fa-mobile');
INSERT INTO `_fa` VALUES ('151', 'fa fa-motorcycle');
INSERT INTO `_fa` VALUES ('152', 'fa fa-mouse-pointer');
INSERT INTO `_fa` VALUES ('153', 'fa ï€ fa-music ');
INSERT INTO `_fa` VALUES ('154', 'fa fa-navicon');
INSERT INTO `_fa` VALUES ('155', 'fa fa-neuter');
INSERT INTO `_fa` VALUES ('156', 'fa fa-newspaper-o ');
INSERT INTO `_fa` VALUES ('157', 'fa fa-object-group ');
INSERT INTO `_fa` VALUES ('158', 'fa fa-object-ungroup');
INSERT INTO `_fa` VALUES ('159', ' fa fa-odnoklassniki');
INSERT INTO `_fa` VALUES ('160', 'fa fa-odnoklassniki-square');
INSERT INTO `_fa` VALUES ('161', 'fa fa-opencart');
INSERT INTO `_fa` VALUES ('162', 'fa fa-openid ');
INSERT INTO `_fa` VALUES ('163', 'fa fa-opera');
INSERT INTO `_fa` VALUES ('164', 'fa fa-optin-monster ');
INSERT INTO `_fa` VALUES ('165', 'fa fa-outdent');
INSERT INTO `_fa` VALUES ('166', 'fa fa-pagelines ');
INSERT INTO `_fa` VALUES ('167', 'fa fa-paint-brush');
INSERT INTO `_fa` VALUES ('168', 'fa fa-paper-plane ');
INSERT INTO `_fa` VALUES ('169', 'fa fa-paper-plane-o  ');
INSERT INTO `_fa` VALUES ('170', 'fa fa-paperclip ');
INSERT INTO `_fa` VALUES ('171', 'fa fa-paragraph');
INSERT INTO `_fa` VALUES ('172', 'fa fa-paste');
INSERT INTO `_fa` VALUES ('176', 'fa  fa-dot-circle-o');
INSERT INTO `_fa` VALUES ('174', 'fa fa-paw');
INSERT INTO `_fa` VALUES ('175', 'fa fa-paypal');
INSERT INTO `_fa` VALUES ('177', 'fa fa-download');
INSERT INTO `_fa` VALUES ('178', 'fa fa-dribbble');
INSERT INTO `_fa` VALUES ('179', 'fa fa-dropbox');
INSERT INTO `_fa` VALUES ('180', 'fa fa-drupal');
INSERT INTO `_fa` VALUES ('341', 'fa fa-pencil ');
INSERT INTO `_fa` VALUES ('182', 'fa fa-edit');
INSERT INTO `_fa` VALUES ('183', 'fa fa-eject');
INSERT INTO `_fa` VALUES ('184', 'fa fa-ellipsis-h');
INSERT INTO `_fa` VALUES ('185', 'fa fa-ellipsis-v');
INSERT INTO `_fa` VALUES ('186', 'fa fa-empire');
INSERT INTO `_fa` VALUES ('187', 'fa fa-envelope');
INSERT INTO `_fa` VALUES ('188', 'fa fa-envelope-o');
INSERT INTO `_fa` VALUES ('189', 'fa fa-envelope-square');
INSERT INTO `_fa` VALUES ('190', 'fa fa-eraser');
INSERT INTO `_fa` VALUES ('191', 'fa fa-eur');
INSERT INTO `_fa` VALUES ('192', 'fa fa-euro');
INSERT INTO `_fa` VALUES ('193', 'fa fa-exchange ');
INSERT INTO `_fa` VALUES ('194', 'fa fa-exclamation');
INSERT INTO `_fa` VALUES ('195', 'fa fa-exclamation-circle');
INSERT INTO `_fa` VALUES ('196', 'fa fa-exclamation-triangle ');
INSERT INTO `_fa` VALUES ('197', 'fa fa-expand');
INSERT INTO `_fa` VALUES ('198', 'fa fa-expeditedssl');
INSERT INTO `_fa` VALUES ('199', 'fa fa-external-link');
INSERT INTO `_fa` VALUES ('200', 'fa fa-external-link-square');
INSERT INTO `_fa` VALUES ('201', 'fa fa-eye');
INSERT INTO `_fa` VALUES ('202', 'fa fa-eye-slash');
INSERT INTO `_fa` VALUES ('203', 'fa fa-eyedropper');
INSERT INTO `_fa` VALUES ('204', 'fa fa-facebook');
INSERT INTO `_fa` VALUES ('205', 'fa fa-facebook-official');
INSERT INTO `_fa` VALUES ('206', 'fa fa-facebook-square');
INSERT INTO `_fa` VALUES ('207', 'fa fa-fast-backward');
INSERT INTO `_fa` VALUES ('208', 'fa fa-fast-forward');
INSERT INTO `_fa` VALUES ('209', 'fa fa-fax');
INSERT INTO `_fa` VALUES ('210', 'fa fa-feed');
INSERT INTO `_fa` VALUES ('211', 'fa fa-female');
INSERT INTO `_fa` VALUES ('212', 'fa fa-fighter-jet');
INSERT INTO `_fa` VALUES ('213', 'fa fa-file');
INSERT INTO `_fa` VALUES ('214', 'fa fa-file-archive-o');
INSERT INTO `_fa` VALUES ('215', 'fa fa-file-audio-o');
INSERT INTO `_fa` VALUES ('216', 'fa fa-file-code-o');
INSERT INTO `_fa` VALUES ('217', 'fa fa-file-excel-o');
INSERT INTO `_fa` VALUES ('218', 'fa fa-file-image-o');
INSERT INTO `_fa` VALUES ('219', 'fa fa-file-movie-o');
INSERT INTO `_fa` VALUES ('220', 'fa fa-file-o');
INSERT INTO `_fa` VALUES ('221', 'fa fa-file-pdf-o');
INSERT INTO `_fa` VALUES ('222', 'fa fa-file-photo-o');
INSERT INTO `_fa` VALUES ('223', 'fa fa-file-picture-o');
INSERT INTO `_fa` VALUES ('224', 'fa  fa-file-powerpoint-o');
INSERT INTO `_fa` VALUES ('225', 'fa fa-file-sound-o');
INSERT INTO `_fa` VALUES ('226', 'fa fa-file-text ');
INSERT INTO `_fa` VALUES ('227', 'fa fa-file-text-o');
INSERT INTO `_fa` VALUES ('228', 'fa fa-file-video-o ');
INSERT INTO `_fa` VALUES ('229', 'fa fa-file-word-o');
INSERT INTO `_fa` VALUES ('230', 'fa fa-file-zip-o');
INSERT INTO `_fa` VALUES ('231', 'fa fa-files-o');
INSERT INTO `_fa` VALUES ('232', 'fa fa-film');
INSERT INTO `_fa` VALUES ('233', 'fa fa-filter ');
INSERT INTO `_fa` VALUES ('234', 'fa fa-fire');
INSERT INTO `_fa` VALUES ('235', 'fa fa-fire-extinguisher');
INSERT INTO `_fa` VALUES ('236', 'fa fa-firefox');
INSERT INTO `_fa` VALUES ('237', 'fa fa-flag ');
INSERT INTO `_fa` VALUES ('238', 'fa fa-flag-checkered');
INSERT INTO `_fa` VALUES ('239', 'fa fa-flag-o');
INSERT INTO `_fa` VALUES ('240', 'fa fa-flash');
INSERT INTO `_fa` VALUES ('241', 'fa fa-flask');
INSERT INTO `_fa` VALUES ('242', 'fa fa-flickr');
INSERT INTO `_fa` VALUES ('243', 'fa fa-floppy-o');
INSERT INTO `_fa` VALUES ('244', 'fa fa-folder');
INSERT INTO `_fa` VALUES ('245', 'fa fa-folder-o');
INSERT INTO `_fa` VALUES ('246', 'fa fa-folder-open');
INSERT INTO `_fa` VALUES ('247', 'fa fa-folder-open-o');
INSERT INTO `_fa` VALUES ('248', 'fa fa-font');
INSERT INTO `_fa` VALUES ('249', 'fa fa-fonticons ');
INSERT INTO `_fa` VALUES ('251', 'fa fa-forumbee');
INSERT INTO `_fa` VALUES ('252', 'fa fa-forward');
INSERT INTO `_fa` VALUES ('253', 'fa fa-foursquare');
INSERT INTO `_fa` VALUES ('254', 'fa fa-frown-o');
INSERT INTO `_fa` VALUES ('255', 'fa fa-futbol-o');
INSERT INTO `_fa` VALUES ('256', 'fa fa-gamepad');
INSERT INTO `_fa` VALUES ('257', 'fa fa-gavel');
INSERT INTO `_fa` VALUES ('258', 'fa fa-gbp ');
INSERT INTO `_fa` VALUES ('259', 'fa fa-ge ');
INSERT INTO `_fa` VALUES ('260', 'fa fa-gear');
INSERT INTO `_fa` VALUES ('261', 'fa fa-gears');
INSERT INTO `_fa` VALUES ('262', 'fa fa-genderless');
INSERT INTO `_fa` VALUES ('263', 'fa fa-get-pocket');
INSERT INTO `_fa` VALUES ('264', 'fa fa-gg ');
INSERT INTO `_fa` VALUES ('265', 'fa fa-gg-circle');
INSERT INTO `_fa` VALUES ('266', 'fa  fa-gift ');
INSERT INTO `_fa` VALUES ('267', 'fa fa-git ');
INSERT INTO `_fa` VALUES ('268', 'fa fa-git-square');
INSERT INTO `_fa` VALUES ('269', 'fa fa-github');
INSERT INTO `_fa` VALUES ('270', 'fa fa-github-alt ');
INSERT INTO `_fa` VALUES ('271', 'fa fa-github-square');
INSERT INTO `_fa` VALUES ('272', 'fa fa-gittip');
INSERT INTO `_fa` VALUES ('273', 'fa fa-glass');
INSERT INTO `_fa` VALUES ('274', 'fa fa-globe');
INSERT INTO `_fa` VALUES ('275', 'fa fa-google');
INSERT INTO `_fa` VALUES ('276', 'fa fa-google-plus');
INSERT INTO `_fa` VALUES ('277', 'fa fa-google-plus-square');
INSERT INTO `_fa` VALUES ('278', 'fa fa-google-wallet');
INSERT INTO `_fa` VALUES ('279', 'fa fa-graduation-cap');
INSERT INTO `_fa` VALUES ('280', 'fa fa-gratipay ');
INSERT INTO `_fa` VALUES ('281', 'fa fa-group');
INSERT INTO `_fa` VALUES ('282', 'fa fa-h-square');
INSERT INTO `_fa` VALUES ('283', 'fa fa-hacker-news ');
INSERT INTO `_fa` VALUES ('284', 'fa fa-hand-grab-o');
INSERT INTO `_fa` VALUES ('285', 'fa fa-hand-lizard-o');
INSERT INTO `_fa` VALUES ('286', 'fa fa-hand-o-down');
INSERT INTO `_fa` VALUES ('287', 'fa fa-hand-o-left');
INSERT INTO `_fa` VALUES ('288', 'fa fa-hand-o-right');
INSERT INTO `_fa` VALUES ('289', 'fa fa-hand-o-up');
INSERT INTO `_fa` VALUES ('290', 'fa fa-hand-paper-o');
INSERT INTO `_fa` VALUES ('291', 'fa fa-hand-peace-o');
INSERT INTO `_fa` VALUES ('292', 'fa fa-hand-pointer-o');
INSERT INTO `_fa` VALUES ('293', 'fa fa-hand-rock-o');
INSERT INTO `_fa` VALUES ('294', 'fa fa-hand-scissors-o');
INSERT INTO `_fa` VALUES ('295', 'fa fa-hand-spock-o');
INSERT INTO `_fa` VALUES ('296', 'fa fa-hand-stop-o');
INSERT INTO `_fa` VALUES ('342', 'fa fa-pencil-square ');
INSERT INTO `_fa` VALUES ('298', 'fa fa-hdd-o');
INSERT INTO `_fa` VALUES ('299', 'fa fa-header ');
INSERT INTO `_fa` VALUES ('300', 'fa fa-headphones');
INSERT INTO `_fa` VALUES ('301', 'fa fa-heart');
INSERT INTO `_fa` VALUES ('302', 'fa fa-heart-o');
INSERT INTO `_fa` VALUES ('303', 'fa fa-heartbeat');
INSERT INTO `_fa` VALUES ('304', 'fa fa-history');
INSERT INTO `_fa` VALUES ('305', 'fa fa-home');
INSERT INTO `_fa` VALUES ('306', 'fa fa-hospital-o');
INSERT INTO `_fa` VALUES ('307', 'fa fa-hotel');
INSERT INTO `_fa` VALUES ('308', 'fa fa-hourglass');
INSERT INTO `_fa` VALUES ('309', 'fa fa-hourglass-1');
INSERT INTO `_fa` VALUES ('310', 'fa fa-hourglass-2');
INSERT INTO `_fa` VALUES ('311', 'fa fa-hourglass-3 ');
INSERT INTO `_fa` VALUES ('312', 'fa fa-hourglass-end ');
INSERT INTO `_fa` VALUES ('313', 'fa fa-hourglass-half ');
INSERT INTO `_fa` VALUES ('314', 'fa fa-hourglass-o');
INSERT INTO `_fa` VALUES ('315', 'fa fa-hourglass-start');
INSERT INTO `_fa` VALUES ('316', 'fa fa-houzz');
INSERT INTO `_fa` VALUES ('317', 'fa fa-html5 ');
INSERT INTO `_fa` VALUES ('318', 'fa fa-i-cursor ');
INSERT INTO `_fa` VALUES ('319', 'fa fa-image');
INSERT INTO `_fa` VALUES ('320', 'fa fa-inbox ');
INSERT INTO `_fa` VALUES ('321', 'fa fa-indent ');
INSERT INTO `_fa` VALUES ('322', 'fa fa-industry');
INSERT INTO `_fa` VALUES ('323', 'fa fa-info');
INSERT INTO `_fa` VALUES ('324', 'fa fa-info-circle');
INSERT INTO `_fa` VALUES ('325', 'fa fa-instagram');
INSERT INTO `_fa` VALUES ('326', 'fa fa-institution');
INSERT INTO `_fa` VALUES ('327', 'fa fa-internet-explorer');
INSERT INTO `_fa` VALUES ('328', 'fa fa-intersex ');
INSERT INTO `_fa` VALUES ('329', 'fa fa-ioxhost');
INSERT INTO `_fa` VALUES ('330', 'fa fa-italic');
INSERT INTO `_fa` VALUES ('331', 'fa fa-joomla');
INSERT INTO `_fa` VALUES ('332', 'fa fa-jpy');
INSERT INTO `_fa` VALUES ('333', 'fa fa-jsfiddle');
INSERT INTO `_fa` VALUES ('334', 'fa fa-key');
INSERT INTO `_fa` VALUES ('335', 'fa fa-keyboard-o');
INSERT INTO `_fa` VALUES ('336', 'fa fa-krw');
INSERT INTO `_fa` VALUES ('337', 'fa fa-language');
INSERT INTO `_fa` VALUES ('338', 'fa fa-laptop');
INSERT INTO `_fa` VALUES ('339', 'fa fa-lastfm');
INSERT INTO `_fa` VALUES ('340', 'fa fa-lastfm-square ');
INSERT INTO `_fa` VALUES ('344', 'fa fa-phone');
INSERT INTO `_fa` VALUES ('345', ' fa fa-phone-square ');
INSERT INTO `_fa` VALUES ('346', 'fa fa-photo');
INSERT INTO `_fa` VALUES ('347', 'fa fa-picture-o');
INSERT INTO `_fa` VALUES ('348', 'fa fa-pie-chart ');
INSERT INTO `_fa` VALUES ('349', 'fa fa-pied-piper fa fa-pied-piper-alt ');
INSERT INTO `_fa` VALUES ('350', 'fa fa-pinterest');
INSERT INTO `_fa` VALUES ('351', 'fa fa-pinterest-p ');
INSERT INTO `_fa` VALUES ('352', 'fa fa-pinterest-square ');
INSERT INTO `_fa` VALUES ('353', 'fa fa-plane ');
INSERT INTO `_fa` VALUES ('354', 'fa fa-play ');
INSERT INTO `_fa` VALUES ('355', 'fa fa-play-circle');
INSERT INTO `_fa` VALUES ('356', 'fa fa-play-circle-o ');
INSERT INTO `_fa` VALUES ('357', 'fa fa-plug  ');
INSERT INTO `_fa` VALUES ('358', 'fa fa-plus');
INSERT INTO `_fa` VALUES ('359', 'fa fa-plus-circle');
INSERT INTO `_fa` VALUES ('360', 'fa fa-plus-square ');
INSERT INTO `_fa` VALUES ('361', 'fa fa-plus-square-o');
INSERT INTO `_fa` VALUES ('362', 'fa fa-power-off');
INSERT INTO `_fa` VALUES ('364', 'fa fa-puzzle-piece');
INSERT INTO `_fa` VALUES ('365', 'fa fa-qq');
INSERT INTO `_fa` VALUES ('366', 'fa fa-qrcode');
INSERT INTO `_fa` VALUES ('367', 'fa fa-question');
INSERT INTO `_fa` VALUES ('368', 'fa fa-question-circle');
INSERT INTO `_fa` VALUES ('369', 'fa fa-quote-left ');
INSERT INTO `_fa` VALUES ('370', 'fa fa-quote-right ');
INSERT INTO `_fa` VALUES ('371', 'fa fa-ra ');
INSERT INTO `_fa` VALUES ('372', 'fa fa-random');
INSERT INTO `_fa` VALUES ('373', 'fa fa-rebel ');
INSERT INTO `_fa` VALUES ('374', 'fa fa-recycle ');
INSERT INTO `_fa` VALUES ('375', 'fa fa-reddit-square ');
INSERT INTO `_fa` VALUES ('376', 'fa fa-refresh ');
INSERT INTO `_fa` VALUES ('377', 'fa fa-registered');
INSERT INTO `_fa` VALUES ('378', 'fa fa-remove ');
INSERT INTO `_fa` VALUES ('379', 'fa fa-renren ');
INSERT INTO `_fa` VALUES ('380', 'fa fa-reorder');
INSERT INTO `_fa` VALUES ('381', ' fa fa-repeat');
INSERT INTO `_fa` VALUES ('382', 'fa fa-reply ');
INSERT INTO `_fa` VALUES ('383', 'fa fa-reply-all');
INSERT INTO `_fa` VALUES ('384', 'fa fa-retweet');
INSERT INTO `_fa` VALUES ('385', 'fa fa-rmb ');
INSERT INTO `_fa` VALUES ('386', 'fa fa-road ');
INSERT INTO `_fa` VALUES ('387', 'fa fa-rocket');
INSERT INTO `_fa` VALUES ('388', 'fa fa-rotate-left ');
INSERT INTO `_fa` VALUES ('389', 'fa fa-rotate-right ');
INSERT INTO `_fa` VALUES ('390', 'fa fa-rouble');
INSERT INTO `_fa` VALUES ('391', 'fa fa-rss ');
INSERT INTO `_fa` VALUES ('392', 'fa fa-rss-square');
INSERT INTO `_fa` VALUES ('393', 'fa fa-rub');
INSERT INTO `_fa` VALUES ('394', 'fa fa-ruble ');
INSERT INTO `_fa` VALUES ('395', 'fa fa-rupee');
INSERT INTO `_fa` VALUES ('396', 'fa fa-safari');
INSERT INTO `_fa` VALUES ('397', 'fa fa-save');
INSERT INTO `_fa` VALUES ('422', 'fa fa-skyatlas');
INSERT INTO `_fa` VALUES ('399', 'fa ï€‚ fa-search ');
INSERT INTO `_fa` VALUES ('400', 'fa fa-search-minus');
INSERT INTO `_fa` VALUES ('401', 'fa fa-search-plus');
INSERT INTO `_fa` VALUES ('402', 'fa fa-sellsy ');
INSERT INTO `_fa` VALUES ('403', 'fa fa-send ');
INSERT INTO `_fa` VALUES ('404', 'fa fa-send-o');
INSERT INTO `_fa` VALUES ('405', 'fa fa-server');
INSERT INTO `_fa` VALUES ('406', 'fa fa-share');
INSERT INTO `_fa` VALUES ('407', 'fa fa-share-alt');
INSERT INTO `_fa` VALUES ('408', 'fa fa-share-alt-square ');
INSERT INTO `_fa` VALUES ('409', 'fa fa-share-square');
INSERT INTO `_fa` VALUES ('410', 'fa fa-share-square-o ');
INSERT INTO `_fa` VALUES ('411', 'fa fa-shekel');
INSERT INTO `_fa` VALUES ('412', 'fa fa-sheqel  ');
INSERT INTO `_fa` VALUES ('413', 'fa fa-shield ');
INSERT INTO `_fa` VALUES ('414', 'fa fa-ship');
INSERT INTO `_fa` VALUES ('416', 'fa fa-shopping-cart ');
INSERT INTO `_fa` VALUES ('417', 'fa fa-sign-in');
INSERT INTO `_fa` VALUES ('418', 'fa fa-sign-out');
INSERT INTO `_fa` VALUES ('419', 'fa fa-signal ');
INSERT INTO `_fa` VALUES ('420', 'fa fa-simplybuilt');
INSERT INTO `_fa` VALUES ('423', 'fa fa-skype');
INSERT INTO `_fa` VALUES ('424', 'fa fa-slack');
INSERT INTO `_fa` VALUES ('425', 'fa fa-sliders');
INSERT INTO `_fa` VALUES ('426', 'fa fa-slideshare');
INSERT INTO `_fa` VALUES ('427', 'fa fa-smile-o');
INSERT INTO `_fa` VALUES ('428', 'fa fa-soccer-ball-o');
INSERT INTO `_fa` VALUES ('429', 'fa fa-sort');
INSERT INTO `_fa` VALUES ('430', 'fa fa-sort-alpha-asc ');
INSERT INTO `_fa` VALUES ('431', 'fa fa-sort-alpha-desc');
INSERT INTO `_fa` VALUES ('432', 'fa fa-sort-amount-asc');
INSERT INTO `_fa` VALUES ('433', 'fa fa-sort-amount-desc');
INSERT INTO `_fa` VALUES ('434', 'fa fa-sort-asc');
INSERT INTO `_fa` VALUES ('435', 'fa fa-sort-desc');
INSERT INTO `_fa` VALUES ('436', 'fa fa-sort-down');
INSERT INTO `_fa` VALUES ('437', 'fa fa-sort-numeric-asc');
INSERT INTO `_fa` VALUES ('438', 'fa fa-sort-numeric-desc');
INSERT INTO `_fa` VALUES ('439', 'fa fa-sort-up');
INSERT INTO `_fa` VALUES ('440', 'fa fa-soundcloud');
INSERT INTO `_fa` VALUES ('441', 'fa fa-space-shuttle');
INSERT INTO `_fa` VALUES ('442', 'fa fa-spinner');
INSERT INTO `_fa` VALUES ('443', 'fa fa-spoon ');
INSERT INTO `_fa` VALUES ('444', 'fa fa-spotify');
INSERT INTO `_fa` VALUES ('445', 'fa fa-square');
INSERT INTO `_fa` VALUES ('446', 'fa fa-square-o');
INSERT INTO `_fa` VALUES ('447', 'fa fa-stack-exchange');
INSERT INTO `_fa` VALUES ('448', 'fa fa-stack-overflow');
INSERT INTO `_fa` VALUES ('449', 'fa fa-star');
INSERT INTO `_fa` VALUES ('450', 'fa fa-star-half');
INSERT INTO `_fa` VALUES ('451', 'fa fa-star-half-empty');
INSERT INTO `_fa` VALUES ('452', 'fa fa-star-half-full');
INSERT INTO `_fa` VALUES ('453', 'fa fa-star-half-o');
INSERT INTO `_fa` VALUES ('454', 'fa fa-star-o');
INSERT INTO `_fa` VALUES ('455', 'fa fa-steam');
INSERT INTO `_fa` VALUES ('456', 'fa fa-steam-square');
INSERT INTO `_fa` VALUES ('457', 'fa fa-step-backward');
INSERT INTO `_fa` VALUES ('458', 'fa fa-step-forward');
INSERT INTO `_fa` VALUES ('459', 'fa fa-stethoscope');
INSERT INTO `_fa` VALUES ('460', 'fa fa-sticky-note');
INSERT INTO `_fa` VALUES ('461', 'fa fa-sticky-note-o');
INSERT INTO `_fa` VALUES ('462', 'fa fa-stop');
INSERT INTO `_fa` VALUES ('465', 'fa fa-street-view ');
INSERT INTO `_fa` VALUES ('466', 'fa fa-strikethrough');
INSERT INTO `_fa` VALUES ('467', 'fa fa-stumbleupon');
INSERT INTO `_fa` VALUES ('468', 'fa fa-stumbleupon-circle');
INSERT INTO `_fa` VALUES ('469', 'fa fa-subscript ');
INSERT INTO `_fa` VALUES ('470', 'fa fa-subway');
INSERT INTO `_fa` VALUES ('471', 'fa fa-suitcase');
INSERT INTO `_fa` VALUES ('472', 'fa fa-sun-o');
INSERT INTO `_fa` VALUES ('473', 'fa fa-superscript');
INSERT INTO `_fa` VALUES ('474', 'fa fa-support ');
INSERT INTO `_fa` VALUES ('475', 'fa fa-table');
INSERT INTO `_fa` VALUES ('476', 'fa fa-tablet');
INSERT INTO `_fa` VALUES ('477', 'fa fa-tachometer');
INSERT INTO `_fa` VALUES ('478', 'fa fa-tag');
INSERT INTO `_fa` VALUES ('479', 'fa fa-tags');
INSERT INTO `_fa` VALUES ('480', 'fa fa-tasks');
INSERT INTO `_fa` VALUES ('481', 'fa fa-taxi');
INSERT INTO `_fa` VALUES ('482', 'fa fa-television');
INSERT INTO `_fa` VALUES ('483', 'fa fa-tencent-weibo');
INSERT INTO `_fa` VALUES ('484', 'fa fa-terminal');
INSERT INTO `_fa` VALUES ('485', 'fa fa-text-height');
INSERT INTO `_fa` VALUES ('486', 'fa fa-text-width');
INSERT INTO `_fa` VALUES ('487', 'fa fa-th');
INSERT INTO `_fa` VALUES ('488', 'fa fa-th-large');
INSERT INTO `_fa` VALUES ('489', 'fa fa-th-list');
INSERT INTO `_fa` VALUES ('490', 'fa fa-thumb-tack');
INSERT INTO `_fa` VALUES ('491', 'fa fa-thumbs-down');
INSERT INTO `_fa` VALUES ('492', 'fa fa-thumbs-o-down');
INSERT INTO `_fa` VALUES ('493', 'fa fa-thumbs-o-up');
INSERT INTO `_fa` VALUES ('494', 'fa fa-thumbs-up ');
INSERT INTO `_fa` VALUES ('495', 'fa fa-ticket');
INSERT INTO `_fa` VALUES ('496', 'fa fa-times');
INSERT INTO `_fa` VALUES ('497', 'fa fa-times-circle');
INSERT INTO `_fa` VALUES ('498', 'fa fa-times-circle-o');
INSERT INTO `_fa` VALUES ('499', 'fa fa-tint ');
INSERT INTO `_fa` VALUES ('500', 'fa fa-toggle-down');
INSERT INTO `_fa` VALUES ('501', 'fa fa-toggle-left');
INSERT INTO `_fa` VALUES ('502', 'fa fa-toggle-off');
INSERT INTO `_fa` VALUES ('503', 'fa fa-toggle-on ');
INSERT INTO `_fa` VALUES ('504', 'fa fa-toggle-right');
INSERT INTO `_fa` VALUES ('505', 'fa fa-toggle-up');
INSERT INTO `_fa` VALUES ('506', 'fa fa-trademark');
INSERT INTO `_fa` VALUES ('507', 'fa fa-train ');
INSERT INTO `_fa` VALUES ('508', 'fa fa-transgender');
INSERT INTO `_fa` VALUES ('509', 'fa fa-transgender-alt');
INSERT INTO `_fa` VALUES ('510', 'fa fa-trash');
INSERT INTO `_fa` VALUES ('511', 'fa fa-trash-o');
INSERT INTO `_fa` VALUES ('512', 'fa fa-tree');
INSERT INTO `_fa` VALUES ('513', 'fa fa-trello');
INSERT INTO `_fa` VALUES ('514', 'fa fa-tripadvisor');
INSERT INTO `_fa` VALUES ('515', 'fa fa-trophy');
INSERT INTO `_fa` VALUES ('516', 'fa fa-truck');
INSERT INTO `_fa` VALUES ('517', 'fa fa-try ');
INSERT INTO `_fa` VALUES ('518', 'fa fa-tty');
INSERT INTO `_fa` VALUES ('519', 'fa fa-tumblr');
INSERT INTO `_fa` VALUES ('520', 'fa fa-tumblr-square');
INSERT INTO `_fa` VALUES ('521', 'fa fa-turkish-lira');
INSERT INTO `_fa` VALUES ('522', 'fa fa-tv');
INSERT INTO `_fa` VALUES ('523', 'fa fa-twitch');
INSERT INTO `_fa` VALUES ('524', 'fa fa-twitter');
INSERT INTO `_fa` VALUES ('525', 'fa fa-twitter-square');
INSERT INTO `_fa` VALUES ('526', 'fa fa-umbrella');
INSERT INTO `_fa` VALUES ('527', 'fa fa-underline');
INSERT INTO `_fa` VALUES ('528', 'fa fa-undo');
INSERT INTO `_fa` VALUES ('529', 'fa fa-university');
INSERT INTO `_fa` VALUES ('530', 'fa fa-unlink');
INSERT INTO `_fa` VALUES ('531', 'fa fa-unlock');
INSERT INTO `_fa` VALUES ('532', 'fa fa-unlock-alt');
INSERT INTO `_fa` VALUES ('533', 'fa fa-print');

-- ----------------------------
-- Table structure for _jabatan
-- ----------------------------
DROP TABLE IF EXISTS `_jabatan`;
CREATE TABLE `_jabatan` (
  `id_jabatan` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status_waiter` enum('TRUE','FALSE') NOT NULL DEFAULT 'FALSE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jabatan
-- ----------------------------
INSERT INTO `_jabatan` VALUES ('GM', 'General Manager', 'FALSE');
INSERT INTO `_jabatan` VALUES ('M', 'Manager', 'FALSE');
INSERT INTO `_jabatan` VALUES ('W', 'Waiters', 'FALSE');
INSERT INTO `_jabatan` VALUES ('ADMIN', 'Administrator FO', 'FALSE');

-- ----------------------------
-- Table structure for _jadwal_bayar
-- ----------------------------
DROP TABLE IF EXISTS `_jadwal_bayar`;
CREATE TABLE `_jadwal_bayar` (
  `id_jadwal_bayar` char(7) NOT NULL,
  `id_tingkat` varchar(10) DEFAULT NULL,
  `tahun` char(4) DEFAULT NULL,
  `bulan` char(2) DEFAULT NULL,
  `tanggal` char(2) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_bayar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jadwal_bayar
-- ----------------------------

-- ----------------------------
-- Table structure for _jadwal_bayar_detail
-- ----------------------------
DROP TABLE IF EXISTS `_jadwal_bayar_detail`;
CREATE TABLE `_jadwal_bayar_detail` (
  `id_jadwal_bayar_detail` int(5) NOT NULL AUTO_INCREMENT,
  `id_jadwal_bayar` varchar(20) DEFAULT NULL,
  `id_jenis_bayar` varchar(20) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_bayar_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jadwal_bayar_detail
-- ----------------------------

-- ----------------------------
-- Table structure for _jenis_bayar_khusus
-- ----------------------------
DROP TABLE IF EXISTS `_jenis_bayar_khusus`;
CREATE TABLE `_jenis_bayar_khusus` (
  `id_jenis_bayar_khusus` varchar(20) NOT NULL,
  `jenis_bayar_khusus` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  PRIMARY KEY (`id_jenis_bayar_khusus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jenis_bayar_khusus
-- ----------------------------
INSERT INTO `_jenis_bayar_khusus` VALUES ('5B0A28594A430', 'TryOut', '1865000');
INSERT INTO `_jenis_bayar_khusus` VALUES ('5B0A289A52EEC', 'Buku Paket', '741000');
INSERT INTO `_jenis_bayar_khusus` VALUES ('5B0A28D724411', 'Uang Pangkal', '2420000');

-- ----------------------------
-- Table structure for _jenis_bayar_khusus_detail
-- ----------------------------
DROP TABLE IF EXISTS `_jenis_bayar_khusus_detail`;
CREATE TABLE `_jenis_bayar_khusus_detail` (
  `id_jenis_bayar_khusus_detail` int(10) NOT NULL AUTO_INCREMENT,
  `id_jenis_bayar_khusus` varchar(20) DEFAULT NULL,
  `jenis_bayar` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  PRIMARY KEY (`id_jenis_bayar_khusus_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jenis_bayar_khusus_detail
-- ----------------------------
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('1', '5B0A28594A430', 'TryOut 1', '765000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('2', '5B0A28594A430', 'TryOut 2', '550000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('3', '5B0A28594A430', 'TryOut 3', '550000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('4', '5B0A289A52EEC', 'Paket Bahasa Indonesia', '176000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('5', '5B0A289A52EEC', 'Paket Matematika', '213000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('6', '5B0A289A52EEC', 'Paket IPA', '125000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('7', '5B0A289A52EEC', 'Paket Bahasa Inggris', '227000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('8', '5B0A28D724411', 'Uang Gedung', '1750000');
INSERT INTO `_jenis_bayar_khusus_detail` VALUES ('9', '5B0A28D724411', 'Uang Pendaftaran', '670000');

-- ----------------------------
-- Table structure for _jenis_bayar_umum
-- ----------------------------
DROP TABLE IF EXISTS `_jenis_bayar_umum`;
CREATE TABLE `_jenis_bayar_umum` (
  `id_jenis_bayar_umum` varchar(20) NOT NULL,
  `jenis_bayar` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  PRIMARY KEY (`id_jenis_bayar_umum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _jenis_bayar_umum
-- ----------------------------
INSERT INTO `_jenis_bayar_umum` VALUES ('5B0A27DD1C6F7', 'SPP', '565000');
INSERT INTO `_jenis_bayar_umum` VALUES ('5B0A27F84704A', 'Seragam', '750000');
INSERT INTO `_jenis_bayar_umum` VALUES ('5B0A281E6CFAB', 'Ujian', '350000');
INSERT INTO `_jenis_bayar_umum` VALUES ('5B0A282F99E12', 'Her Registrasi', '150000');

-- ----------------------------
-- Table structure for _karyawan
-- ----------------------------
DROP TABLE IF EXISTS `_karyawan`;
CREATE TABLE `_karyawan` (
  `nip` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nama_alias` varchar(8) NOT NULL,
  `passwords` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `id_agama` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `foto` text NOT NULL,
  `pendidikan_terakhir` varchar(100) NOT NULL,
  `status_aktif` enum('AKTIF','NONAKTIF') NOT NULL DEFAULT 'AKTIF',
  `id_jabatan` varchar(10) NOT NULL,
  `id_divisi` varchar(5) NOT NULL,
  `id_outlet` varchar(5) NOT NULL,
  `user_admin` enum('TRUE','FALSE') NOT NULL DEFAULT 'FALSE',
  `tanggal_masuk` date NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `status_pernikahan` tinyint(1) NOT NULL,
  `jumlah_tanggungan` int(3) NOT NULL,
  `nomor_bpjs` varchar(20) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _karyawan
-- ----------------------------
INSERT INTO `_karyawan` VALUES ('123', 'kARYAWAN BARU', '', '5687d889d3e019d5add1875792f96e51', 'BANYUWANGI', '2001-01-01', 'L', '1', '', '', '-', '', '-', 'AKTIF', 'ADMIN', '', '', 'TRUE', '0000-00-00', '0000-00-00', '0', '0', '', '');
INSERT INTO `_karyawan` VALUES ('1461505163', 'Administrator 123', '', 'bd82d3016227801b780ed24ec4867e02', 'Cirebon', '1995-12-31', 'L', '1', '', '', '123', '', 'S1', 'AKTIF', 'ADMIN', '', '', 'TRUE', '0000-00-00', '0000-00-00', '0', '0', '', '');

-- ----------------------------
-- Table structure for _kelas
-- ----------------------------
DROP TABLE IF EXISTS `_kelas`;
CREATE TABLE `_kelas` (
  `id_kelas` varchar(10) NOT NULL,
  `id_tingkat` varchar(10) DEFAULT NULL,
  `nama` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _kelas
-- ----------------------------
INSERT INTO `_kelas` VALUES ('7A', '7', '7A');
INSERT INTO `_kelas` VALUES ('7B', '7', '7B');
INSERT INTO `_kelas` VALUES ('7C', '7', '7C');
INSERT INTO `_kelas` VALUES ('7D', '7', '7D');
INSERT INTO `_kelas` VALUES ('8A', '8', '8A');
INSERT INTO `_kelas` VALUES ('8B', '8', '8B');
INSERT INTO `_kelas` VALUES ('8C', '8', '8C');
INSERT INTO `_kelas` VALUES ('8D', '8', '8D');
INSERT INTO `_kelas` VALUES ('9A', '9', '9A');
INSERT INTO `_kelas` VALUES ('9B', '9', '9B');
INSERT INTO `_kelas` VALUES ('9C', '9', '9C');
INSERT INTO `_kelas` VALUES ('9D', '9', '9D');

-- ----------------------------
-- Table structure for _notifikasi
-- ----------------------------
DROP TABLE IF EXISTS `_notifikasi`;
CREATE TABLE `_notifikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usernames` varchar(20) NOT NULL,
  `judul_notifikasi` varchar(200) NOT NULL,
  `isi_notifikasi` text NOT NULL,
  `link` text NOT NULL,
  `icon` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `status_open` enum('SUDAH','BELUM') NOT NULL DEFAULT 'BELUM',
  `status_push` enum('TRUE','FALSE') NOT NULL DEFAULT 'TRUE',
  `tabel` varchar(30) NOT NULL,
  `idx` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=692 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _notifikasi
-- ----------------------------
INSERT INTO `_notifikasi` VALUES ('71', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WINDA AYU LESTARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.006', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.006');
INSERT INTO `_notifikasi` VALUES ('69', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>jiop</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.006', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.006');
INSERT INTO `_notifikasi` VALUES ('70', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ALFINA NAILUZZAKIYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.001', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.001');
INSERT INTO `_notifikasi` VALUES ('68', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA FARA MARLINDA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.001', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.001');
INSERT INTO `_notifikasi` VALUES ('67', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WULAN APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.005', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.005');
INSERT INTO `_notifikasi` VALUES ('66', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYARIFAH CHOIRIYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.004');
INSERT INTO `_notifikasi` VALUES ('65', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IDINHA DE JESUS VIEGAS LOBO BELO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.004');
INSERT INTO `_notifikasi` VALUES ('64', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IDINHA DE JESUS VIEGAS LOBO BELO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.003');
INSERT INTO `_notifikasi` VALUES ('63', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Mega Ayu Wulandhari</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.002');
INSERT INTO `_notifikasi` VALUES ('62', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Mega Ayu Wulandhari</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.002');
INSERT INTO `_notifikasi` VALUES ('11', 'admin', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Mega Ayu Wulandhari</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002', '<i class=\'fa fa-book\'></i>', '2017-03-29 10:40:10', 'SUDAH', 'FALSE', 'pmb_create', '17.1.002');
INSERT INTO `_notifikasi` VALUES ('12', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Mega Ayu Wulandhari</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002', '<i class=\'fa fa-book\'></i>', '2017-03-29 10:40:10', 'BELUM', 'TRUE', 'pmb_create', '17.1.002');
INSERT INTO `_notifikasi` VALUES ('13', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IDINHA DE JESUS VIEGAS LOBO BELO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003', '<i class=\'fa fa-book\'></i>', '2017-03-31 12:12:12', 'BELUM', 'TRUE', 'pmb_create', '17.1.003');
INSERT INTO `_notifikasi` VALUES ('14', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IDINHA DE JESUS VIEGAS LOBO BELO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.004', '<i class=\'fa fa-book\'></i>', '2017-03-31 12:44:34', 'BELUM', 'TRUE', 'pmb_create', '17.1.004');
INSERT INTO `_notifikasi` VALUES ('15', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYARIFAH CHOIRIYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.004', '<i class=\'fa fa-book\'></i>', '2017-04-12 09:33:56', 'BELUM', 'TRUE', 'pmb_create', '17.1.004');
INSERT INTO `_notifikasi` VALUES ('16', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WULAN APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.005', '<i class=\'fa fa-book\'></i>', '2017-04-19 10:28:02', 'BELUM', 'TRUE', 'pmb_create', '17.1.005');
INSERT INTO `_notifikasi` VALUES ('17', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA FARA MARLINDA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.001', '<i class=\'fa fa-book\'></i>', '2017-04-21 09:17:52', 'BELUM', 'TRUE', 'pmb_create', '17.2.001');
INSERT INTO `_notifikasi` VALUES ('18', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>jiop</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.006', '<i class=\'fa fa-book\'></i>', '2017-04-22 11:48:07', 'BELUM', 'TRUE', 'pmb_create', '17.1.006');
INSERT INTO `_notifikasi` VALUES ('19', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ALFINA NAILUZZAKIYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.001', '<i class=\'fa fa-book\'></i>', '2017-04-25 09:57:29', 'BELUM', 'TRUE', 'pmb_create', '17.3.001');
INSERT INTO `_notifikasi` VALUES ('20', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WINDA AYU LESTARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.006', '<i class=\'fa fa-book\'></i>', '2017-04-25 10:42:30', 'BELUM', 'TRUE', 'pmb_create', '17.1.006');
INSERT INTO `_notifikasi` VALUES ('21', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDUR ROHMAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.007', '<i class=\'fa fa-book\'></i>', '2017-04-25 11:14:32', 'BELUM', 'TRUE', 'pmb_create', '17.1.007');
INSERT INTO `_notifikasi` VALUES ('22', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROBIATHUL HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.002', '<i class=\'fa fa-book\'></i>', '2017-04-25 11:36:46', 'BELUM', 'TRUE', 'pmb_create', '17.2.002');
INSERT INTO `_notifikasi` VALUES ('23', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LILIK EKA PUSPITASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.002', '<i class=\'fa fa-book\'></i>', '2017-04-26 09:21:54', 'BELUM', 'TRUE', 'pmb_create', '17.3.002');
INSERT INTO `_notifikasi` VALUES ('24', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKLIMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.003', '<i class=\'fa fa-book\'></i>', '2017-04-26 10:58:03', 'BELUM', 'TRUE', 'pmb_create', '17.3.003');
INSERT INTO `_notifikasi` VALUES ('25', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIDYA RAHMININGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.008', '<i class=\'fa fa-book\'></i>', '2017-04-26 11:17:10', 'BELUM', 'TRUE', 'pmb_create', '17.1.008');
INSERT INTO `_notifikasi` VALUES ('26', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZULFA FATKHU RAHMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.009', '<i class=\'fa fa-book\'></i>', '2017-04-26 11:21:58', 'BELUM', 'TRUE', 'pmb_create', '17.1.009');
INSERT INTO `_notifikasi` VALUES ('27', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.004', '<i class=\'fa fa-book\'></i>', '2017-04-26 11:47:46', 'BELUM', 'TRUE', 'pmb_create', '17.3.004');
INSERT INTO `_notifikasi` VALUES ('28', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LOLA MEY ARTIKASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.005', '<i class=\'fa fa-book\'></i>', '2017-04-26 12:00:18', 'BELUM', 'TRUE', 'pmb_create', '17.3.005');
INSERT INTO `_notifikasi` VALUES ('29', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITTI AISYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.006', '<i class=\'fa fa-book\'></i>', '2017-04-26 01:00:09', 'BELUM', 'TRUE', 'pmb_create', '17.3.006');
INSERT INTO `_notifikasi` VALUES ('30', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITASARI PUSPITA DEWI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.010', '<i class=\'fa fa-book\'></i>', '2017-04-27 10:13:39', 'BELUM', 'TRUE', 'pmb_create', '17.1.010');
INSERT INTO `_notifikasi` VALUES ('31', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ESTIWI MANDASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.003', '<i class=\'fa fa-book\'></i>', '2017-04-27 12:59:37', 'BELUM', 'TRUE', 'pmb_create', '17.2.003');
INSERT INTO `_notifikasi` VALUES ('32', 'baak', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KAROLINA PURNAMASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.007', '<i class=\'fa fa-book\'></i>', '2017-04-27 01:15:46', 'BELUM', 'TRUE', 'pmb_create', '17.3.007');
INSERT INTO `_notifikasi` VALUES ('33', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AGUSTINUS NGAIR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.011', '<i class=\'fa fa-book\'></i>', '2017-05-03 11:51:33', 'SUDAH', 'FALSE', 'pmb_create', '17.1.011');
INSERT INTO `_notifikasi` VALUES ('34', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AGUSTINUS NGAIR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.011', '<i class=\'fa fa-book\'></i>', '2017-05-03 11:51:33', 'SUDAH', 'FALSE', 'pmb_create', '17.1.011');
INSERT INTO `_notifikasi` VALUES ('35', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA ANGGRAINI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.012', '<i class=\'fa fa-book\'></i>', '2017-05-09 12:20:49', 'SUDAH', 'FALSE', 'pmb_create', '17.1.012');
INSERT INTO `_notifikasi` VALUES ('36', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA ANGGRAINI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.012', '<i class=\'fa fa-book\'></i>', '2017-05-09 12:20:49', 'SUDAH', 'FALSE', 'pmb_create', '17.1.012');
INSERT INTO `_notifikasi` VALUES ('37', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WISNO BOSA MAWUKODA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.013', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:08:43', 'SUDAH', 'FALSE', 'pmb_create', '17.1.013');
INSERT INTO `_notifikasi` VALUES ('38', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WISNO BOSA MAWUKODA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.013', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:08:43', 'SUDAH', 'FALSE', 'pmb_create', '17.1.013');
INSERT INTO `_notifikasi` VALUES ('39', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIA RADA KAKA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.014', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:27:48', 'SUDAH', 'FALSE', 'pmb_create', '17.1.014');
INSERT INTO `_notifikasi` VALUES ('40', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIA RADA KAKA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.014', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:27:48', 'SUDAH', 'FALSE', 'pmb_create', '17.1.014');
INSERT INTO `_notifikasi` VALUES ('41', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>dessy siska fitriani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.015', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:50:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.015');
INSERT INTO `_notifikasi` VALUES ('42', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>dessy siska fitriani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.015', '<i class=\'fa fa-book\'></i>', '2017-05-12 10:50:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.015');
INSERT INTO `_notifikasi` VALUES ('43', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LULUK HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.009', '<i class=\'fa fa-book\'></i>', '2017-05-16 09:36:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.009');
INSERT INTO `_notifikasi` VALUES ('44', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LULUK HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.009', '<i class=\'fa fa-book\'></i>', '2017-05-16 09:36:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.009');
INSERT INTO `_notifikasi` VALUES ('45', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DIDIMA BAIRSADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.010', '<i class=\'fa fa-book\'></i>', '2017-05-16 09:39:17', 'SUDAH', 'FALSE', 'pmb_create', '17.3.010');
INSERT INTO `_notifikasi` VALUES ('46', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DIDIMA BAIRSADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.010', '<i class=\'fa fa-book\'></i>', '2017-05-16 09:39:17', 'SUDAH', 'FALSE', 'pmb_create', '17.3.010');
INSERT INTO `_notifikasi` VALUES ('47', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:37:57', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('48', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:37:57', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('49', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:37:57', 'BELUM', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('50', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:37:57', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('51', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:37:57', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('52', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:53:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('53', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:53:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('54', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:53:15', 'BELUM', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('55', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:53:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('56', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 09:53:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('57', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 10:03:13', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('58', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 10:03:13', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('59', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 10:03:13', 'BELUM', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('60', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 10:03:13', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('61', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 10:03:13', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('72', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDUR ROHMAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.007', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.007');
INSERT INTO `_notifikasi` VALUES ('73', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROBIATHUL HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.002', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.002');
INSERT INTO `_notifikasi` VALUES ('74', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LILIK EKA PUSPITASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.002', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.002');
INSERT INTO `_notifikasi` VALUES ('75', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKLIMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.003', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.003');
INSERT INTO `_notifikasi` VALUES ('76', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIDYA RAHMININGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.008', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.008');
INSERT INTO `_notifikasi` VALUES ('77', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZULFA FATKHU RAHMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.009', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.009');
INSERT INTO `_notifikasi` VALUES ('78', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.004');
INSERT INTO `_notifikasi` VALUES ('79', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LOLA MEY ARTIKASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.005', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.005');
INSERT INTO `_notifikasi` VALUES ('80', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITTI AISYAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.006', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.006');
INSERT INTO `_notifikasi` VALUES ('81', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITASARI PUSPITA DEWI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.010', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.010');
INSERT INTO `_notifikasi` VALUES ('82', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ESTIWI MANDASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.003', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.003');
INSERT INTO `_notifikasi` VALUES ('83', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KAROLINA PURNAMASARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.007', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.007');
INSERT INTO `_notifikasi` VALUES ('84', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AGUSTINUS NGAIR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.011');
INSERT INTO `_notifikasi` VALUES ('85', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AGUSTINUS NGAIR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.011');
INSERT INTO `_notifikasi` VALUES ('86', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA ANGGRAINI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.012');
INSERT INTO `_notifikasi` VALUES ('87', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DITA ANGGRAINI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.012');
INSERT INTO `_notifikasi` VALUES ('88', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WISNO BOSA MAWUKODA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.013', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.013');
INSERT INTO `_notifikasi` VALUES ('89', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>WISNO BOSA MAWUKODA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.013', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.013');
INSERT INTO `_notifikasi` VALUES ('90', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIA RADA KAKA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.014', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.014');
INSERT INTO `_notifikasi` VALUES ('91', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIA RADA KAKA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.014', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.014');
INSERT INTO `_notifikasi` VALUES ('92', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>dessy siska fitriani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.015', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.015');
INSERT INTO `_notifikasi` VALUES ('93', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>dessy siska fitriani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.015', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.015');
INSERT INTO `_notifikasi` VALUES ('94', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LULUK HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.009', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.009');
INSERT INTO `_notifikasi` VALUES ('95', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LULUK HASANAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.009', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.009');
INSERT INTO `_notifikasi` VALUES ('96', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DIDIMA BAIRSADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.010', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.010');
INSERT INTO `_notifikasi` VALUES ('97', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DIDIMA BAIRSADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.010', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.010');
INSERT INTO `_notifikasi` VALUES ('98', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('99', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('100', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('101', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CARINA SZAINI ANGGELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.011', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.011');
INSERT INTO `_notifikasi` VALUES ('102', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('103', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('104', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('105', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHANIF KURNIA KHIKMATUL LUTZA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.012', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.012');
INSERT INTO `_notifikasi` VALUES ('106', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('107', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('108', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('109', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IFTIN MAULIDIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.004', '<i class=\'fa fa-book\'></i>', '2017-05-17 02:48:22', 'SUDAH', 'FALSE', 'pmb_create', '17.2.004');
INSERT INTO `_notifikasi` VALUES ('110', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SAVERINA IJU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.013', '<i class=\'fa fa-book\'></i>', '2017-05-19 02:05:23', 'SUDAH', 'FALSE', 'pmb_create', '17.3.013');
INSERT INTO `_notifikasi` VALUES ('111', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SAVERINA IJU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.013', '<i class=\'fa fa-book\'></i>', '2017-05-19 02:05:23', 'SUDAH', 'FALSE', 'pmb_create', '17.3.013');
INSERT INTO `_notifikasi` VALUES ('112', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SAVERINA IJU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.013', '<i class=\'fa fa-book\'></i>', '2017-05-19 02:05:23', 'BELUM', 'FALSE', 'pmb_create', '17.3.013');
INSERT INTO `_notifikasi` VALUES ('113', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SAVERINA IJU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.013', '<i class=\'fa fa-book\'></i>', '2017-05-19 02:05:23', 'SUDAH', 'FALSE', 'pmb_create', '17.3.013');
INSERT INTO `_notifikasi` VALUES ('114', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SAVERINA IJU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.013', '<i class=\'fa fa-book\'></i>', '2017-05-19 02:05:23', 'SUDAH', 'FALSE', 'pmb_create', '17.3.013');
INSERT INTO `_notifikasi` VALUES ('115', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FARIZQI BAYU LAKSONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.016', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:17:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.016');
INSERT INTO `_notifikasi` VALUES ('116', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FARIZQI BAYU LAKSONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.016', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:17:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.016');
INSERT INTO `_notifikasi` VALUES ('117', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FARIZQI BAYU LAKSONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.016', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:17:40', 'BELUM', 'FALSE', 'pmb_create', '17.1.016');
INSERT INTO `_notifikasi` VALUES ('118', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FARIZQI BAYU LAKSONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.016', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:17:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.016');
INSERT INTO `_notifikasi` VALUES ('119', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FARIZQI BAYU LAKSONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.016', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:17:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.016');
INSERT INTO `_notifikasi` VALUES ('120', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUCKIK PURWANTO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.017', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:27:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.017');
INSERT INTO `_notifikasi` VALUES ('121', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUCKIK PURWANTO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.017', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:27:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.017');
INSERT INTO `_notifikasi` VALUES ('122', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUCKIK PURWANTO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.017', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:27:44', 'BELUM', 'FALSE', 'pmb_create', '17.1.017');
INSERT INTO `_notifikasi` VALUES ('123', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUCKIK PURWANTO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.017', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:27:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.017');
INSERT INTO `_notifikasi` VALUES ('124', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUCKIK PURWANTO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.017', '<i class=\'fa fa-book\'></i>', '2017-05-22 11:27:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.017');
INSERT INTO `_notifikasi` VALUES ('125', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NICO AIBA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.018', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:40:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.018');
INSERT INTO `_notifikasi` VALUES ('126', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NICO AIBA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.018', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:40:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.018');
INSERT INTO `_notifikasi` VALUES ('127', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NICO AIBA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.018', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:40:45', 'BELUM', 'FALSE', 'pmb_create', '17.1.018');
INSERT INTO `_notifikasi` VALUES ('128', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NICO AIBA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.018', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:40:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.018');
INSERT INTO `_notifikasi` VALUES ('129', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NICO AIBA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.018', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:40:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.018');
INSERT INTO `_notifikasi` VALUES ('130', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>siti anissa miftakhul jannah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.014', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:45:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.014');
INSERT INTO `_notifikasi` VALUES ('131', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>siti anissa miftakhul jannah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.014', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:45:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.014');
INSERT INTO `_notifikasi` VALUES ('132', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>siti anissa miftakhul jannah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.014', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:45:15', 'BELUM', 'FALSE', 'pmb_create', '17.3.014');
INSERT INTO `_notifikasi` VALUES ('133', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>siti anissa miftakhul jannah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.014', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:45:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.014');
INSERT INTO `_notifikasi` VALUES ('134', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>siti anissa miftakhul jannah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.014', '<i class=\'fa fa-book\'></i>', '2017-05-22 01:45:15', 'SUDAH', 'FALSE', 'pmb_create', '17.3.014');
INSERT INTO `_notifikasi` VALUES ('135', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b> SOLEMAN IYOMUSI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.019', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:09:23', 'SUDAH', 'FALSE', 'pmb_create', '17.1.019');
INSERT INTO `_notifikasi` VALUES ('136', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b> SOLEMAN IYOMUSI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.019', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:09:23', 'SUDAH', 'FALSE', 'pmb_create', '17.1.019');
INSERT INTO `_notifikasi` VALUES ('137', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b> SOLEMAN IYOMUSI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.019', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:09:23', 'BELUM', 'FALSE', 'pmb_create', '17.1.019');
INSERT INTO `_notifikasi` VALUES ('138', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b> SOLEMAN IYOMUSI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.019', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:09:23', 'SUDAH', 'FALSE', 'pmb_create', '17.1.019');
INSERT INTO `_notifikasi` VALUES ('139', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b> SOLEMAN IYOMUSI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.019', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:09:23', 'SUDAH', 'FALSE', 'pmb_create', '17.1.019');
INSERT INTO `_notifikasi` VALUES ('140', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LAILATUL KARROMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.020', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:25:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.020');
INSERT INTO `_notifikasi` VALUES ('141', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LAILATUL KARROMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.020', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:25:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.020');
INSERT INTO `_notifikasi` VALUES ('142', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LAILATUL KARROMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.020', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:25:59', 'BELUM', 'FALSE', 'pmb_create', '17.1.020');
INSERT INTO `_notifikasi` VALUES ('143', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LAILATUL KARROMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.020', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:25:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.020');
INSERT INTO `_notifikasi` VALUES ('144', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LAILATUL KARROMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.020', '<i class=\'fa fa-book\'></i>', '2017-05-22 02:25:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.020');
INSERT INTO `_notifikasi` VALUES ('145', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI MUTMAINNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.021', '<i class=\'fa fa-book\'></i>', '2017-05-22 03:00:50', 'SUDAH', 'FALSE', 'pmb_create', '17.1.021');
INSERT INTO `_notifikasi` VALUES ('146', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI MUTMAINNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.021', '<i class=\'fa fa-book\'></i>', '2017-05-22 03:00:50', 'SUDAH', 'FALSE', 'pmb_create', '17.1.021');
INSERT INTO `_notifikasi` VALUES ('147', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI MUTMAINNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.021', '<i class=\'fa fa-book\'></i>', '2017-05-22 03:00:50', 'BELUM', 'FALSE', 'pmb_create', '17.1.021');
INSERT INTO `_notifikasi` VALUES ('148', 'partinah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI MUTMAINNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.021', '<i class=\'fa fa-book\'></i>', '2017-05-22 03:00:50', 'SUDAH', 'FALSE', 'pmb_create', '17.1.021');
INSERT INTO `_notifikasi` VALUES ('149', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI MUTMAINNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.021', '<i class=\'fa fa-book\'></i>', '2017-05-22 03:00:50', 'SUDAH', 'FALSE', 'pmb_create', '17.1.021');
INSERT INTO `_notifikasi` VALUES ('150', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YANTI KRISTINAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.015', '<i class=\'fa fa-book\'></i>', '2017-05-23 09:30:04', 'SUDAH', 'FALSE', 'pmb_create', '17.3.015');
INSERT INTO `_notifikasi` VALUES ('151', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YANTI KRISTINAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.015', '<i class=\'fa fa-book\'></i>', '2017-05-23 09:30:04', 'SUDAH', 'FALSE', 'pmb_create', '17.3.015');
INSERT INTO `_notifikasi` VALUES ('152', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YANTI KRISTINAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.015', '<i class=\'fa fa-book\'></i>', '2017-05-23 09:30:04', 'BELUM', 'FALSE', 'pmb_create', '17.3.015');
INSERT INTO `_notifikasi` VALUES ('153', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YANTI KRISTINAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.015', '<i class=\'fa fa-book\'></i>', '2017-05-23 09:30:04', 'SUDAH', 'FALSE', 'pmb_create', '17.3.015');
INSERT INTO `_notifikasi` VALUES ('154', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'BELUM', 'TRUE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('155', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('156', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('157', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'BELUM', 'FALSE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('158', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('159', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JAMES WILLIAM MANDACAN </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.022', '<i class=\'fa fa-book\'></i>', '2017-05-23 01:44:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.022');
INSERT INTO `_notifikasi` VALUES ('160', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'BELUM', 'TRUE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('161', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'SUDAH', 'FALSE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('162', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'SUDAH', 'FALSE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('163', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'BELUM', 'FALSE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('164', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'SUDAH', 'FALSE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('165', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOH. ALDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.023', '<i class=\'fa fa-book\'></i>', '2017-05-23 02:07:28', 'SUDAH', 'FALSE', 'pmb_create', '17.1.023');
INSERT INTO `_notifikasi` VALUES ('166', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'BELUM', 'TRUE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('167', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('168', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('169', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'BELUM', 'FALSE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('170', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'BELUM', 'FALSE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('171', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>CHANDRA HERMAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.024', '<i class=\'fa fa-book\'></i>', '2017-05-24 09:58:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.024');
INSERT INTO `_notifikasi` VALUES ('172', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILLA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.025', '<i class=\'fa fa-book\'></i>', '2017-05-30 09:57:40', 'BELUM', 'TRUE', 'pmb_create', '17.1.025');
INSERT INTO `_notifikasi` VALUES ('173', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILLA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.025', '<i class=\'fa fa-book\'></i>', '2017-05-30 09:57:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.025');
INSERT INTO `_notifikasi` VALUES ('174', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILLA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.025', '<i class=\'fa fa-book\'></i>', '2017-05-30 09:57:40', 'BELUM', 'FALSE', 'pmb_create', '17.1.025');
INSERT INTO `_notifikasi` VALUES ('175', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILLA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.025', '<i class=\'fa fa-book\'></i>', '2017-05-30 09:57:40', 'SUDAH', 'FALSE', 'pmb_create', '17.1.025');
INSERT INTO `_notifikasi` VALUES ('176', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.026', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:04', 'BELUM', 'TRUE', 'pmb_create', '17.1.026');
INSERT INTO `_notifikasi` VALUES ('177', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.026', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.026');
INSERT INTO `_notifikasi` VALUES ('178', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.026', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:04', 'BELUM', 'FALSE', 'pmb_create', '17.1.026');
INSERT INTO `_notifikasi` VALUES ('179', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.026', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.026');
INSERT INTO `_notifikasi` VALUES ('180', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:05', 'BELUM', 'TRUE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('181', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:05', 'SUDAH', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('182', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:05', 'BELUM', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('183', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Keisha Flavia Labobar</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-05-30 11:33:05', 'SUDAH', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('184', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISHA FLAVIA LABOBAR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:03:03', 'BELUM', 'TRUE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('185', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISHA FLAVIA LABOBAR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:03:03', 'SUDAH', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('186', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISHA FLAVIA LABOBAR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:03:03', 'BELUM', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('187', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISHA FLAVIA LABOBAR</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.027', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:03:03', 'SUDAH', 'FALSE', 'pmb_create', '17.1.027');
INSERT INTO `_notifikasi` VALUES ('188', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FILEX JHON ROI SANGKEK</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.028', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:12:56', 'BELUM', 'TRUE', 'pmb_create', '17.1.028');
INSERT INTO `_notifikasi` VALUES ('189', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FILEX JHON ROI SANGKEK</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.028', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:12:56', 'SUDAH', 'FALSE', 'pmb_create', '17.1.028');
INSERT INTO `_notifikasi` VALUES ('190', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FILEX JHON ROI SANGKEK</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.028', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:12:56', 'BELUM', 'FALSE', 'pmb_create', '17.1.028');
INSERT INTO `_notifikasi` VALUES ('191', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FILEX JHON ROI SANGKEK</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.028', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:12:56', 'SUDAH', 'FALSE', 'pmb_create', '17.1.028');
INSERT INTO `_notifikasi` VALUES ('192', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIKHA AMELIA SAFITRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.029', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:20:52', 'BELUM', 'TRUE', 'pmb_create', '17.1.029');
INSERT INTO `_notifikasi` VALUES ('193', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIKHA AMELIA SAFITRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.029', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:20:52', 'SUDAH', 'FALSE', 'pmb_create', '17.1.029');
INSERT INTO `_notifikasi` VALUES ('194', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIKHA AMELIA SAFITRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.029', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:20:52', 'BELUM', 'FALSE', 'pmb_create', '17.1.029');
INSERT INTO `_notifikasi` VALUES ('195', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RIKHA AMELIA SAFITRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.029', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:20:52', 'SUDAH', 'FALSE', 'pmb_create', '17.1.029');
INSERT INTO `_notifikasi` VALUES ('196', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YUDI FRANSINA TALAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.030', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:28:00', 'BELUM', 'TRUE', 'pmb_create', '17.1.030');
INSERT INTO `_notifikasi` VALUES ('197', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YUDI FRANSINA TALAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.030', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:28:00', 'SUDAH', 'FALSE', 'pmb_create', '17.1.030');
INSERT INTO `_notifikasi` VALUES ('198', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YUDI FRANSINA TALAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.030', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:28:00', 'BELUM', 'FALSE', 'pmb_create', '17.1.030');
INSERT INTO `_notifikasi` VALUES ('199', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>YUDI FRANSINA TALAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.030', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:28:00', 'SUDAH', 'FALSE', 'pmb_create', '17.1.030');
INSERT INTO `_notifikasi` VALUES ('200', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIA RENINSIA KEWA PATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.016', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:42:02', 'BELUM', 'TRUE', 'pmb_create', '17.3.016');
INSERT INTO `_notifikasi` VALUES ('201', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIA RENINSIA KEWA PATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.016', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:42:02', 'SUDAH', 'FALSE', 'pmb_create', '17.3.016');
INSERT INTO `_notifikasi` VALUES ('202', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIA RENINSIA KEWA PATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.016', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:42:02', 'BELUM', 'FALSE', 'pmb_create', '17.3.016');
INSERT INTO `_notifikasi` VALUES ('203', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIA RENINSIA KEWA PATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.016', '<i class=\'fa fa-book\'></i>', '2017-06-06 10:42:02', 'SUDAH', 'FALSE', 'pmb_create', '17.3.016');
INSERT INTO `_notifikasi` VALUES ('204', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>M.  SUBALI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.005', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:29:26', 'BELUM', 'TRUE', 'pmb_create', '17.2.005');
INSERT INTO `_notifikasi` VALUES ('205', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>M.  SUBALI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.005', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:29:26', 'SUDAH', 'FALSE', 'pmb_create', '17.2.005');
INSERT INTO `_notifikasi` VALUES ('206', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>M.  SUBALI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.005', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:29:26', 'BELUM', 'FALSE', 'pmb_create', '17.2.005');
INSERT INTO `_notifikasi` VALUES ('207', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>M.  SUBALI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.005', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:29:26', 'SUDAH', 'FALSE', 'pmb_create', '17.2.005');
INSERT INTO `_notifikasi` VALUES ('208', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI ROUDHOTUL JANNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.031', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:50:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.031');
INSERT INTO `_notifikasi` VALUES ('209', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI ROUDHOTUL JANNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.031', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:50:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.031');
INSERT INTO `_notifikasi` VALUES ('210', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI ROUDHOTUL JANNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.031', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:50:55', 'BELUM', 'FALSE', 'pmb_create', '17.1.031');
INSERT INTO `_notifikasi` VALUES ('211', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI ROUDHOTUL JANNAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.031', '<i class=\'fa fa-book\'></i>', '2017-06-06 11:50:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.031');
INSERT INTO `_notifikasi` VALUES ('212', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MENTARI PUTRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.032', '<i class=\'fa fa-book\'></i>', '2017-06-07 11:43:44', 'BELUM', 'TRUE', 'pmb_create', '17.1.032');
INSERT INTO `_notifikasi` VALUES ('213', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MENTARI PUTRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.032', '<i class=\'fa fa-book\'></i>', '2017-06-07 11:43:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.032');
INSERT INTO `_notifikasi` VALUES ('214', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MENTARI PUTRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.032', '<i class=\'fa fa-book\'></i>', '2017-06-07 11:43:44', 'BELUM', 'FALSE', 'pmb_create', '17.1.032');
INSERT INTO `_notifikasi` VALUES ('215', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MENTARI PUTRI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.032', '<i class=\'fa fa-book\'></i>', '2017-06-07 11:43:44', 'SUDAH', 'FALSE', 'pmb_create', '17.1.032');
INSERT INTO `_notifikasi` VALUES ('216', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANTONIUS PADUA TOMAE</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.033', '<i class=\'fa fa-book\'></i>', '2017-06-13 10:20:49', 'BELUM', 'TRUE', 'pmb_create', '17.1.033');
INSERT INTO `_notifikasi` VALUES ('217', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANTONIUS PADUA TOMAE</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.033', '<i class=\'fa fa-book\'></i>', '2017-06-13 10:20:49', 'SUDAH', 'FALSE', 'pmb_create', '17.1.033');
INSERT INTO `_notifikasi` VALUES ('218', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANTONIUS PADUA TOMAE</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.033', '<i class=\'fa fa-book\'></i>', '2017-06-13 10:20:49', 'BELUM', 'FALSE', 'pmb_create', '17.1.033');
INSERT INTO `_notifikasi` VALUES ('219', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANTONIUS PADUA TOMAE</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.033', '<i class=\'fa fa-book\'></i>', '2017-06-13 10:20:49', 'SUDAH', 'FALSE', 'pmb_create', '17.1.033');
INSERT INTO `_notifikasi` VALUES ('220', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AYU KRISTIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.034', '<i class=\'fa fa-book\'></i>', '2017-06-15 09:16:45', 'BELUM', 'TRUE', 'pmb_create', '17.1.034');
INSERT INTO `_notifikasi` VALUES ('221', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AYU KRISTIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.034', '<i class=\'fa fa-book\'></i>', '2017-06-15 09:16:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.034');
INSERT INTO `_notifikasi` VALUES ('222', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AYU KRISTIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.034', '<i class=\'fa fa-book\'></i>', '2017-06-15 09:16:45', 'BELUM', 'FALSE', 'pmb_create', '17.1.034');
INSERT INTO `_notifikasi` VALUES ('223', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AYU KRISTIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.034', '<i class=\'fa fa-book\'></i>', '2017-06-15 09:16:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.034');
INSERT INTO `_notifikasi` VALUES ('224', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIS INDAH MELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.035', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:16:42', 'BELUM', 'TRUE', 'pmb_create', '17.1.035');
INSERT INTO `_notifikasi` VALUES ('225', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIS INDAH MELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.035', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:16:43', 'SUDAH', 'FALSE', 'pmb_create', '17.1.035');
INSERT INTO `_notifikasi` VALUES ('226', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIS INDAH MELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.035', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:16:43', 'BELUM', 'FALSE', 'pmb_create', '17.1.035');
INSERT INTO `_notifikasi` VALUES ('227', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANIS INDAH MELINA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.035', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:16:43', 'SUDAH', 'FALSE', 'pmb_create', '17.1.035');
INSERT INTO `_notifikasi` VALUES ('228', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI ROHILAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.036', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:37:51', 'BELUM', 'TRUE', 'pmb_create', '17.1.036');
INSERT INTO `_notifikasi` VALUES ('229', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI ROHILAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.036', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:37:51', 'SUDAH', 'FALSE', 'pmb_create', '17.1.036');
INSERT INTO `_notifikasi` VALUES ('230', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI ROHILAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.036', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:37:51', 'BELUM', 'FALSE', 'pmb_create', '17.1.036');
INSERT INTO `_notifikasi` VALUES ('231', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI ROHILAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.036', '<i class=\'fa fa-book\'></i>', '2017-06-16 10:37:51', 'SUDAH', 'FALSE', 'pmb_create', '17.1.036');
INSERT INTO `_notifikasi` VALUES ('232', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OCTARICA CERVINA ASHARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.037', '<i class=\'fa fa-book\'></i>', '2017-06-16 11:58:48', 'BELUM', 'TRUE', 'pmb_create', '17.1.037');
INSERT INTO `_notifikasi` VALUES ('233', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OCTARICA CERVINA ASHARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.037', '<i class=\'fa fa-book\'></i>', '2017-06-16 11:58:48', 'SUDAH', 'FALSE', 'pmb_create', '17.1.037');
INSERT INTO `_notifikasi` VALUES ('234', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OCTARICA CERVINA ASHARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.037', '<i class=\'fa fa-book\'></i>', '2017-06-16 11:58:48', 'BELUM', 'FALSE', 'pmb_create', '17.1.037');
INSERT INTO `_notifikasi` VALUES ('235', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OCTARICA CERVINA ASHARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.037', '<i class=\'fa fa-book\'></i>', '2017-06-16 11:58:48', 'SUDAH', 'FALSE', 'pmb_create', '17.1.037');
INSERT INTO `_notifikasi` VALUES ('236', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LELY NUR ROSALENA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.038', '<i class=\'fa fa-book\'></i>', '2017-06-16 01:13:06', 'BELUM', 'TRUE', 'pmb_create', '17.1.038');
INSERT INTO `_notifikasi` VALUES ('237', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LELY NUR ROSALENA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.038', '<i class=\'fa fa-book\'></i>', '2017-06-16 01:13:06', 'SUDAH', 'FALSE', 'pmb_create', '17.1.038');
INSERT INTO `_notifikasi` VALUES ('238', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LELY NUR ROSALENA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.038', '<i class=\'fa fa-book\'></i>', '2017-06-16 01:13:06', 'BELUM', 'FALSE', 'pmb_create', '17.1.038');
INSERT INTO `_notifikasi` VALUES ('239', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LELY NUR ROSALENA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.038', '<i class=\'fa fa-book\'></i>', '2017-06-16 01:13:06', 'SUDAH', 'FALSE', 'pmb_create', '17.1.038');
INSERT INTO `_notifikasi` VALUES ('240', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DANIEL VEBRI YASIN LOLOLUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.006', '<i class=\'fa fa-book\'></i>', '2017-06-19 09:21:18', 'BELUM', 'TRUE', 'pmb_create', '17.2.006');
INSERT INTO `_notifikasi` VALUES ('241', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DANIEL VEBRI YASIN LOLOLUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.006', '<i class=\'fa fa-book\'></i>', '2017-06-19 09:21:18', 'SUDAH', 'FALSE', 'pmb_create', '17.2.006');
INSERT INTO `_notifikasi` VALUES ('242', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DANIEL VEBRI YASIN LOLOLUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.006', '<i class=\'fa fa-book\'></i>', '2017-06-19 09:21:18', 'BELUM', 'FALSE', 'pmb_create', '17.2.006');
INSERT INTO `_notifikasi` VALUES ('243', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DANIEL VEBRI YASIN LOLOLUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.006', '<i class=\'fa fa-book\'></i>', '2017-06-19 09:21:18', 'SUDAH', 'FALSE', 'pmb_create', '17.2.006');
INSERT INTO `_notifikasi` VALUES ('244', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAXIMUS SISTIAN ATUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.039', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:00:52', 'BELUM', 'TRUE', 'pmb_create', '17.1.039');
INSERT INTO `_notifikasi` VALUES ('245', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAXIMUS SISTIAN ATUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.039', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:00:52', 'SUDAH', 'FALSE', 'pmb_create', '17.1.039');
INSERT INTO `_notifikasi` VALUES ('246', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAXIMUS SISTIAN ATUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.039', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:00:52', 'BELUM', 'FALSE', 'pmb_create', '17.1.039');
INSERT INTO `_notifikasi` VALUES ('247', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAXIMUS SISTIAN ATUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.039', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:00:52', 'BELUM', 'FALSE', 'pmb_create', '17.1.039');
INSERT INTO `_notifikasi` VALUES ('248', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISYA.M.M.KILMANUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.040', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:22:00', 'BELUM', 'TRUE', 'pmb_create', '17.1.040');
INSERT INTO `_notifikasi` VALUES ('249', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISYA.M.M.KILMANUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.040', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:22:00', 'SUDAH', 'FALSE', 'pmb_create', '17.1.040');
INSERT INTO `_notifikasi` VALUES ('250', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISYA.M.M.KILMANUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.040', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:22:00', 'BELUM', 'FALSE', 'pmb_create', '17.1.040');
INSERT INTO `_notifikasi` VALUES ('251', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KEISYA.M.M.KILMANUN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.040', '<i class=\'fa fa-book\'></i>', '2017-06-19 11:22:00', 'BELUM', 'FALSE', 'pmb_create', '17.1.040');
INSERT INTO `_notifikasi` VALUES ('252', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZILFIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.017', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:06:52', 'BELUM', 'TRUE', 'pmb_create', '17.3.017');
INSERT INTO `_notifikasi` VALUES ('253', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZILFIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.017', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:06:52', 'BELUM', 'FALSE', 'pmb_create', '17.3.017');
INSERT INTO `_notifikasi` VALUES ('254', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZILFIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.017', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:06:52', 'BELUM', 'FALSE', 'pmb_create', '17.3.017');
INSERT INTO `_notifikasi` VALUES ('255', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZILFIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.017', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:06:52', 'SUDAH', 'FALSE', 'pmb_create', '17.3.017');
INSERT INTO `_notifikasi` VALUES ('256', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAKARINA AHOINNAI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.018', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:26:38', 'BELUM', 'TRUE', 'pmb_create', '17.3.018');
INSERT INTO `_notifikasi` VALUES ('257', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAKARINA AHOINNAI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.018', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:26:38', 'SUDAH', 'FALSE', 'pmb_create', '17.3.018');
INSERT INTO `_notifikasi` VALUES ('258', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAKARINA AHOINNAI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.018', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:26:38', 'BELUM', 'FALSE', 'pmb_create', '17.3.018');
INSERT INTO `_notifikasi` VALUES ('259', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAKARINA AHOINNAI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.018', '<i class=\'fa fa-book\'></i>', '2017-06-19 12:26:38', 'BELUM', 'FALSE', 'pmb_create', '17.3.018');
INSERT INTO `_notifikasi` VALUES ('260', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Anggita Dinda Florensya</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.019', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:16:50', 'BELUM', 'TRUE', 'pmb_create', '17.3.019');
INSERT INTO `_notifikasi` VALUES ('261', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Anggita Dinda Florensya</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.019', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:16:50', 'SUDAH', 'FALSE', 'pmb_create', '17.3.019');
INSERT INTO `_notifikasi` VALUES ('262', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Anggita Dinda Florensya</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.019', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:16:50', 'BELUM', 'FALSE', 'pmb_create', '17.3.019');
INSERT INTO `_notifikasi` VALUES ('263', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Anggita Dinda Florensya</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.019', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:16:50', 'SUDAH', 'FALSE', 'pmb_create', '17.3.019');
INSERT INTO `_notifikasi` VALUES ('264', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISKANDAR MUKTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.041', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:36:04', 'BELUM', 'TRUE', 'pmb_create', '17.1.041');
INSERT INTO `_notifikasi` VALUES ('265', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISKANDAR MUKTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.041', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:36:04', 'BELUM', 'FALSE', 'pmb_create', '17.1.041');
INSERT INTO `_notifikasi` VALUES ('266', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISKANDAR MUKTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.041', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:36:04', 'BELUM', 'FALSE', 'pmb_create', '17.1.041');
INSERT INTO `_notifikasi` VALUES ('267', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISKANDAR MUKTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.041', '<i class=\'fa fa-book\'></i>', '2017-06-21 11:36:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.041');
INSERT INTO `_notifikasi` VALUES ('268', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSINA RANGKORATAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.042', '<i class=\'fa fa-book\'></i>', '2017-07-03 10:27:57', 'BELUM', 'TRUE', 'pmb_create', '17.1.042');
INSERT INTO `_notifikasi` VALUES ('269', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSINA RANGKORATAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.042', '<i class=\'fa fa-book\'></i>', '2017-07-03 10:27:57', 'SUDAH', 'FALSE', 'pmb_create', '17.1.042');
INSERT INTO `_notifikasi` VALUES ('270', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSINA RANGKORATAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.042', '<i class=\'fa fa-book\'></i>', '2017-07-03 10:27:57', 'BELUM', 'FALSE', 'pmb_create', '17.1.042');
INSERT INTO `_notifikasi` VALUES ('271', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSINA RANGKORATAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.042', '<i class=\'fa fa-book\'></i>', '2017-07-03 10:27:57', 'BELUM', 'FALSE', 'pmb_create', '17.1.042');
INSERT INTO `_notifikasi` VALUES ('272', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OKTAVIANI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.043', '<i class=\'fa fa-book\'></i>', '2017-07-04 10:39:46', 'BELUM', 'TRUE', 'pmb_create', '17.1.043');
INSERT INTO `_notifikasi` VALUES ('273', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OKTAVIANI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.043', '<i class=\'fa fa-book\'></i>', '2017-07-04 10:39:46', 'SUDAH', 'FALSE', 'pmb_create', '17.1.043');
INSERT INTO `_notifikasi` VALUES ('274', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OKTAVIANI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.043', '<i class=\'fa fa-book\'></i>', '2017-07-04 10:39:46', 'BELUM', 'FALSE', 'pmb_create', '17.1.043');
INSERT INTO `_notifikasi` VALUES ('275', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>OKTAVIANI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.043', '<i class=\'fa fa-book\'></i>', '2017-07-04 10:39:46', 'BELUM', 'FALSE', 'pmb_create', '17.1.043');
INSERT INTO `_notifikasi` VALUES ('276', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MADINATUL MUNAWWARAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.044', '<i class=\'fa fa-book\'></i>', '2017-07-04 11:07:05', 'BELUM', 'TRUE', 'pmb_create', '17.1.044');
INSERT INTO `_notifikasi` VALUES ('277', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MADINATUL MUNAWWARAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.044', '<i class=\'fa fa-book\'></i>', '2017-07-04 11:07:05', 'SUDAH', 'FALSE', 'pmb_create', '17.1.044');
INSERT INTO `_notifikasi` VALUES ('278', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MADINATUL MUNAWWARAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.044', '<i class=\'fa fa-book\'></i>', '2017-07-04 11:07:05', 'BELUM', 'FALSE', 'pmb_create', '17.1.044');
INSERT INTO `_notifikasi` VALUES ('279', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MADINATUL MUNAWWARAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.044', '<i class=\'fa fa-book\'></i>', '2017-07-04 11:07:05', 'BELUM', 'FALSE', 'pmb_create', '17.1.044');
INSERT INTO `_notifikasi` VALUES ('280', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JANATUL MUNAWAROH NURROHMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.007', '<i class=\'fa fa-book\'></i>', '2017-07-05 08:00:54', 'BELUM', 'TRUE', 'pmb_create', '17.2.007');
INSERT INTO `_notifikasi` VALUES ('281', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JANATUL MUNAWAROH NURROHMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.007', '<i class=\'fa fa-book\'></i>', '2017-07-05 08:00:55', 'SUDAH', 'FALSE', 'pmb_create', '17.2.007');
INSERT INTO `_notifikasi` VALUES ('282', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JANATUL MUNAWAROH NURROHMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.007', '<i class=\'fa fa-book\'></i>', '2017-07-05 08:00:55', 'BELUM', 'FALSE', 'pmb_create', '17.2.007');
INSERT INTO `_notifikasi` VALUES ('283', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JANATUL MUNAWAROH NURROHMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.007', '<i class=\'fa fa-book\'></i>', '2017-07-05 08:00:55', 'SUDAH', 'FALSE', 'pmb_create', '17.2.007');
INSERT INTO `_notifikasi` VALUES ('284', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DINI SRI RAHAYU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.045', '<i class=\'fa fa-book\'></i>', '2017-07-05 09:44:26', 'BELUM', 'TRUE', 'pmb_create', '17.1.045');
INSERT INTO `_notifikasi` VALUES ('285', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DINI SRI RAHAYU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.045', '<i class=\'fa fa-book\'></i>', '2017-07-05 09:44:26', 'SUDAH', 'FALSE', 'pmb_create', '17.1.045');
INSERT INTO `_notifikasi` VALUES ('286', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DINI SRI RAHAYU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.045', '<i class=\'fa fa-book\'></i>', '2017-07-05 09:44:26', 'BELUM', 'FALSE', 'pmb_create', '17.1.045');
INSERT INTO `_notifikasi` VALUES ('287', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DINI SRI RAHAYU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.045', '<i class=\'fa fa-book\'></i>', '2017-07-05 09:44:26', 'BELUM', 'FALSE', 'pmb_create', '17.1.045');
INSERT INTO `_notifikasi` VALUES ('288', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUDMILA JAMLEAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.020', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:29:26', 'BELUM', 'TRUE', 'pmb_create', '17.3.020');
INSERT INTO `_notifikasi` VALUES ('289', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUDMILA JAMLEAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.020', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:29:26', 'SUDAH', 'FALSE', 'pmb_create', '17.3.020');
INSERT INTO `_notifikasi` VALUES ('290', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUDMILA JAMLEAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.020', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:29:26', 'BELUM', 'FALSE', 'pmb_create', '17.3.020');
INSERT INTO `_notifikasi` VALUES ('291', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUDMILA JAMLEAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.020', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:29:26', 'SUDAH', 'FALSE', 'pmb_create', '17.3.020');
INSERT INTO `_notifikasi` VALUES ('292', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FERA MEGA NINGRUM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.021', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:54:50', 'BELUM', 'TRUE', 'pmb_create', '17.3.021');
INSERT INTO `_notifikasi` VALUES ('293', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FERA MEGA NINGRUM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.021', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:54:50', 'SUDAH', 'FALSE', 'pmb_create', '17.3.021');
INSERT INTO `_notifikasi` VALUES ('294', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FERA MEGA NINGRUM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.021', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:54:50', 'BELUM', 'FALSE', 'pmb_create', '17.3.021');
INSERT INTO `_notifikasi` VALUES ('295', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FERA MEGA NINGRUM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.021', '<i class=\'fa fa-book\'></i>', '2017-07-05 10:54:50', 'BELUM', 'FALSE', 'pmb_create', '17.3.021');
INSERT INTO `_notifikasi` VALUES ('296', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI NURUL LUTFIAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.022', '<i class=\'fa fa-book\'></i>', '2017-07-06 09:48:58', 'BELUM', 'TRUE', 'pmb_create', '17.3.022');
INSERT INTO `_notifikasi` VALUES ('297', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI NURUL LUTFIAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.022', '<i class=\'fa fa-book\'></i>', '2017-07-06 09:48:58', 'SUDAH', 'FALSE', 'pmb_create', '17.3.022');
INSERT INTO `_notifikasi` VALUES ('298', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI NURUL LUTFIAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.022', '<i class=\'fa fa-book\'></i>', '2017-07-06 09:48:58', 'BELUM', 'FALSE', 'pmb_create', '17.3.022');
INSERT INTO `_notifikasi` VALUES ('299', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI NURUL LUTFIAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.022', '<i class=\'fa fa-book\'></i>', '2017-07-06 09:48:58', 'SUDAH', 'FALSE', 'pmb_create', '17.3.022');
INSERT INTO `_notifikasi` VALUES ('300', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELFIANA AGAPA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.046', '<i class=\'fa fa-book\'></i>', '2017-07-06 10:21:01', 'BELUM', 'TRUE', 'pmb_create', '17.1.046');
INSERT INTO `_notifikasi` VALUES ('301', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELFIANA AGAPA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.046', '<i class=\'fa fa-book\'></i>', '2017-07-06 10:21:01', 'SUDAH', 'FALSE', 'pmb_create', '17.1.046');
INSERT INTO `_notifikasi` VALUES ('302', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELFIANA AGAPA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.046', '<i class=\'fa fa-book\'></i>', '2017-07-06 10:21:01', 'BELUM', 'FALSE', 'pmb_create', '17.1.046');
INSERT INTO `_notifikasi` VALUES ('303', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELFIANA AGAPA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.046', '<i class=\'fa fa-book\'></i>', '2017-07-06 10:21:01', 'SUDAH', 'FALSE', 'pmb_create', '17.1.046');
INSERT INTO `_notifikasi` VALUES ('304', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARITJE ROMROMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.047', '<i class=\'fa fa-book\'></i>', '2017-07-07 10:33:54', 'BELUM', 'TRUE', 'pmb_create', '17.1.047');
INSERT INTO `_notifikasi` VALUES ('305', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARITJE ROMROMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.047', '<i class=\'fa fa-book\'></i>', '2017-07-07 10:33:54', 'SUDAH', 'FALSE', 'pmb_create', '17.1.047');
INSERT INTO `_notifikasi` VALUES ('306', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARITJE ROMROMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.047', '<i class=\'fa fa-book\'></i>', '2017-07-07 10:33:54', 'SUDAH', 'FALSE', 'pmb_create', '17.1.047');
INSERT INTO `_notifikasi` VALUES ('307', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARITJE ROMROMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.047', '<i class=\'fa fa-book\'></i>', '2017-07-07 10:33:54', 'BELUM', 'FALSE', 'pmb_create', '17.1.047');
INSERT INTO `_notifikasi` VALUES ('308', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSIYANA RENUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.048', '<i class=\'fa fa-book\'></i>', '2017-07-08 12:26:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.048');
INSERT INTO `_notifikasi` VALUES ('309', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSIYANA RENUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.048', '<i class=\'fa fa-book\'></i>', '2017-07-08 12:26:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.048');
INSERT INTO `_notifikasi` VALUES ('310', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSIYANA RENUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.048', '<i class=\'fa fa-book\'></i>', '2017-07-08 12:26:55', 'BELUM', 'FALSE', 'pmb_create', '17.1.048');
INSERT INTO `_notifikasi` VALUES ('311', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROSIYANA RENUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.048', '<i class=\'fa fa-book\'></i>', '2017-07-08 12:26:55', 'BELUM', 'FALSE', 'pmb_create', '17.1.048');
INSERT INTO `_notifikasi` VALUES ('312', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LYAN HADI KUSUMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.008', '<i class=\'fa fa-book\'></i>', '2017-07-10 09:54:21', 'BELUM', 'TRUE', 'pmb_create', '17.2.008');
INSERT INTO `_notifikasi` VALUES ('313', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LYAN HADI KUSUMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.008', '<i class=\'fa fa-book\'></i>', '2017-07-10 09:54:21', 'SUDAH', 'FALSE', 'pmb_create', '17.2.008');
INSERT INTO `_notifikasi` VALUES ('314', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LYAN HADI KUSUMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.008', '<i class=\'fa fa-book\'></i>', '2017-07-10 09:54:21', 'BELUM', 'FALSE', 'pmb_create', '17.2.008');
INSERT INTO `_notifikasi` VALUES ('315', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LYAN HADI KUSUMA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.008', '<i class=\'fa fa-book\'></i>', '2017-07-10 09:54:21', 'BELUM', 'FALSE', 'pmb_create', '17.2.008');
INSERT INTO `_notifikasi` VALUES ('316', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>REINHARD AGUSTHINUS RUMLAWANG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.049', '<i class=\'fa fa-book\'></i>', '2017-07-10 01:12:04', 'BELUM', 'TRUE', 'pmb_create', '17.1.049');
INSERT INTO `_notifikasi` VALUES ('317', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>REINHARD AGUSTHINUS RUMLAWANG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.049', '<i class=\'fa fa-book\'></i>', '2017-07-10 01:12:04', 'SUDAH', 'FALSE', 'pmb_create', '17.1.049');
INSERT INTO `_notifikasi` VALUES ('318', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>REINHARD AGUSTHINUS RUMLAWANG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.049', '<i class=\'fa fa-book\'></i>', '2017-07-10 01:12:04', 'BELUM', 'FALSE', 'pmb_create', '17.1.049');
INSERT INTO `_notifikasi` VALUES ('319', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>REINHARD AGUSTHINUS RUMLAWANG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.049', '<i class=\'fa fa-book\'></i>', '2017-07-10 01:12:04', 'BELUM', 'FALSE', 'pmb_create', '17.1.049');
INSERT INTO `_notifikasi` VALUES ('320', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.050', '<i class=\'fa fa-book\'></i>', '2017-07-11 12:04:39', 'BELUM', 'TRUE', 'pmb_create', '17.1.050');
INSERT INTO `_notifikasi` VALUES ('321', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.050', '<i class=\'fa fa-book\'></i>', '2017-07-11 12:04:39', 'SUDAH', 'FALSE', 'pmb_create', '17.1.050');
INSERT INTO `_notifikasi` VALUES ('322', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.050', '<i class=\'fa fa-book\'></i>', '2017-07-11 12:04:39', 'BELUM', 'FALSE', 'pmb_create', '17.1.050');
INSERT INTO `_notifikasi` VALUES ('323', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NOVITA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.050', '<i class=\'fa fa-book\'></i>', '2017-07-11 12:04:39', 'SUDAH', 'FALSE', 'pmb_create', '17.1.050');
INSERT INTO `_notifikasi` VALUES ('324', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SARNA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.023', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:11:18', 'BELUM', 'TRUE', 'pmb_create', '17.3.023');
INSERT INTO `_notifikasi` VALUES ('325', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SARNA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.023', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:11:18', 'SUDAH', 'FALSE', 'pmb_create', '17.3.023');
INSERT INTO `_notifikasi` VALUES ('326', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SARNA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.023', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:11:18', 'SUDAH', 'FALSE', 'pmb_create', '17.3.023');
INSERT INTO `_notifikasi` VALUES ('327', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SARNA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.023', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:11:18', 'BELUM', 'FALSE', 'pmb_create', '17.3.023');
INSERT INTO `_notifikasi` VALUES ('328', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI RATNA NUZULA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.009', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:33:15', 'BELUM', 'TRUE', 'pmb_create', '17.2.009');
INSERT INTO `_notifikasi` VALUES ('329', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI RATNA NUZULA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.009', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:33:15', 'SUDAH', 'FALSE', 'pmb_create', '17.2.009');
INSERT INTO `_notifikasi` VALUES ('330', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI RATNA NUZULA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.009', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:33:15', 'SUDAH', 'FALSE', 'pmb_create', '17.2.009');
INSERT INTO `_notifikasi` VALUES ('331', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI RATNA NUZULA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.009', '<i class=\'fa fa-book\'></i>', '2017-07-12 09:33:15', 'BELUM', 'FALSE', 'pmb_create', '17.2.009');
INSERT INTO `_notifikasi` VALUES ('332', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ASRIANA LAINONG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.051', '<i class=\'fa fa-book\'></i>', '2017-07-13 11:28:10', 'BELUM', 'TRUE', 'pmb_create', '17.1.051');
INSERT INTO `_notifikasi` VALUES ('333', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ASRIANA LAINONG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.051', '<i class=\'fa fa-book\'></i>', '2017-07-13 11:28:10', 'SUDAH', 'FALSE', 'pmb_create', '17.1.051');
INSERT INTO `_notifikasi` VALUES ('334', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ASRIANA LAINONG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.051', '<i class=\'fa fa-book\'></i>', '2017-07-13 11:28:10', 'BELUM', 'TRUE', 'pmb_create', '17.1.051');
INSERT INTO `_notifikasi` VALUES ('335', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ASRIANA LAINONG</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.051', '<i class=\'fa fa-book\'></i>', '2017-07-13 11:28:10', 'SUDAH', 'FALSE', 'pmb_create', '17.1.051');
INSERT INTO `_notifikasi` VALUES ('336', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>yuliana kwaitota</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.024', '<i class=\'fa fa-book\'></i>', '2017-07-14 09:40:52', 'BELUM', 'TRUE', 'pmb_create', '17.3.024');
INSERT INTO `_notifikasi` VALUES ('337', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>yuliana kwaitota</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.024', '<i class=\'fa fa-book\'></i>', '2017-07-14 09:40:52', 'SUDAH', 'FALSE', 'pmb_create', '17.3.024');
INSERT INTO `_notifikasi` VALUES ('338', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>yuliana kwaitota</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.024', '<i class=\'fa fa-book\'></i>', '2017-07-14 09:40:52', 'BELUM', 'TRUE', 'pmb_create', '17.3.024');
INSERT INTO `_notifikasi` VALUES ('339', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>yuliana kwaitota</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.024', '<i class=\'fa fa-book\'></i>', '2017-07-14 09:40:52', 'SUDAH', 'FALSE', 'pmb_create', '17.3.024');
INSERT INTO `_notifikasi` VALUES ('340', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LISYE EFRUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.052', '<i class=\'fa fa-book\'></i>', '2017-07-14 11:39:59', 'BELUM', 'TRUE', 'pmb_create', '17.1.052');
INSERT INTO `_notifikasi` VALUES ('341', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LISYE EFRUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.052', '<i class=\'fa fa-book\'></i>', '2017-07-14 11:39:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.052');
INSERT INTO `_notifikasi` VALUES ('342', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LISYE EFRUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.052', '<i class=\'fa fa-book\'></i>', '2017-07-14 11:39:59', 'BELUM', 'TRUE', 'pmb_create', '17.1.052');
INSERT INTO `_notifikasi` VALUES ('343', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LISYE EFRUAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.052', '<i class=\'fa fa-book\'></i>', '2017-07-14 11:39:59', 'SUDAH', 'FALSE', 'pmb_create', '17.1.052');
INSERT INTO `_notifikasi` VALUES ('344', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEBORA KURNIA SEPTIYA BUDI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.010', '<i class=\'fa fa-book\'></i>', '2017-07-17 01:05:54', 'BELUM', 'TRUE', 'pmb_create', '17.2.010');
INSERT INTO `_notifikasi` VALUES ('345', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEBORA KURNIA SEPTIYA BUDI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.010', '<i class=\'fa fa-book\'></i>', '2017-07-17 01:05:54', 'SUDAH', 'FALSE', 'pmb_create', '17.2.010');
INSERT INTO `_notifikasi` VALUES ('346', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEBORA KURNIA SEPTIYA BUDI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.010', '<i class=\'fa fa-book\'></i>', '2017-07-17 01:05:54', 'BELUM', 'TRUE', 'pmb_create', '17.2.010');
INSERT INTO `_notifikasi` VALUES ('347', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEBORA KURNIA SEPTIYA BUDI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.010', '<i class=\'fa fa-book\'></i>', '2017-07-17 01:05:54', 'SUDAH', 'FALSE', 'pmb_create', '17.2.010');
INSERT INTO `_notifikasi` VALUES ('348', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>nurillah widiani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.053', '<i class=\'fa fa-book\'></i>', '2017-07-19 02:33:34', 'BELUM', 'TRUE', 'pmb_create', '17.1.053');
INSERT INTO `_notifikasi` VALUES ('349', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>nurillah widiani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.053', '<i class=\'fa fa-book\'></i>', '2017-07-19 02:33:34', 'SUDAH', 'FALSE', 'pmb_create', '17.1.053');
INSERT INTO `_notifikasi` VALUES ('350', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>nurillah widiani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.053', '<i class=\'fa fa-book\'></i>', '2017-07-19 02:33:34', 'BELUM', 'TRUE', 'pmb_create', '17.1.053');
INSERT INTO `_notifikasi` VALUES ('351', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>nurillah widiani</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.053', '<i class=\'fa fa-book\'></i>', '2017-07-19 02:33:34', 'SUDAH', 'FALSE', 'pmb_create', '17.1.053');
INSERT INTO `_notifikasi` VALUES ('352', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NUR AFIFA NOVIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.054', '<i class=\'fa fa-book\'></i>', '2017-07-20 10:44:25', 'BELUM', 'TRUE', 'pmb_create', '17.1.054');
INSERT INTO `_notifikasi` VALUES ('353', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NUR AFIFA NOVIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.054', '<i class=\'fa fa-book\'></i>', '2017-07-20 10:44:25', 'SUDAH', 'FALSE', 'pmb_create', '17.1.054');
INSERT INTO `_notifikasi` VALUES ('354', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NUR AFIFA NOVIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.054', '<i class=\'fa fa-book\'></i>', '2017-07-20 10:44:25', 'BELUM', 'TRUE', 'pmb_create', '17.1.054');
INSERT INTO `_notifikasi` VALUES ('355', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NUR AFIFA NOVIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.054', '<i class=\'fa fa-book\'></i>', '2017-07-20 10:44:25', 'SUDAH', 'FALSE', 'pmb_create', '17.1.054');
INSERT INTO `_notifikasi` VALUES ('356', '04.06.012', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>Vivi Azmil Umroh</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002.P', '<i class=\'fa fa-book\'></i>', '2017-07-20 11:37:16', 'BELUM', 'TRUE', 'pmb_create', '17.1.002.P');
INSERT INTO `_notifikasi` VALUES ('357', 'deni', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>Vivi Azmil Umroh</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002.P', '<i class=\'fa fa-book\'></i>', '2017-07-20 11:37:16', 'SUDAH', 'FALSE', 'pmb_create', '17.1.002.P');
INSERT INTO `_notifikasi` VALUES ('358', 'dyah', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>Vivi Azmil Umroh</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002.P', '<i class=\'fa fa-book\'></i>', '2017-07-20 11:37:16', 'BELUM', 'TRUE', 'pmb_create', '17.1.002.P');
INSERT INTO `_notifikasi` VALUES ('359', 'trimawati', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>Vivi Azmil Umroh</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.002.P', '<i class=\'fa fa-book\'></i>', '2017-07-20 11:37:16', 'SUDAH', 'FALSE', 'pmb_create', '17.1.002.P');
INSERT INTO `_notifikasi` VALUES ('360', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>m0hammad zaki arif</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.055', '<i class=\'fa fa-book\'></i>', '2017-07-21 01:19:24', 'BELUM', 'TRUE', 'pmb_create', '17.1.055');
INSERT INTO `_notifikasi` VALUES ('361', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>m0hammad zaki arif</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.055', '<i class=\'fa fa-book\'></i>', '2017-07-21 01:19:24', 'SUDAH', 'FALSE', 'pmb_create', '17.1.055');
INSERT INTO `_notifikasi` VALUES ('362', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>m0hammad zaki arif</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.055', '<i class=\'fa fa-book\'></i>', '2017-07-21 01:19:24', 'BELUM', 'TRUE', 'pmb_create', '17.1.055');
INSERT INTO `_notifikasi` VALUES ('363', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>m0hammad zaki arif</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.055', '<i class=\'fa fa-book\'></i>', '2017-07-21 01:19:24', 'SUDAH', 'FALSE', 'pmb_create', '17.1.055');
INSERT INTO `_notifikasi` VALUES ('364', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NATALIA RADA </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.056', '<i class=\'fa fa-book\'></i>', '2017-07-24 08:30:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.056');
INSERT INTO `_notifikasi` VALUES ('365', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NATALIA RADA </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.056', '<i class=\'fa fa-book\'></i>', '2017-07-24 08:30:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.056');
INSERT INTO `_notifikasi` VALUES ('366', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NATALIA RADA </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.056', '<i class=\'fa fa-book\'></i>', '2017-07-24 08:30:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.056');
INSERT INTO `_notifikasi` VALUES ('367', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NATALIA RADA </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.056', '<i class=\'fa fa-book\'></i>', '2017-07-24 08:30:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.056');
INSERT INTO `_notifikasi` VALUES ('368', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI SHOLIHAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.057', '<i class=\'fa fa-book\'></i>', '2017-07-27 09:56:15', 'BELUM', 'TRUE', 'pmb_create', '17.1.057');
INSERT INTO `_notifikasi` VALUES ('369', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI SHOLIHAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.057', '<i class=\'fa fa-book\'></i>', '2017-07-27 09:56:15', 'SUDAH', 'FALSE', 'pmb_create', '17.1.057');
INSERT INTO `_notifikasi` VALUES ('370', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI SHOLIHAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.057', '<i class=\'fa fa-book\'></i>', '2017-07-27 09:56:15', 'BELUM', 'TRUE', 'pmb_create', '17.1.057');
INSERT INTO `_notifikasi` VALUES ('371', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI SHOLIHAH </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.057', '<i class=\'fa fa-book\'></i>', '2017-07-27 09:56:15', 'BELUM', 'FALSE', 'pmb_create', '17.1.057');
INSERT INTO `_notifikasi` VALUES ('372', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ELTON JHON SUMARDI MARS BILI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.058', '<i class=\'fa fa-book\'></i>', '2017-08-07 11:46:38', 'BELUM', 'TRUE', 'pmb_create', '17.1.058');
INSERT INTO `_notifikasi` VALUES ('373', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ELTON JHON SUMARDI MARS BILI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.058', '<i class=\'fa fa-book\'></i>', '2017-08-07 11:46:38', 'SUDAH', 'FALSE', 'pmb_create', '17.1.058');
INSERT INTO `_notifikasi` VALUES ('374', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ELTON JHON SUMARDI MARS BILI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.058', '<i class=\'fa fa-book\'></i>', '2017-08-07 11:46:38', 'BELUM', 'TRUE', 'pmb_create', '17.1.058');
INSERT INTO `_notifikasi` VALUES ('375', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ELTON JHON SUMARDI MARS BILI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.058', '<i class=\'fa fa-book\'></i>', '2017-08-07 11:46:38', 'BELUM', 'FALSE', 'pmb_create', '17.1.058');
INSERT INTO `_notifikasi` VALUES ('376', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELIS ABISAI BANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.059', '<i class=\'fa fa-book\'></i>', '2017-08-07 12:28:45', 'BELUM', 'TRUE', 'pmb_create', '17.1.059');
INSERT INTO `_notifikasi` VALUES ('377', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELIS ABISAI BANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.059', '<i class=\'fa fa-book\'></i>', '2017-08-07 12:28:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.059');
INSERT INTO `_notifikasi` VALUES ('378', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELIS ABISAI BANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.059', '<i class=\'fa fa-book\'></i>', '2017-08-07 12:28:45', 'BELUM', 'TRUE', 'pmb_create', '17.1.059');
INSERT INTO `_notifikasi` VALUES ('379', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DELIS ABISAI BANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.059', '<i class=\'fa fa-book\'></i>', '2017-08-07 12:28:45', 'SUDAH', 'FALSE', 'pmb_create', '17.1.059');
INSERT INTO `_notifikasi` VALUES ('380', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DWI RENY PRAMUDITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.011', '<i class=\'fa fa-book\'></i>', '2017-08-08 02:05:32', 'BELUM', 'TRUE', 'pmb_create', '17.2.011');
INSERT INTO `_notifikasi` VALUES ('381', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DWI RENY PRAMUDITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.011', '<i class=\'fa fa-book\'></i>', '2017-08-08 02:05:32', 'SUDAH', 'FALSE', 'pmb_create', '17.2.011');
INSERT INTO `_notifikasi` VALUES ('382', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DWI RENY PRAMUDITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.011', '<i class=\'fa fa-book\'></i>', '2017-08-08 02:05:32', 'BELUM', 'TRUE', 'pmb_create', '17.2.011');
INSERT INTO `_notifikasi` VALUES ('383', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DWI RENY PRAMUDITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.011', '<i class=\'fa fa-book\'></i>', '2017-08-08 02:05:32', 'SUDAH', 'FALSE', 'pmb_create', '17.2.011');
INSERT INTO `_notifikasi` VALUES ('384', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILA ROMADHONI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.060', '<i class=\'fa fa-book\'></i>', '2017-08-10 10:57:22', 'BELUM', 'TRUE', 'pmb_create', '17.1.060');
INSERT INTO `_notifikasi` VALUES ('385', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILA ROMADHONI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.060', '<i class=\'fa fa-book\'></i>', '2017-08-10 10:57:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.060');
INSERT INTO `_notifikasi` VALUES ('386', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILA ROMADHONI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.060', '<i class=\'fa fa-book\'></i>', '2017-08-10 10:57:22', 'BELUM', 'TRUE', 'pmb_create', '17.1.060');
INSERT INTO `_notifikasi` VALUES ('387', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>BILA ROMADHONI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.060', '<i class=\'fa fa-book\'></i>', '2017-08-10 10:57:22', 'SUDAH', 'FALSE', 'pmb_create', '17.1.060');
INSERT INTO `_notifikasi` VALUES ('388', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SOLIHA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.061', '<i class=\'fa fa-book\'></i>', '2017-08-10 11:12:33', 'BELUM', 'TRUE', 'pmb_create', '17.1.061');
INSERT INTO `_notifikasi` VALUES ('389', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SOLIHA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.061', '<i class=\'fa fa-book\'></i>', '2017-08-10 11:12:33', 'SUDAH', 'FALSE', 'pmb_create', '17.1.061');
INSERT INTO `_notifikasi` VALUES ('390', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SOLIHA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.061', '<i class=\'fa fa-book\'></i>', '2017-08-10 11:12:33', 'BELUM', 'TRUE', 'pmb_create', '17.1.061');
INSERT INTO `_notifikasi` VALUES ('391', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SOLIHA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.061', '<i class=\'fa fa-book\'></i>', '2017-08-10 11:12:33', 'SUDAH', 'FALSE', 'pmb_create', '17.1.061');
INSERT INTO `_notifikasi` VALUES ('392', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'BELUM', 'TRUE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('393', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'SUDAH', 'FALSE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('394', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'SUDAH', 'FALSE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('395', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'BELUM', 'TRUE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('396', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'BELUM', 'TRUE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('397', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SELPINA YOSINTA DOUW</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.012', '<i class=\'fa fa-book\'></i>', '2017-08-14 09:19:01', 'BELUM', 'FALSE', 'pmb_create', '17.2.012');
INSERT INTO `_notifikasi` VALUES ('398', '04.06.012', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'BELUM', 'TRUE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('399', 'dedy', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('400', 'deni', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'BELUM', 'FALSE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('401', 'dyah', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'BELUM', 'TRUE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('402', 'suwondo', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'BELUM', 'TRUE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('403', 'trimawati', 'Pendaftaran Mahasiswa Pindahan / Alih Jenjang.', 'Pendaftaran Calon Mahasiswa Pindahan / Alih Jenjang atas nama <b>RAFIKA LISANA AMELIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.003.P', '<i class=\'fa fa-book\'></i>', '2017-08-15 01:02:13', 'SUDAH', 'FALSE', 'pmb_create', '17.1.003.P');
INSERT INTO `_notifikasi` VALUES ('404', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'BELUM', 'TRUE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('405', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'SUDAH', 'FALSE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('406', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'SUDAH', 'FALSE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('407', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'BELUM', 'TRUE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('408', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'BELUM', 'TRUE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('409', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>LUKMAN HAKIM</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.001', '<i class=\'fa fa-book\'></i>', '2017-08-15 02:20:36', 'SUDAH', 'FALSE', 'pmb_create', '17.4.001');
INSERT INTO `_notifikasi` VALUES ('410', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('411', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'SUDAH', 'FALSE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('412', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'SUDAH', 'FALSE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('413', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('414', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('415', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.002', '<i class=\'fa fa-book\'></i>', '2017-08-15 04:08:05', 'SUDAH', 'FALSE', 'pmb_create', '17.4.002');
INSERT INTO `_notifikasi` VALUES ('416', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'BELUM', 'TRUE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('417', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'SUDAH', 'FALSE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('418', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'SUDAH', 'FALSE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('419', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'BELUM', 'TRUE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('420', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'BELUM', 'TRUE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('421', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Sarli Lekatompessy</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.003', '<i class=\'fa fa-book\'></i>', '2017-08-21 10:28:25', 'SUDAH', 'FALSE', 'pmb_create', '17.4.003');
INSERT INTO `_notifikasi` VALUES ('422', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('423', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'SUDAH', 'FALSE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('424', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'SUDAH', 'FALSE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('425', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('426', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('427', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>AHMAD ZAKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.004', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:20:08', 'SUDAH', 'FALSE', 'pmb_create', '17.4.004');
INSERT INTO `_notifikasi` VALUES ('428', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'BELUM', 'TRUE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('429', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'SUDAH', 'FALSE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('430', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'SUDAH', 'FALSE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('431', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'BELUM', 'TRUE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('432', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'BELUM', 'TRUE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('433', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KATHARINA BEKU</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.005', '<i class=\'fa fa-book\'></i>', '2017-08-21 01:48:49', 'SUDAH', 'FALSE', 'pmb_create', '17.4.005');
INSERT INTO `_notifikasi` VALUES ('434', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('435', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'BELUM', 'FALSE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('436', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'SUDAH', 'FALSE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('437', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('438', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('439', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANISAH MAHRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.006', '<i class=\'fa fa-book\'></i>', '2017-08-21 02:47:39', 'SUDAH', 'FALSE', 'pmb_create', '17.4.006');
INSERT INTO `_notifikasi` VALUES ('440', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'BELUM', 'TRUE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('441', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'BELUM', 'FALSE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('442', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'SUDAH', 'FALSE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('443', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'BELUM', 'TRUE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('444', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'BELUM', 'TRUE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('445', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ANNISA OKTAVIANA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.007', '<i class=\'fa fa-book\'></i>', '2017-08-21 04:08:11', 'SUDAH', 'FALSE', 'pmb_create', '17.4.007');
INSERT INTO `_notifikasi` VALUES ('446', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'BELUM', 'TRUE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('447', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'BELUM', 'FALSE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('448', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'BELUM', 'FALSE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('449', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'BELUM', 'TRUE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('450', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'BELUM', 'TRUE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('451', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.008', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:21:29', 'SUDAH', 'FALSE', 'pmb_create', '17.4.008');
INSERT INTO `_notifikasi` VALUES ('452', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'BELUM', 'TRUE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('453', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'BELUM', 'FALSE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('454', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'SUDAH', 'FALSE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('455', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'BELUM', 'TRUE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('456', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'BELUM', 'TRUE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('457', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI WIDYAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.009', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:29:00', 'BELUM', 'FALSE', 'pmb_create', '17.4.009');
INSERT INTO `_notifikasi` VALUES ('458', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('459', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'BELUM', 'FALSE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('460', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'SUDAH', 'FALSE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('461', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('462', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('463', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>UMMATUM MUQTASHIDAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.010', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:36:28', 'BELUM', 'FALSE', 'pmb_create', '17.4.010');
INSERT INTO `_notifikasi` VALUES ('464', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'BELUM', 'TRUE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('465', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'BELUM', 'FALSE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('466', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'SUDAH', 'FALSE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('467', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'BELUM', 'TRUE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('468', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'BELUM', 'TRUE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('469', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SENDHI INDRIANA SARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.011', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:42:42', 'BELUM', 'FALSE', 'pmb_create', '17.4.011');
INSERT INTO `_notifikasi` VALUES ('470', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'BELUM', 'TRUE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('471', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'BELUM', 'FALSE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('472', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'SUDAH', 'FALSE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('473', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'BELUM', 'TRUE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('474', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'BELUM', 'TRUE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('475', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RENNY MANDA IRFANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.012', '<i class=\'fa fa-book\'></i>', '2017-08-28 03:49:09', 'SUDAH', 'FALSE', 'pmb_create', '17.4.012');
INSERT INTO `_notifikasi` VALUES ('476', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'BELUM', 'TRUE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('477', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'BELUM', 'FALSE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('478', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'SUDAH', 'FALSE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('479', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'BELUM', 'TRUE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('480', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'BELUM', 'TRUE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('481', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>Aminatus sa\'diyah</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.013', '<i class=\'fa fa-book\'></i>', '2017-08-29 11:44:54', 'BELUM', 'FALSE', 'pmb_create', '17.4.013');
INSERT INTO `_notifikasi` VALUES ('482', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('483', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'BELUM', 'FALSE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('484', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'SUDAH', 'FALSE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('485', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('486', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('487', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NAFIRA FITRIANI BAHTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.014', '<i class=\'fa fa-book\'></i>', '2017-08-30 09:53:53', 'SUDAH', 'FALSE', 'pmb_create', '17.4.014');
INSERT INTO `_notifikasi` VALUES ('488', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'BELUM', 'TRUE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('489', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'BELUM', 'FALSE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('490', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'SUDAH', 'FALSE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('491', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'BELUM', 'TRUE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('492', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'BELUM', 'TRUE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('493', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ENGELBERTUS STEVENSON.B</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.015', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:00:57', 'SUDAH', 'FALSE', 'pmb_create', '17.4.015');
INSERT INTO `_notifikasi` VALUES ('494', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'BELUM', 'TRUE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('495', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'SUDAH', 'FALSE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('496', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'SUDAH', 'FALSE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('497', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'BELUM', 'TRUE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('498', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'BELUM', 'TRUE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('499', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MARIANA RAMOS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.025', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:05:01', 'BELUM', 'FALSE', 'pmb_create', '17.3.025');
INSERT INTO `_notifikasi` VALUES ('500', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'BELUM', 'TRUE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('501', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'BELUM', 'FALSE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('502', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'SUDAH', 'FALSE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('503', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'BELUM', 'TRUE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('504', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'BELUM', 'TRUE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('505', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANCISCA RODRIGUES VARELA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.016', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:21:52', 'SUDAH', 'FALSE', 'pmb_create', '17.4.016');
INSERT INTO `_notifikasi` VALUES ('506', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('507', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'BELUM', 'FALSE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('508', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'SUDAH', 'FALSE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('509', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('510', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('511', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>KHOIRUS SHOLEH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.017', '<i class=\'fa fa-book\'></i>', '2017-08-30 10:47:01', 'BELUM', 'FALSE', 'pmb_create', '17.4.017');
INSERT INTO `_notifikasi` VALUES ('512', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('513', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'BELUM', 'FALSE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('514', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'SUDAH', 'FALSE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('515', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('516', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'BELUM', 'TRUE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('517', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NYOMAN ROIATUL NINGSIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.018', '<i class=\'fa fa-book\'></i>', '2017-08-30 04:18:28', 'SUDAH', 'FALSE', 'pmb_create', '17.4.018');
INSERT INTO `_notifikasi` VALUES ('518', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'BELUM', 'TRUE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('519', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'BELUM', 'FALSE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('520', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'SUDAH', 'FALSE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('521', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'BELUM', 'TRUE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('522', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'BELUM', 'TRUE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('523', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HUSNULAFIFA RUMATIGA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.062', '<i class=\'fa fa-book\'></i>', '2017-08-31 10:54:54', 'SUDAH', 'FALSE', 'pmb_create', '17.1.062');
INSERT INTO `_notifikasi` VALUES ('524', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'BELUM', 'TRUE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('525', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'BELUM', 'FALSE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('526', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'SUDAH', 'FALSE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('527', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'BELUM', 'TRUE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('528', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'BELUM', 'TRUE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('529', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SYNTYA DEWI IRIANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.019', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:08:40', 'SUDAH', 'FALSE', 'pmb_create', '17.4.019');
INSERT INTO `_notifikasi` VALUES ('530', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('531', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'BELUM', 'FALSE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('532', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'SUDAH', 'FALSE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('533', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('534', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'BELUM', 'TRUE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('535', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SURYA RISKA SUKARIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.020', '<i class=\'fa fa-book\'></i>', '2017-08-31 11:23:01', 'SUDAH', 'FALSE', 'pmb_create', '17.4.020');
INSERT INTO `_notifikasi` VALUES ('536', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('537', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'BELUM', 'FALSE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('538', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'SUDAH', 'FALSE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('539', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('540', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('541', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VISTA WULANDARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.021', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:07:20', 'SUDAH', 'FALSE', 'pmb_create', '17.4.021');
INSERT INTO `_notifikasi` VALUES ('542', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'BELUM', 'TRUE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('543', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'BELUM', 'FALSE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('544', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'SUDAH', 'FALSE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('545', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'BELUM', 'TRUE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('546', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'BELUM', 'TRUE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('547', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HANIK AYU ANGGRITA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.022', '<i class=\'fa fa-book\'></i>', '2017-08-31 01:12:45', 'SUDAH', 'FALSE', 'pmb_create', '17.4.022');
INSERT INTO `_notifikasi` VALUES ('548', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'BELUM', 'TRUE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('549', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'BELUM', 'FALSE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('550', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'SUDAH', 'FALSE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('551', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'BELUM', 'TRUE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('552', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'BELUM', 'TRUE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('553', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ROHMAD BUDIONO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.2.013', '<i class=\'fa fa-book\'></i>', '2017-09-04 09:15:43', 'BELUM', 'FALSE', 'pmb_create', '17.2.013');
INSERT INTO `_notifikasi` VALUES ('554', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'BELUM', 'TRUE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('555', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'BELUM', 'FALSE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('556', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'SUDAH', 'FALSE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('557', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'BELUM', 'TRUE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('558', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'BELUM', 'TRUE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('559', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ZUMROTUL HANIFAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.023', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:25:10', 'BELUM', 'FALSE', 'pmb_create', '17.4.023');
INSERT INTO `_notifikasi` VALUES ('560', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('561', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'BELUM', 'FALSE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('562', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'SUDAH', 'FALSE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('563', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('564', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'BELUM', 'TRUE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('565', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>SITI KHOTIJAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.024', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:43:08', 'BELUM', 'FALSE', 'pmb_create', '17.4.024');
INSERT INTO `_notifikasi` VALUES ('566', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('567', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'BELUM', 'FALSE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('568', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'SUDAH', 'FALSE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('569', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('570', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('571', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PUTRI ARDIYANTI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.025', '<i class=\'fa fa-book\'></i>', '2017-09-04 10:50:20', 'BELUM', 'FALSE', 'pmb_create', '17.4.025');
INSERT INTO `_notifikasi` VALUES ('572', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'BELUM', 'TRUE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('573', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'SUDAH', 'FALSE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('574', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'SUDAH', 'FALSE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('575', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'BELUM', 'TRUE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('576', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'BELUM', 'TRUE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('577', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ADELINA PRISILLIA NINGTIAS</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.026', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:03:04', 'BELUM', 'FALSE', 'pmb_create', '17.4.026');
INSERT INTO `_notifikasi` VALUES ('578', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('579', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'BELUM', 'FALSE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('580', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'SUDAH', 'FALSE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('581', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('582', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'BELUM', 'TRUE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('583', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FENDI. S</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.027', '<i class=\'fa fa-book\'></i>', '2017-09-04 11:57:53', 'SUDAH', 'FALSE', 'pmb_create', '17.4.027');
INSERT INTO `_notifikasi` VALUES ('584', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'BELUM', 'TRUE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('585', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'BELUM', 'FALSE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('586', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'SUDAH', 'FALSE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('587', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'BELUM', 'TRUE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('588', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'BELUM', 'TRUE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('589', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>PANGESTU RAHMASARI </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.063', '<i class=\'fa fa-book\'></i>', '2017-09-04 02:25:56', 'SUDAH', 'FALSE', 'pmb_create', '17.1.063');
INSERT INTO `_notifikasi` VALUES ('590', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'BELUM', 'TRUE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('591', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'BELUM', 'FALSE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('592', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'SUDAH', 'FALSE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('593', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'BELUM', 'TRUE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('594', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'BELUM', 'TRUE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('595', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>FRANSISKA. TOISUTA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.064', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:29:39', 'BELUM', 'FALSE', 'pmb_create', '17.1.064');
INSERT INTO `_notifikasi` VALUES ('596', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'BELUM', 'TRUE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('597', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'BELUM', 'FALSE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('598', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'SUDAH', 'FALSE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('599', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'BELUM', 'TRUE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('600', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'BELUM', 'TRUE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('601', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUKMAWATI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.065', '<i class=\'fa fa-book\'></i>', '2017-09-04 03:51:11', 'SUDAH', 'FALSE', 'pmb_create', '17.1.065');
INSERT INTO `_notifikasi` VALUES ('602', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('603', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'BELUM', 'FALSE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('604', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('605', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('606', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'BELUM', 'TRUE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('607', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>JACINTA DA CONCEICAO</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.066', '<i class=\'fa fa-book\'></i>', '2017-09-05 09:34:55', 'SUDAH', 'FALSE', 'pmb_create', '17.1.066');
INSERT INTO `_notifikasi` VALUES ('608', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'BELUM', 'TRUE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('609', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'BELUM', 'FALSE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('610', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'SUDAH', 'FALSE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('611', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'BELUM', 'TRUE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('612', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'BELUM', 'TRUE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('613', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NURUL HIDAYAT</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.028', '<i class=\'fa fa-book\'></i>', '2017-09-05 10:36:48', 'SUDAH', 'FALSE', 'pmb_create', '17.4.028');
INSERT INTO `_notifikasi` VALUES ('614', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'BELUM', 'TRUE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('615', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'BELUM', 'FALSE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('616', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'SUDAH', 'FALSE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('617', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'BELUM', 'TRUE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('618', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'BELUM', 'TRUE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('619', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MOHAMMAD ROMLI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.067', '<i class=\'fa fa-book\'></i>', '2017-09-05 11:51:08', 'SUDAH', 'FALSE', 'pmb_create', '17.1.067');
INSERT INTO `_notifikasi` VALUES ('620', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('621', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'BELUM', 'FALSE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('622', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'SUDAH', 'FALSE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('623', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('624', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'BELUM', 'TRUE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('625', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>HIKMAH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.029', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:05:05', 'SUDAH', 'FALSE', 'pmb_create', '17.4.029');
INSERT INTO `_notifikasi` VALUES ('626', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'BELUM', 'TRUE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('627', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'BELUM', 'FALSE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('628', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('629', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'BELUM', 'TRUE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('630', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'BELUM', 'TRUE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('631', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ISNAINIH</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.026', '<i class=\'fa fa-book\'></i>', '2017-09-05 01:14:22', 'SUDAH', 'FALSE', 'pmb_create', '17.3.026');
INSERT INTO `_notifikasi` VALUES ('632', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'BELUM', 'TRUE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('633', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'BELUM', 'FALSE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('634', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'SUDAH', 'FALSE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('635', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'BELUM', 'TRUE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('636', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'BELUM', 'TRUE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('637', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IKMAL RIYANDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.030', '<i class=\'fa fa-book\'></i>', '2017-09-05 02:52:46', 'SUDAH', 'FALSE', 'pmb_create', '17.4.030');
INSERT INTO `_notifikasi` VALUES ('638', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'BELUM', 'TRUE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('639', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'BELUM', 'FALSE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('640', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'SUDAH', 'FALSE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('641', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'BELUM', 'TRUE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('642', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'BELUM', 'TRUE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('643', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ACH DHANI KURNIAWAN</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.031', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:18:41', 'BELUM', 'FALSE', 'pmb_create', '17.4.031');
INSERT INTO `_notifikasi` VALUES ('644', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('645', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'BELUM', 'FALSE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('646', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'SUDAH', 'FALSE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('647', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('648', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'BELUM', 'TRUE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('649', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>ABDURRAHMAN WAHED</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.032', '<i class=\'fa fa-book\'></i>', '2017-09-05 03:20:20', 'BELUM', 'FALSE', 'pmb_create', '17.4.032');
INSERT INTO `_notifikasi` VALUES ('650', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'BELUM', 'TRUE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('651', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'BELUM', 'FALSE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('652', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'SUDAH', 'FALSE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('653', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'BELUM', 'TRUE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('654', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'BELUM', 'TRUE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('655', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>VIVI APRILIA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.3.027', '<i class=\'fa fa-book\'></i>', '2017-09-06 09:31:59', 'BELUM', 'FALSE', 'pmb_create', '17.3.027');
INSERT INTO `_notifikasi` VALUES ('656', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('657', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'BELUM', 'FALSE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('658', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'SUDAH', 'FALSE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('659', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('660', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'BELUM', 'TRUE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('661', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAT SAHAR </b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.033', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:01:39', 'BELUM', 'FALSE', 'pmb_create', '17.4.033');
INSERT INTO `_notifikasi` VALUES ('662', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'BELUM', 'TRUE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('663', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'BELUM', 'FALSE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('664', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'SUDAH', 'FALSE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('665', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'BELUM', 'TRUE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('666', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'BELUM', 'TRUE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('667', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>DEDY ANDIKA PUTRA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.034', '<i class=\'fa fa-book\'></i>', '2017-09-06 11:24:47', 'BELUM', 'FALSE', 'pmb_create', '17.4.034');
INSERT INTO `_notifikasi` VALUES ('668', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'BELUM', 'TRUE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('669', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'BELUM', 'FALSE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('670', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'SUDAH', 'FALSE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('671', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'BELUM', 'TRUE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('672', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'BELUM', 'TRUE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('673', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>RUSTADI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.035', '<i class=\'fa fa-book\'></i>', '2017-09-06 12:21:19', 'BELUM', 'FALSE', 'pmb_create', '17.4.035');
INSERT INTO `_notifikasi` VALUES ('674', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'BELUM', 'TRUE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('675', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'BELUM', 'FALSE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('676', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'SUDAH', 'FALSE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('677', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'BELUM', 'TRUE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('678', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'BELUM', 'TRUE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('679', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>MAHMUDI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.4.036', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:53:33', 'SUDAH', 'FALSE', 'pmb_create', '17.4.036');
INSERT INTO `_notifikasi` VALUES ('680', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'BELUM', 'TRUE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('681', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'SUDAH', 'FALSE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('682', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'SUDAH', 'FALSE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('683', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'BELUM', 'TRUE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('684', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'BELUM', 'TRUE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('685', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>NARTI RUMBIAA</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.068', '<i class=\'fa fa-book\'></i>', '2017-09-08 10:54:29', 'BELUM', 'FALSE', 'pmb_create', '17.1.068');
INSERT INTO `_notifikasi` VALUES ('686', '04.06.012', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'BELUM', 'TRUE', 'pmb_create', '17.1.069');
INSERT INTO `_notifikasi` VALUES ('687', 'dedy', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'BELUM', 'FALSE', 'pmb_create', '17.1.069');
INSERT INTO `_notifikasi` VALUES ('688', 'deni', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'SUDAH', 'FALSE', 'pmb_create', '17.1.069');
INSERT INTO `_notifikasi` VALUES ('689', 'dyah', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'BELUM', 'TRUE', 'pmb_create', '17.1.069');
INSERT INTO `_notifikasi` VALUES ('690', 'suwondo', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'BELUM', 'TRUE', 'pmb_create', '17.1.069');
INSERT INTO `_notifikasi` VALUES ('691', 'trimawati', 'Pendaftaran Mahasiswa Baru.', 'Pendaftaran Calon Mahasiswa Baru atas nama <b>IIN RUWAYARI</b>', 'content.php?module=admin&component=mahasiswa_pendaftaran&action=detail&nomor=17.1.069', '<i class=\'fa fa-book\'></i>', '2017-09-08 11:13:24', 'SUDAH', 'FALSE', 'pmb_create', '17.1.069');

-- ----------------------------
-- Table structure for _pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `_pembayaran`;
CREATE TABLE `_pembayaran` (
  `id_pembayaran` varchar(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _pembayaran
-- ----------------------------

-- ----------------------------
-- Table structure for _pembayaran_detail
-- ----------------------------
DROP TABLE IF EXISTS `_pembayaran_detail`;
CREATE TABLE `_pembayaran_detail` (
  `id_pembayaran_detail` int(10) NOT NULL AUTO_INCREMENT,
  `id_pembayaran` varchar(20) DEFAULT NULL,
  `tahun` char(4) DEFAULT NULL,
  `bulan` char(2) DEFAULT NULL,
  `id_jenis_bayar` varchar(20) DEFAULT NULL,
  `status_jenis_bayar` enum('UMUM','KHUSUS') DEFAULT NULL,
  `nominal_beban` double DEFAULT NULL,
  `nominal_bayar` double DEFAULT NULL,
  `status_dispensasi` enum('TRUE','FALSE') DEFAULT 'FALSE',
  `angsuran_ke` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id_pembayaran_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _pembayaran_detail
-- ----------------------------

-- ----------------------------
-- Table structure for _semester_ajaran
-- ----------------------------
DROP TABLE IF EXISTS `_semester_ajaran`;
CREATE TABLE `_semester_ajaran` (
  `id_semester_ajaran` varchar(20) NOT NULL,
  `tahun_ajaran` varchar(30) NOT NULL,
  `semester` enum('GANJIL','GENAP') NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  PRIMARY KEY (`id_semester_ajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _semester_ajaran
-- ----------------------------
INSERT INTO `_semester_ajaran` VALUES ('20151', '2015', 'GANJIL', '2015-07-01', '2015-12-31');
INSERT INTO `_semester_ajaran` VALUES ('20152', '2015', 'GENAP', '2016-01-01', '2016-06-30');
INSERT INTO `_semester_ajaran` VALUES ('20161', '2016', 'GANJIL', '2016-07-01', '2016-12-31');
INSERT INTO `_semester_ajaran` VALUES ('20162', '2016', 'GENAP', '2017-01-01', '2017-06-30');
INSERT INTO `_semester_ajaran` VALUES ('20171', '2017', 'GANJIL', '2017-07-01', '2017-12-31');
INSERT INTO `_semester_ajaran` VALUES ('20172', '2017', 'GENAP', '2018-01-01', '2018-06-30');
INSERT INTO `_semester_ajaran` VALUES ('20181', '2018', 'GANJIL', '2018-07-01', '2018-12-31');
INSERT INTO `_semester_ajaran` VALUES ('20182', '2018', 'GENAP', '2019-01-01', '2018-06-30');

-- ----------------------------
-- Table structure for _setting
-- ----------------------------
DROP TABLE IF EXISTS `_setting`;
CREATE TABLE `_setting` (
  `instansi_id` varchar(10) NOT NULL,
  `instansi_nama` varchar(100) DEFAULT NULL,
  `instansi_nama_perusahaan` varchar(100) DEFAULT NULL,
  `instansi_alamat` text,
  `instansi_telepon` varchar(50) DEFAULT NULL,
  `instansi_fax` varchar(50) DEFAULT NULL,
  `instansi_kota` varchar(50) DEFAULT NULL,
  `instansi_logo` varchar(200) DEFAULT NULL,
  `instansi_kepsek` varchar(100) DEFAULT NULL,
  `instansi_kepsek_nip` varchar(20) DEFAULT NULL,
  `web_title` varchar(200) DEFAULT NULL,
  `web_favicon` varchar(200) DEFAULT NULL,
  `web_header` varchar(200) DEFAULT NULL,
  `web_footer` varchar(200) DEFAULT NULL,
  `web_background` varchar(200) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  PRIMARY KEY (`instansi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _setting
-- ----------------------------
INSERT INTO `_setting` VALUES ('073125', 'Sistem Pembayaran Sekolah | SIPS', 'Sistem Pembayaran Sekolah | SIPS', '-', '-', '-', 'BANYUWANGI', 'images/1 a.png', 'Akhwan, S.Pd', '', 'Sistem Pembayaran Sekolah | SIPS', 'images/fav-1 a.png', 'Sistem Pembayaran Sekolah | SIPS', 'Sistem Pembayaran Sekolah | SIPS', 'images/bg-background.png', null);

-- ----------------------------
-- Table structure for _siswa
-- ----------------------------
DROP TABLE IF EXISTS `_siswa`;
CREATE TABLE `_siswa` (
  `nis` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `id_kelas` varchar(10) DEFAULT NULL,
  `status` enum('AKTIF','LULUS') DEFAULT 'AKTIF'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _siswa
-- ----------------------------

-- ----------------------------
-- Table structure for _siswa_history
-- ----------------------------
DROP TABLE IF EXISTS `_siswa_history`;
CREATE TABLE `_siswa_history` (
  `id_siswa_history` int(10) NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) DEFAULT NULL,
  `id_kelas_asal` varchar(10) DEFAULT NULL,
  `id_kelas_tujuan` varchar(10) DEFAULT NULL,
  `status` enum('AKTIF','LULUS') DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_history`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _siswa_history
-- ----------------------------

-- ----------------------------
-- Table structure for _tingkat
-- ----------------------------
DROP TABLE IF EXISTS `_tingkat`;
CREATE TABLE `_tingkat` (
  `id_tingkat` varchar(10) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tingkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _tingkat
-- ----------------------------
INSERT INTO `_tingkat` VALUES ('7', '7');
INSERT INTO `_tingkat` VALUES ('8', '8');
INSERT INTO `_tingkat` VALUES ('9', '9');
SET FOREIGN_KEY_CHECKS=1;

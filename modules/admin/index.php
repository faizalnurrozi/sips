<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

list($title, $logo, $fav, $bg, $header, $footer) = $db->result_row("SELECT web_title, instansi_logo, web_favicon, web_background, web_header, web_footer FROM _setting");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $title; ?></title>

		<!-- Favicon -->
		<link rel="icon" href="<?php echo $fav; ?>" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo $fav; ?>" type="image/x-icon" />
		<!-- End of Favicon -->
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap-glyphicons.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Notify.js -->
		<link href="includes/notify/notify-metro.css" rel="stylesheet" />
		<script src="includes/notify/notify.js"></script>
		<script src="includes/notify/notify-metro.js"></script>
		<!-- End Notify.js -->
		
		<!-- AutoComplete -->
		<script type="text/javascript" src="includes/autocomplete/jquery-1.4.js"></script>
		<script type="text/javascript" src="includes/autocomplete/jquery.autocomplete.js"></script>
		<link rel="stylesheet" type="text/css" href="includes/autocomplete/jquery.autocomplete.css" />
		<!-- End of AutoComplete -->
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- popup calendar -->
		<link rel="stylesheet" href="includes/popup-calendar/dhtmlgoodies_calendar.css" media="screen"></link>
		<script type="text/javascript" src="includes/popup-calendar/dhtmlgoodies_calendar.js"></script>
		<!-- End of popup calendar -->		
		
		
		<script type="text/javascript">
		var $j=jQuery.noConflict(true);
		var n=true;
		function flip_flop(){
			if(n==true){
				window.document.title='<?php echo $title; ?>';
				n=false;
			}else{
				window.document.title=document.getElementById('hdnotift').value;
				n=true;
			}
		}
		function notifikasi(){
			$('#notifications').load('content.php?module=general&component=notifications&action=notif&ajax=true');
			flip_flop();
			waktu = setTimeout('notifikasi()', 3000);
		}
		function notif(){
			$('#notify').load('content.php?module=general&component=notifications&action=notify&ajax=true');
			waktu = setTimeout('notif()', 3000);
		}
		
		function notify(judul, isi, style){
			$.notify({
				title: judul,
				text: isi,
				image: "<img src='images/icon-mail.png'/>"
			}, {
				style: 'metro',
				className: style,
				autoHide: false,
				clickToHide: true,
				globalPosition: 'bottom right'
			});
		}
		
		function autoResize(id){
			if(document.getElementById(id)){
				document.getElementById(id).height = document.getElementById(id).contentDocument .body.scrollHeight;
			}		
		}
		
		function cekPassword(a, b){
			var _target	= document.getElementById(a).value;
			var _value	= document.getElementById(b).value;
			var submit = document.getElementById('simpan');			
			if(_target != _value){
				document.getElementById('has-retype').className='form-group has-error';
				document.getElementById('error-validate').style.display='inline';
				submit.disabled=true;
			}else{
				document.getElementById('has-retype').className='form-group';
				document.getElementById('error-validate').style.display='none';
				submit.disabled=false;
			}
		}
		
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>
	</head>
	
	<?php
	if(isset($_SESSION[_APP_.'s_userAdmin']) && $_SESSION[_APP_.'s_userAdmin']!=""){
		if(!isset($_SESSION[_APP_.'s_adminPage'])) $page = "module=admin&component=home";
		else $page = $_SESSION[_APP_.'s_adminPage'];
		
		$foto = $_SESSION[_APP_.'s_fotoAdmin'];
		
		list($idJabatan, $namaJabatan) = $db->result_row("
		SELECT A.id_jabatan, B.nama FROM _karyawan AS A INNER JOIN _jabatan AS B ON (A.id_jabatan=B.id_jabatan) WHERE A.nip = '".$_SESSION[_APP_.'s_nipAdmin']."' 
		UNION
		SELECT A.id_jabatan, B.nama FROM _karyawan AS A INNER JOIN _jabatan AS B ON (A.id_jabatan=B.id_jabatan) WHERE A.nip = '".$_SESSION[_APP_.'s_nipAdmin']."' 
		");
		$_SESSION[_APP_.'s_idJabatan']	= $idJabatan;
		$_SESSION[_APP_.'s_namaJabatan']= $namaJabatan;
		
		list($lastDate, $lastTime) = $db->result_row("SELECT DATE_FORMAT(tanggal, '%d/%m/%Y') AS last_date, DATE_FORMAT(tanggal, '%h:%i') AS last_time FROM _admin_logs WHERE id_user = '".$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC LIMIT 1, 1 ");
	?>
	<body class="hold-transition skin-black sidebar-mini layout-boxed" onload="javascript: sendRequest('content.php', '<?php echo $page; ?>', 'content', 'div'); notifikasi(); notif();">	
		<div class="wrapper">
			<header class="main-header">
				<a href="index2.html" class="logo">
					<span class="logo-mini"><img src="<?php echo $logo; ?>" height="45" /></span>
					<span class="logo-lg"><?php echo $header; ?></span>
				</a>

				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">&nbsp;</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo $foto; ?>" class="user-image" alt="User Image">
									<span class="hidden-xs" id="nama_preload-index-1"><?php echo $_SESSION[_APP_.'s_namaAdmin']; ?></span>
								</a>
								<ul class="dropdown-menu">
									<li class="user-header">
										<img src="<?php echo $foto; ?>" class="img-circle" alt="User Image">
										<p id="nama_preload-index-2" style="margin-bottom:0px;"><?php echo $_SESSION[_APP_.'s_namaAdmin']; ?></p>
										<p style="color:yellow;font-size:10px;margin:0px 10px 0px 10px;"><?php echo $namaJabatan; ?></p>
									</li>
									<li class="user-footer">
										<div class="pull-left">
											<a href="#" class="btn btn-default btn-flat" onclick="javascript: sendRequest('content.php', 'module=admin&component=profil&action=main', 'content', 'div');"><i class="fa fa-user"></i> Profile</a>
										</div>
										<div class="pull-right">
											<a tabindex="-1" href="#logoutModal" role="button" data-toggle="modal" class="btn btn-warning btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			<aside class="main-sidebar">
				<section class="sidebar">
					<div class="user-panel">
						<div class="pull-left image" style="min-height:70px;">
							<img src="<?php echo $foto; ?>" class="img-circle" alt="User Image" style="width:100%; height:100%;" />
						</div>
						<div class="pull-left info" id="nama_preload-index-3">
							<p style="margin-bottom:5px;" id="nama_preload-parent"><?php echo $_SESSION[_APP_.'s_namaAdmin']; ?></p>
							<p style="font-size:12px;color:yellow;margin-bottom:5px;"><?php echo $namaJabatan; ?></p>
							<p style="font-size:10px;color:white;font-style:normal;">Login Terakhir : <?php echo $lastDate; echo " "; echo $lastTime; ?></p>
						</div>
					</div>
					
					<?php include "menu.php"; ?>
					
				</section>
			</aside>

			<div class="content-wrapper" id="content"></div>
			
			<footer class="main-footer">
			<?php echo $footer; ?>
			<a class="btn btn-flat btn-sm pull-right" style="margin-top:-10px;" onmouseover="$(this).tooltip();" title="Bantuan" data-toggle="modal" data-target="#modalFlowChart" onclick="javascript: document.getElementById('iframe_flowchart').src='content.php?module=admin&component=home&action=flowchart' "><i class="fa fa-question-circle fa-3x"></i></a>
			</footer>
			<div class="control-sidebar-bg"></div>
		</div>	
		
		<div class="modal fade" id="logoutModal" data-backdrop="static">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Logout</h4>
					</div>
					<div class="modal-body">
						Anda yakin ingin keluar ?
					</div>
					<div class="modal-footer">
						<button class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true" id="dismiss"><i class="fa fa-remove"></i> Batal</button>
						<button class="btn btn-danger btn-flat" data-dismiss="modal" onclick="javascript: sendRequest('content.php', 'module=admin&component=auth&action=logout', 'content', 'div');"><i class="fa fa-sign-out"></i> Keluar</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="modalFlowChart" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Flowchart</h4>
					</div>
					<div class="modal-body">
						<iframe name="iframe_flowchart" id="iframe_flowchart" width="100%" height="600" frameborder="0"></iframe>
					</div>
					<div class="modal-footer">
						<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-close"></i> Tutup</button>
					</div>
				</div>
			</div>
		</div>
	</body>
	<?php
	}else{
	?>
	<body class="hold-transition login-page" style="background-image:url(<?php echo $bg; ?>);">
		<div id="login">
			<?php include "modules/admin/components/auth/login.php"; ?>
		</div>
	</body>
	<?php
	}
	?>
</html>

<ul class="sidebar-menu">
	<li class="header">Main Menu</li>
	<li><a href="#" onclick="javascript: sendRequest('content.php', 'module=admin&component=home', 'content', 'div'); "><i class="fa fa-home"></i> <span>Dashboard</span></a></a></li>
	<?php
	/* Level 0 */
	$group = $_SESSION[_APP_.'s_idGroupAdmin'];
	$qData0 = $db->sql("SELECT A.id_admin_menus, A.nama, A.link, A.icon FROM _admin_menus AS A WHERE A.level = '0' AND A.id_admin_menus IN (SELECT B.id_admin_menus FROM _admin_menus_access AS B WHERE B.id_admin_group = '$group' ) ORDER BY A.urutan ASC");
	while(list($idMenus0, $nama0, $link0, $icon0) 	= $db->fetch_row($qData0)){
		
		/* Level 1 */		
		$qData1 = $db->sql("SELECT A.id_admin_menus, A.nama, A.link, A.icon FROM _admin_menus AS A WHERE A.level = '1' AND A.id_admin_menus_parent = '$idMenus0' AND A.id_admin_menus IN (SELECT B.id_admin_menus FROM _admin_menus_access AS B WHERE B.id_admin_group = '$group' ) ORDER BY A.urutan ASC");
		$jumData1 = $db->num_rows($qData1);
					
		echo "<li class'treeview'><a href='#' onclick=\"".stripslashes($link0)."\">".stripslashes($icon0)." <span>".str_replace(" ", "&nbsp;", $nama0)."</span> "; if($jumData1 > 0){ echo "<i class='fa fa-angle-left pull-right'></i>"; } echo "</a>";
		
		if($jumData1 != 0){
			echo "<ul class='treeview-menu'>";
			while(list($idMenus1, $nama1, $link1, $icon1) 	= $db->fetch_row($qData1)){
				
				/* Level 2 */						
				$qData2 = $db->sql("SELECT A.id_admin_menus, A.nama, A.link, A.icon FROM _admin_menus AS A WHERE A.level = '2' AND A.id_admin_menus_parent = '$idMenus1' AND A.id_admin_menus IN (SELECT B.id_admin_menus FROM _admin_menus_access AS B WHERE B.id_admin_group = '$group' ) ORDER BY A.urutan ASC");					
				$jumData2 = $db->num_rows($qData2);
				
				echo "<li class'treeview'><a href='#' onclick=\"".stripslashes($link1)."\">".stripslashes($icon1)." <span>".str_replace(" ", "&nbsp;", $nama1)."</span> "; if($jumData2 > 0){ echo "<i class='fa fa-angle-left pull-right'></i>"; } echo "</a>";
				if($jumData2 != 0){
					echo "<ul class='treeview-menu'>";
					while(list($idMenus2, $nama2, $link2, $icon2) = $db->fetch_row($qData2)){
						
						/* Level 3 */					
						$qData3 = $db->sql("SELECT A.id_admin_menus, A.nama, A.link, A.icon FROM _admin_menus AS A WHERE A.level = '3' AND A.id_admin_menus_parent = '$idMenus2' AND A.id_admin_menus IN (SELECT B.id_admin_menus FROM _admin_menus_access AS B WHERE B.id_admin_group = '$group' ) ORDER BY A.urutan ASC");
						$jumData3 = $db->num_rows($qData3);
						
						echo "<li class'treeview '><a href='#' onclick=\"".stripslashes($link2)."\">".stripslashes($icon2)." <span>".str_replace(" ", "&nbsp;", $nama2)."</span> "; if($jumData3 > 0){ echo "<i class='fa fa-angle-left pull-right'></i>"; } echo "</a>";
						
						if($jumData3 != 0){
							echo "<ul class='treeview-menu'>";
							while(list($idMenus3, $nama3, $link3, $icon3) = $db->fetch_row($qData3)){
								
								/* Level 4 */
								$qData4 = $db->sql("SELECT A.id_admin_menus, A.nama, A.link, A.icon FROM _admin_menus AS A WHERE A.level = '4' AND A.id_admin_menus_parent = '$idMenus3' AND A.id_admin_menus IN (SELECT B.id_admin_menus FROM _admin_menus_access AS B WHERE B.id_admin_group = '$group' ) ORDER BY A.urutan ASC");
								$jumData4 = $db->num_rows($qData4);									
								
								echo "<li class'treeview'><a href='#' onclick=\"".stripslashes($link3)."\">".stripslashes($icon3)." <span>".str_replace(" ", "&nbsp;", $nama3)."</span> "; if($jumData4 > 0){ echo "<i class='fa fa-angle-left pull-right'></i>"; } echo "</a>";
								if($jumData4 != 0){
									echo "<ul class='treeview-menu'>";
									while(list($idMenus4, $nama4, $link4, $icon4) 	= $db->fetch_row($qData4)){
																				
										echo "<li class'treeview'><a href='#' onclick=\"".stripslashes($link4)."\">".stripslashes($icon4)." <span>".str_replace(" ", "&nbsp;", $nama4)."</span> "; if($jumData5 > 0){ echo "<i class='fa fa-angle-left pull-right'></i>"; } echo "</a>";										
										echo "</li>";
									}
									echo "</ul>";
								}
								echo "</li>";
							}
							echo "</ul>";
						}
						echo "</li>";
					}
					echo "</ul>";
				}
				echo "</li>";
			}
			echo "</ul>";
		}
		echo "</li>";
	}
	?>
</ul>
<?php
if(@$_REQUEST['ajax']=='true'){
	session_start();
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
<?php } ?>
<!-- Alert Process -->

<?php
	/*Sorting*/
	if(isset($_POST['field'])){ $field = $_POST['field']; }else{ $field = ""; }
	if(isset($_POST['act'])){ $act = $_POST['act']; }else{ $act = ""; }
	if(isset($_POST['sort']) && $_POST['sort']=='reset'){
		$_SESSION[_APP_.'s_field_group'] = "id_admin_group";
		$_SESSION[_APP_.'s_sort_group'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}
	switch($field){
		case 'id_admin_group' : $_SESSION[_APP_.'s_field_group'] = "id_admin_group"; break;
		case 'nama' : $_SESSION[_APP_.'s_field_group'] = "nama"; break;
		default : 
			if(!isset($_SESSION[_APP_.'s_field_group'])){
				$_SESSION[_APP_.'s_field_group'] = "id_admin_group";
			}
			break;
	}
	if(!isset($_SESSION[_APP_.'s_sort_group'])){
		$_SESSION[_APP_.'s_sort_group'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}else{
		switch($act){
			case 'sort' :
				if($_SESSION[_APP_.'s_sort_group'] == "ASC"){
					$_SESSION[_APP_.'s_sort_group'] = "DESC";
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}else if($_SESSION[_APP_.'s_sort_group'] == "DESC"){
					$_SESSION[_APP_.'s_sort_group'] = "ASC";
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}
				break;
			case 'paging' :
				if($_SESSION[_APP_.'s_sort_group'] == "ASC"){
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}else if($_SESSION[_APP_.'s_sort_group'] == "DESC"){
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}
				break;
		}
	}
	/*End Sorting*/
	
	if(@$_REQUEST['start']=='') $start = 0; else $start = $_REQUEST['start'];
	
	if(!isset($msc)) $msc = microtime(true);
	@$keyword = @$_REQUEST['keyword'];
	$qSQL 	= "SELECT * FROM _admin_group WHERE (nama LIKE :keyword) ORDER BY ".$_SESSION[_APP_.'s_field_group']." ".$_SESSION[_APP_.'s_sort_group'];
	$hqSQL 	= $db->query($qSQL);
	$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT $start, $limit";
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th width="20%" class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true&act=sort&field=id_admin_group&keyword=<?php echo $keyword; ?>', 'list', 'div');">Kode&nbsp;<?php if($_SESSION[_APP_.'s_field_group'] == 'id_admin_group') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true&act=sort&field=nama&keyword=<?php echo $keyword; ?>', 'list', 'div');">Group&nbsp;<?php if($_SESSION[_APP_.'s_field_group'] == 'nama') echo $iconsort; ?></th>
				<th width="60">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='4' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['id_admin_group'], $keyword)."</td>";			
					echo "<td align='left'>".$func->highlight($hasil['nama'], $keyword)."</td>";
					
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
					
					/* Akses menu View */
					if($func->akses('R') == true){
						echo "<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modal-view' onclick=\"javascript:  document.getElementById('iframe_view').src='content.php?module=admin&component=admin&action=view&id=$hasil[id_admin_group]'; \" onmouseover=\"$(this).tooltip();\" title='View'><i class='fa fa-search'></i> </button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i> </button>";
					}
					/* End Akses menu View */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#myModal' onclick=\"javascript: document.getElementById('iframe_admin').src='content.php?module=admin&component=admin&action=add&id=$hasil[id_admin_group]'; \" onmouseover=\"$(this).tooltip();\" title='Edit'><i class='fa fa-edit'></i> </button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i> </button>";
					}
					/* End Akses menu edit */
					
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($start-$limit)."&keyword=$keyword', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($a*$limit)."&keyword=$keyword', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($start+$limit)."&keyword=$keyword', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>


<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300" frameborder="0" onload="javascript: autoResize('iframe_view')"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="idx" value="" />
<div class="modal fade" id="modal-reset" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Reset Password</h4>
			</div>
			<div class="modal-body">
				Password akan di reset sesuai dengan username nya.<br>
				Apakah anda yakin ingin reset password untuk user <b id="bnama"></b> ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=process&proc=reset&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('idx').value, 'list', 'div');"><i class="fa fa-remove"></i> Reset Password</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->
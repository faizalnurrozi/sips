<?php  
session_start();
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'profil' :		
		//Tangkap variabel		
		$usernames 	= @$_POST['username'];		
		$nama		= @$_POST['nama'];	
		$telepon	= @$_POST['txttelp'];
		
		list($nip)	= $db->result_row("SELECT nip FROM _admin WHERE usernames = '$usernames'");
		
		$qData = $db->update("_karyawan", array('nama' => $nama, 'telepon' => $telepon), array('nip' => $nip));
		if($qData==true){
			$_SESSION[_APP_.'s_message_info'] = "Profil berhasil di ganti.";
			$_SESSION[_APP_.'s_namaAdmin'] = $nama;
			echo "
			<script>
				document.getElementById('nama_preload-index-1').innerHTML='$nama';
				document.getElementById('nama_preload-index-2').innerHTML='$nama';
				document.getElementById('nama_preload-parent').innerHTML='$nama';
			</script>
			";
		}else $_SESSION[_APP_.'s_message_error'] = "Profil gagal di ganti.";
		
		echo "
			<script>
				sendRequest('content.php', 'module=admin&component=profil&action=form_profil&ajax=true', 'settings', 'div');
			</script>
		";
		break;
	
	case 'password' :	
		$username		= $_SESSION[_APP_.'s_userAdmin'];
		$passlama		= $func->encrypt_md5($_REQUEST['passlama']);
		$passbaru		= $func->encrypt_md5($_REQUEST['passbaru']);
		$passbaruretype	= $func->encrypt_md5($_REQUEST['passbaruretype']);
		
		$queryCek = $db->sql("SELECT COUNT(*) FROM _admin WHERE (passwords = '$passlama' OR passwords = '".$_REQUEST['passlama']."') AND usernames = '$username' ");
		list($cek)	= $db->fetch_row($queryCek);
		
		if($cek == 0){
			$_SESSION[_APP_.'s_message_error'] = "Ganti password gagal, password tidak sama dengan sebelumnya.";
		}else{
			if($passbaru != $passbaruretype){
				$_SESSION[_APP_.'s_message_error'] = "Password baru tidak cocok";
			}else{
				$hqData = $db->update("_admin", array('passwords' => $passbaru), array('usernames' => $username));
				if($hqData==true){
					$_SESSION[_APP_.'s_message_info'] = "Password telah berhasil diubah.";
				}else $_SESSION[_APP_.'s_message_error'] = "Password gagal diubah.";
			}
		}
		
		echo "
			<script>
				sendRequest('content.php', 'module=admin&component=profil&action=form_password&ajax=true', 'password', 'div');
			</script>
		";
		break;
		
	case "upload_foto":
		$usernames 	= @$_SESSION[_APP_.'s_userAdmin'];
		if($func->is_image(@$_FILES['txtfoto']) == true){
			$asal		= @$_FILES['txtfoto']['tmp_name'];
			$tujuan		= "modules/admin/photos/".uniqid().$_FILES['txtfoto']['name'];
			if(move_uploaded_file($asal, $tujuan)==true){		
				$hqData = $db->sql("UPDATE _admin SET icon = '$tujuan' WHERE usernames = '$usernames'");
				if($hqData==true){
					$_SESSION[_APP_.'s_fotoAdmin'] = $tujuan;
					$_SESSION[_APP_.'s_message_info'] = "Foto telah berhasil diubah.";
				}else $_SESSION[_APP_.'s_message_error'] = "Foto gagal diubah.";
			
				echo "
					<script>
						window.top.document.location.reload();
					</script>
				";
			}
		}else{ 
			$_SESSION[_APP_.'s_message_error'] = "File sepertinya bukan Image.";
			echo "
				<script>
					window.location.href='content.php?module=admin&component=profil&action=foto';
				</script>
			";
		}
		break;
}
?>
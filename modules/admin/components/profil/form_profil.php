<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}else{
	global $db;
	global $func;
}
$qData = "
	SELECT A.*, B.nama, B.telepon FROM _admin AS A INNER JOIN _karyawan AS B ON (A.nip = B.nip) WHERE usernames = '".$_SESSION[_APP_.'s_userAdmin']."'";
$hqData = $db->sql($qData);
$result = $db->fetch_assoc($hqData);
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong><?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?></strong>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
<?php } ?>
<!-- Alert Process -->

<form class="form-horizontal" action="javascript: void(null);">
	<div class="form-group">
		<label for="txtusername" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="txtusername" id="txtusername" placeholder="Username"  value="<?php echo $result['usernames']; ?>" readonly />
		</div>
	</div>
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">Nama</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="txtnama" id="txtnama" placeholder="Nama" value="<?php echo $result['nama']; ?>" autocomplete="off" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">No. Telp</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="txttelp" id="txttelp" placeholder="Telepon" value="<?php echo $result['telepon']; ?>" autocomplete="off" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary btn-flat" onclick="javascript: sendRequest('content.php', 'module=admin&component=profil&action=process&proc=profil&username='+document.getElementById('txtusername').value+'&nama='+document.getElementById('txtnama').value+'&txttelp='+document.getElementById('txttelp').value, 'settings', 'div'); "><i class="fa fa-save"></i> Simpan</button>
		</div>
	</div>
</form>
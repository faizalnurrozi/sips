<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
$msc = microtime(true);

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=profil";
	
	$qData = "
	SELECT A.*, B.nama, B.telepon FROM _admin AS A LEFT JOIN _karyawan AS B ON (A.nip = B.nip) WHERE A.usernames = '".$_SESSION[_APP_.'s_userAdmin']."'";
	$hqData = $db->sql($qData);
	$result = $db->fetch_assoc($hqData);
	
	$foto = $_SESSION[_APP_.'s_fotoAdmin'];
?>

<section class="content-header">
	<h1>Profil</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> Profil</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3" id="profil">
			<?php include "profil.php"; ?>
		</div>
		
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#settings" data-toggle="tab">Pengaturan</a></li>
					<li><a href="#password" data-toggle="tab">Ganti Password</a></li>
					<li><a href="#foto" data-toggle="tab">Ganti Foto</a></li>
					<li><a href="#logs" data-toggle="tab">Login History</a></li>
				</ul>
				<div class="tab-content">
					<div class="active tab-pane" id="settings">
						<?php include "form_profil.php"; ?>
					</div>
					<div class="tab-pane" id="password">
						<?php include "form_password.php"; ?>
					</div>
					<div class="tab-pane" id="foto">
						<iframe src="content.php?module=admin&component=profil&action=foto" width="100%" height="300" frameborder="0" scrolling="auto"></iframe>
					</div>
					<div class="tab-pane" id="logs">
						<iframe src="content.php?module=admin&component=profil&action=logs" width="100%" height="500" frameborder="0" scrolling="auto"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>
<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include("globals/config.php");
		include("globals/functions.php");
		$db = new Database();
		$func = new Functions();
	}

	$proc 	= $_REQUEST['proc'];

	/**
	 * Prepare query substansial
	 */
						
	$tahun 	= $_SESSION['date_y'];
	$bulan 	= $_SESSION['date_m'];
	$nis 	= $_SESSION['s_nis'];
	
	list($id_tingkat) = $db->result_row("SELECT B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '$nis'");

	switch($proc){
		case "add":
			$id_jenis_bayar_umum	= $_POST['id_jenis_bayar_umum'];
			$nominal_detail_umum	= $_POST['nominal_detail_umum'];
			$tahun					= $_POST['tahun'];
			$bulan					= $_POST['bulan'];
			$status_dispensasi_umum = ($_POST['status_dispensasi_umum'] != '') ? $_POST['status_dispensasi_umum'] : 'FALSE';

			if(count($_SESSION['UMUM']['s_jenis_bayar']) > 0 && in_array($tahun, $_SESSION['UMUM']['s_tahun']) && in_array($bulan, $_SESSION['UMUM']['s_bulan']) && in_array($id_jenis_bayar_umum, $_SESSION['UMUM']['s_jenis_bayar'])){
				list($namaJenisBayar) = $db->result_row("SELECT jenis_bayar FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar_umum'");
				echo "<script>alert('Item bayar \'$namaJenisBayar\' sudah tercatat di periode $tahun ".$func->nama_bulan($bulan)."!');</script>";
			}else{

				$_SESSION['UMUM']['s_jenis_bayar'][] 		= $id_jenis_bayar_umum;
				$_SESSION['UMUM']['s_nominal'][] 			= $nominal_detail_umum;
				$_SESSION['UMUM']['s_status_dispensasi'][] 	= $status_dispensasi_umum;
				$_SESSION['UMUM']['s_tahun'][] 				= $tahun;
				$_SESSION['UMUM']['s_bulan'][] 				= $bulan;
				$_SESSION['UMUM']['s_angsuran_ke'][] 		= 1;

			}
		break;

		case "update":
			$posisi					= $_POST['posisi'];
			$id_jenis_bayar_umum	= $_POST['id_jenis_bayar_umum'];
			$nominal_detail_umum 	= $_POST['nominal_detail_umum'];
			$status_dispensasi_umum	= ($_POST['status_dispensasi_umum'] != '') ? $_POST['status_dispensasi_umum'] : 'FALSE';

			$_SESSION['UMUM']['s_jenis_bayar'][$posisi]			= $id_jenis_bayar_umum;
			$_SESSION['UMUM']['s_nominal'][$posisi] 			= $nominal_detail_umum;
			$_SESSION['UMUM']['s_status_dispensasi'][$posisi] 	= $status_dispensasi_umum;
		break;

		case "delete":
			$posisi = $_POST['posisi'];

			unset($_SESSION['UMUM']['s_jenis_bayar'][$posisi]);
			unset($_SESSION['UMUM']['s_nominal'][$posisi]);
			unset($_SESSION['UMUM']['s_status_dispensasi'][$posisi]);
			unset($_SESSION['UMUM']['s_angsuran_ke'][$posisi]);
			unset($_SESSION['UMUM']['s_tahun'][$posisi]);
			unset($_SESSION['UMUM']['s_bulan'][$posisi]);

			$_SESSION['UMUM']['s_jenis_bayar'] 			= array_values($_SESSION['UMUM']['s_jenis_bayar']);
			$_SESSION['UMUM']['s_nominal'] 				= array_values($_SESSION['UMUM']['s_nominal']);
			$_SESSION['UMUM']['s_status_dispensasi'] 	= array_values($_SESSION['UMUM']['s_status_dispensasi']);
			$_SESSION['UMUM']['s_angsuran_ke'] 			= array_values($_SESSION['UMUM']['s_angsuran_ke']);
			$_SESSION['UMUM']['s_tahun'] 				= array_values($_SESSION['UMUM']['s_tahun']);
			$_SESSION['UMUM']['s_bulan'] 				= array_values($_SESSION['UMUM']['s_bulan']);
		break;
	}
?>

<h4>1. List Pembayaran Umum</h4>
<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th width="30%" colspan="2">Periode</th>
			<th width="20%">Detail</th>
			<th width="10%">Dispensasi</th>
			<th width="20%">Nominal</th>
			<th width="10%">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
		<?php
			$no = 1; $i = 0;
			$total_nominal = 0;
			if(count($_SESSION['UMUM']['s_jenis_bayar']) > 0){
				foreach($_SESSION['UMUM']['s_jenis_bayar'] as $id_jenis_bayar_umum){
					$nominal = ($_SESSION['UMUM']['s_nominal'][$i] == '') ? 0 : $_SESSION['UMUM']['s_nominal'][$i];
					$status_dispensasi = ($_SESSION['UMUM']['s_status_dispensasi'][$i] == 'FALSE') ? 'Tidak' : 'Ya';
					$tahun = $_SESSION['UMUM']['s_tahun'][$i];
					$bulan = $_SESSION['UMUM']['s_bulan'][$i];

					if($proc == 'edit' & $_REQUEST['posisi'] == $i){

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td align='center' colspan='2'>$tahun ".$func->nama_bulan($bulan)."</td>";
						echo "	<td>";
						echo "		<select name='id_jenis_bayar_umum_edit' id='id_jenis_bayar_umum_edit' class='form-control input-sm' onchange=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=nominal_umum&act=_edit&id_jenis_bayar='+this.value, 'load-nominal-umum'); \">";
						$query_jenis_bayar_umum = $db->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_umum AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_umum) WHERE A.id_tingkat = '$id_tingkat' AND tahun = '$tahun' AND bulan = '$bulan' AND id_jenis_bayar_umum NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND B.tanggal LIKE '$tahun-$bulan-%' AND A.status_jenis_bayar = 'UMUM')");
						while($result_jenis_bayar_umum = $db->fetch_assoc($query_jenis_bayar_umum)){

							echo "<option value='$result_jenis_bayar_umum[id_jenis_bayar_umum]' "; if($result_jenis_bayar_umum['id_jenis_bayar'] == $id_jenis_bayar_umum) echo "selected"; echo ">$result_jenis_bayar_umum[jenis_bayar]</option>";

						}
						echo "		</select>";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<input type='checkbox' name='status_dispensasi_umum_edit' id='status_dispensasi_umum_edit' value='TRUE' title='Centang jika item tersebut sebagai dispensasi kepada siswa' onclick=\"javascript: if(this.checked == true){ document.getElementById('nominal_detail_umum_edit').readOnly=false; }else{ document.getElementById('nominal_detail_umum_edit').readOnly=true; }\" "; if($_SESSION['UMUM']['s_status_dispensasi'][$i] == 'TRUE') echo "checked"; echo " />";
						echo "	</td>";
						echo "	<td>";
						echo "		<input type='number' name='nominal_detail_umum_edit' id='nominal_detail_umum_edit' placeholder='...' value='$nominal' class='form-control input-sm' style='text-align: right;' "; if($_SESSION['UMUM']['s_status_dispensasi'][$i] == 'FALSE') echo "readonly"; echo ">";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<button type='button' class='btn btn-xs btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_umum&proc=update&posisi=$i&ajax=true&id_jenis_bayar_umum='+document.getElementById('id_jenis_bayar_umum_edit').value+'&nominal_detail_umum='+document.getElementById('nominal_detail_umum_edit').value+'&status_dispensasi_umum='+((document.querySelector('input[name=status_dispensasi_umum_edit]:checked')) ? document.querySelector('input[name=status_dispensasi_umum_edit]:checked').value : 'FALSE'), 'detail_umum'); \">&nbsp;<i class='fa fa-save'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}else{

						list($jenis_bayar) = $db->result_row("SELECT jenis_bayar FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar_umum'");

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td align='center' colspan='2'>$tahun ".$func->nama_bulan($bulan)."</td>";
						echo "	<td>$jenis_bayar</td>";
						echo "	<td align='center'>$status_dispensasi</td>";
						echo "	<td align='right'>Rp. ".number_format($nominal,0,',','.')."</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-xs btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_umum&proc=edit&ajax=true&posisi=$i', 'detail_umum'); \">&nbsp;<i class='fa fa-edit'></i>&nbsp;</button>";
						echo "			<button type='button' class='btn btn-xs btn-danger btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_umum&proc=delete&ajax=true&posisi=$i', 'detail_umum'); \">&nbsp;<i class='fa fa-minus'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}

					$total_nominal += $nominal;

					$no++; $i++;
				}
			}
		?>
		<tr>
			<td align="center"><?php echo $no; ?></td>
			<td>

				<select class="form-control input-sm" name="tahun" id="tahun" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=load_jenis_bayar_detail&nis='+document.getElementById('nis').value+'&date_y='+document.getElementById('tahun').value+'&date_m='+document.getElementById('bulan').value, 'load-siswa');">
					<option value="">-Pilih Tahun-</option>
					<?php
						for($tahun_loop = date("Y"); $tahun_loop >= date("Y")-2; $tahun_loop--){
							echo "<option value='$tahun_loop' "; if($tahun_loop == $tahun) echo "selected"; echo " >$tahun_loop</option>";
						}
					?>
				</select>
			</td>
			<td>
				<select class="form-control input-sm" name="bulan" id="bulan" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=load_jenis_bayar_detail&nis='+document.getElementById('nis').value+'&date_y='+document.getElementById('tahun').value+'&date_m='+document.getElementById('bulan').value, 'load-siswa');">
					<option value="">-Pilih Bulan-</option>
					<?php
						for($bulan_loop = 1; $bulan_loop <= 12; $bulan_loop++){
							echo "<option value='".$func->number_pad($bulan_loop,2)."' "; if($func->number_pad($bulan_loop,2) == $bulan) echo "selected"; echo " >".$func->nama_bulan($bulan_loop,2)."</option>";
						}
					?>
				</select>
			</td>
			<td>
				<span id="load-nominal-umum"></span>
				<select class="form-control input-sm" name="id_jenis_bayar_umum" id="id_jenis_bayar_umum" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=nominal_umum&id_jenis_bayar='+this.value, 'load-nominal-umum'); ">
					<option value="">-Jenis bayar-</option>
					<?php
						$query_jenis_bayar_umum = $db->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_umum AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_umum) WHERE A.id_tingkat = '$id_tingkat' AND tahun = '$tahun' AND bulan = '$bulan' AND id_jenis_bayar_umum NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND B.tanggal <= '$tahun-$bulan' AND A.status_jenis_bayar = 'UMUM')");
						while($result_jenis_bayar_umum = $db->fetch_assoc($query_jenis_bayar_umum)){

							echo "<option value='$result_jenis_bayar_umum[id_jenis_bayar_umum]'>$result_jenis_bayar_umum[jenis_bayar]</option>";

						}
					?>
				</select>
			</td>
			<td align="center">
				<input type="checkbox" name="status_dispensasi_umum" id="status_dispensasi_umum" value="TRUE" title="Centang jika item tersebut sebagai dispensasi kepada siswa" onclick="javascript: if(this.checked == true){ document.getElementById('nominal_detail_umum').readOnly=false; }else{ document.getElementById('nominal_detail_umum').readOnly=true; }" />
			</td>
			<td>
				<input type="number" name="nominal_detail_umum" id="nominal_detail_umum" class="form-control input-sm" placeholder="..." style="text-align: right;" readonly />
			</td>
			<td align="center">
				<button type="button" class="btn btn-sm btn-default btn-flat" onclick="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_umum&proc=add&ajax=true&id_jenis_bayar_umum='+document.getElementById('id_jenis_bayar_umum').value+'&nominal_detail_umum='+document.getElementById('nominal_detail_umum').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&status_dispensasi_umum='+((document.querySelector('input[name=status_dispensasi_umum]:checked')) ? document.querySelector('input[name=status_dispensasi_umum]:checked').value : 'FALSE'), 'detail_umum'); ">&nbsp;<i class="fa fa-plus"></i>&nbsp;</button>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="right"><b>Total Nominal</b></td>
			<td align="right"><b>Rp. <?php echo number_format($total_nominal,0,',','.') ?></b></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

function loadOptionSelectJenisBayarUmum($nis, $tahun, $bulan, $id_tingkat){
	$dbx = new Database();

	echo "<script>";

	/**
	 * Blok untuk jenis bayar umum
	 */
	
	echo "document.getElementById('id_jenis_bayar_umum').options.length = 0;";

	echo "var jenis_bayar_umum 		= document.getElementById('id_jenis_bayar_umum');";
	echo "var option_umum			= document.createElement('option');";
	echo "option_umum.text 			= '-Jenis bayar-';";
	echo "jenis_bayar_umum.add(option_umum, jenis_bayar_umum[0]);";
	
	$i = 1;
	$query_jenis_bayar_umum = $dbx->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_umum AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_umum) WHERE A.id_tingkat = '$id_tingkat' AND tahun = '$tahun' AND bulan = '$bulan' AND id_jenis_bayar_umum NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND tahun = '$tahun' AND bulan = '$bulan' AND A.status_jenis_bayar = 'UMUM')");
	while($result_jenis_bayar_umum = $dbx->fetch_assoc($query_jenis_bayar_umum)){

		echo "var jenis_bayar_umum 		= document.getElementById('id_jenis_bayar_umum');";
		echo "var option_umum			= document.createElement('option');";
		echo "option_umum.value 		= '$result_jenis_bayar_umum[id_jenis_bayar_umum]';";
		echo "option_umum.text 			= '$result_jenis_bayar_umum[jenis_bayar]';";
		echo "jenis_bayar_umum.add(option_umum, jenis_bayar_umum[$i]);";

		$i++;

	}

	/**
	 * Blok untuk jenis bayar khusus
	 */
	
	echo "document.getElementById('id_jenis_bayar_khusus').options.length = 0;";

	echo "var jenis_bayar_khusus 		= document.getElementById('id_jenis_bayar_khusus');";
	echo "var option_umum			= document.createElement('option');";
	echo "option_umum.text 			= '- Pilih jenis bayar-';";
	echo "jenis_bayar_khusus.add(option_umum, jenis_bayar_khusus[0]);";
	
	$i = 1;
	$query_jenis_bayar_khusus = $dbx->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_khusus AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_khusus) WHERE C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE nominal <= (SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A1 INNER JOIN _pembayaran AS B1 ON(A1.id_pembayaran = B1.id_pembayaran) WHERE A1.id_jenis_bayar = C.id_jenis_bayar_khusus AND B1.nis = '$nis')) AND C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND status_dispensasi = 'TRUE' GROUP BY id_jenis_bayar)");
	while($result_jenis_bayar_khusus = $dbx->fetch_assoc($query_jenis_bayar_khusus)){

		echo "var jenis_bayar_khusus 	= document.getElementById('id_jenis_bayar_khusus');";
		echo "var option_umum			= document.createElement('option');";
		echo "option_umum.value 		= '$result_jenis_bayar_khusus[id_jenis_bayar_khusus]';";
		echo "option_umum.text 			= '$result_jenis_bayar_khusus[jenis_bayar_khusus]';";
		echo "jenis_bayar_khusus.add(option_umum, jenis_bayar_khusus[$i]);";

		$i++;

	}

	echo "</script>";
}

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_pembayaran 	= $_POST['id_pembayaran'];
		$tanggal 		= $func->explode_date($_POST['tanggal']);
		$nis 			= $_POST['nis'];
		
		$hqData = $db->insert("_pembayaran", array('id_pembayaran' => $id_pembayaran, 'tanggal' => $tanggal, 'nis' => $nis));

		$i = 0;
		if(count($_SESSION['UMUM']['s_jenis_bayar']) > 0){
			foreach($_SESSION['UMUM']['s_jenis_bayar'] as $id_jenis_bayar_umum){
				$nominal_bayar		= ($_SESSION['UMUM']['s_nominal'][$i] == '') ? 0 : $_SESSION['UMUM']['s_nominal'][$i];
				$status_dispensasi 	= $_SESSION['UMUM']['s_status_dispensasi'][$i];
				$tahun 				= $_SESSION['UMUM']['s_tahun'][$i];
				$bulan 				= $_SESSION['UMUM']['s_bulan'][$i];
				$angsuran_ke 		= $_SESSION['UMUM']['s_angsuran_ke'][$i];
				list($nominal_beban) = $db->result_row("SELECT nominal FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar_umum'");

				$hqData = $db->insert("_pembayaran_detail", array('id_pembayaran' => $id_pembayaran, 'id_jenis_bayar' => $id_jenis_bayar_umum, 'tahun' => $tahun, 'bulan' => $bulan, 'status_jenis_bayar' => 'UMUM', 'nominal_beban' => $nominal_beban, 'nominal_bayar' => $nominal_bayar, 'status_dispensasi' => $status_dispensasi, 'angsuran_ke' => $angsuran_ke));

				$i++;
			}
		}

		$i = 0;
		if(count($_SESSION['KHUSUS']['s_jenis_bayar']) > 0){
			foreach($_SESSION['KHUSUS']['s_jenis_bayar'] as $id_jenis_bayar_khusus){
				$nominal_bayar		= ($_SESSION['KHUSUS']['s_nominal'][$i] == '') ? 0 : $_SESSION['KHUSUS']['s_nominal'][$i];
				$status_dispensasi 	= $_SESSION['KHUSUS']['s_status_dispensasi'][$i];
				$angsuran_ke 		= $_SESSION['KHUSUS']['s_angsuran_ke'][$i];
				list($nominal_beban) = $db->result_row("SELECT nominal FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar_khusus'");

				$hqData = $db->insert("_pembayaran_detail", array('id_pembayaran' => $id_pembayaran, 'id_jenis_bayar' => $id_jenis_bayar_khusus, 'status_jenis_bayar' => 'KHUSUS', 'nominal_beban' => $nominal_beban, 'nominal_bayar' => $nominal_bayar, 'status_dispensasi' => $status_dispensasi, 'angsuran_ke' => $angsuran_ke));

				$i++;
			}
		}

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_pembayaran", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=pembayaran&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_pembayaran 	= @$_POST['id_pembayaran'];
		$tanggal 		= $func->explode_date($_POST['tanggal']);
		$nis 			= $_POST['nis'];

		$db->delete("_pembayaran_detail", array('id_pembayaran' => $id_pembayaran));

		$i = 0;
		if(count($_SESSION['UMUM']['s_jenis_bayar']) > 0){
			foreach($_SESSION['UMUM']['s_jenis_bayar'] as $id_jenis_bayar_umum){
				$nominal_bayar		= ($_SESSION['UMUM']['s_nominal'][$i] == '') ? 0 : $_SESSION['UMUM']['s_nominal'][$i];
				$status_dispensasi 	= $_SESSION['UMUM']['s_status_dispensasi'][$i];
				$tahun 				= $_SESSION['UMUM']['s_tahun'][$i];
				$bulan 				= $_SESSION['UMUM']['s_bulan'][$i];
				$angsuran_ke 		= $_SESSION['UMUM']['s_angsuran_ke'][$i];
				list($nominal_beban) = $db->result_row("SELECT nominal FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar_umum'");

				$hqData = $db->insert("_pembayaran_detail", array('id_pembayaran' => $id_pembayaran, 'id_jenis_bayar' => $id_jenis_bayar_umum, 'tahun' => $tahun, 'bulan' => $bulan, 'status_jenis_bayar' => 'UMUM', 'nominal_beban' => $nominal_beban, 'nominal_bayar' => $nominal_bayar, 'status_dispensasi' => $status_dispensasi, 'angsuran_ke' => $angsuran_ke));

				$i++;
			}
		}

		$i = 0;
		if(count($_SESSION['KHUSUS']['s_jenis_bayar']) > 0){
			foreach($_SESSION['KHUSUS']['s_jenis_bayar'] as $id_jenis_bayar_khusus){
				$nominal_bayar		= ($_SESSION['KHUSUS']['s_nominal'][$i] == '') ? 0 : $_SESSION['KHUSUS']['s_nominal'][$i];
				$status_dispensasi 	= $_SESSION['KHUSUS']['s_status_dispensasi'][$i];
				$angsuran_ke 		= $_SESSION['KHUSUS']['s_angsuran_ke'][$i];
				list($nominal_beban) = $db->result_row("SELECT nominal FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar_khusus'");

				$hqData = $db->insert("_pembayaran_detail", array('id_pembayaran' => $id_pembayaran, 'id_jenis_bayar' => $id_jenis_bayar_khusus, 'status_jenis_bayar' => 'KHUSUS', 'nominal_beban' => $nominal_beban, 'nominal_bayar' => $nominal_bayar, 'status_dispensasi' => $status_dispensasi, 'angsuran_ke' => $angsuran_ke));

				$i++;
			}
		}
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=pembayaran&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_pembayaran", array('id_pembayaran' => $idx));
		$hqData = $db->delete("_pembayaran_detail", array('id_pembayaran' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_pembayaran", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=pembayaran&action=list&ajax=true");
		
		#---------- * ----------#
		break;

	case "nominal_umum":
		$id_jenis_bayar = $_REQUEST['id_jenis_bayar'];
		$act 			= $_REQUEST['act'];
		list($nominal) = $db->result_row("SELECT nominal FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar'");

		echo "<script>document.getElementById('nominal_detail_umum$act').value='$nominal';</script>";
		break;

	case "nominal_khusus":
		$id_jenis_bayar = $_REQUEST['id_jenis_bayar'];
		$act 			= $_REQUEST['act'];
		list($nominal) = $db->result_row("SELECT nominal FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar'");

		echo "<script>document.getElementById('nominal_detail_khusus$act').value='$nominal';</script>";
		break;

	case "load_jenis_bayar":
		$nis 	= $_REQUEST['nis'];
		$tahun 	= $_SESSION['date_y'];
		$bulan 	= $_SESSION['date_m'];

		/**
		 * Mengaktifkan session berdasarkan $nis
		 */
		
		$_SESSION['s_nis'] = $nis;

		/**
		 * Mencari tingkat dari siswa $nis
		 */
		
		list($id_tingkat) = $db->result_row("SELECT B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '".$_SESSION['s_nis']."'");


		/**
		 * Load jenis bayar umum
		 */
		
		loadOptionSelectJenisBayarUmum($nis, $tahun, $bulan, $id_tingkat);
		
		break;

	case "load_jenis_bayar_detail":
		$nis 	= $_REQUEST['nis'];
		$tahun 	= $_REQUEST['date_y'];
		$bulan 	= $_REQUEST['date_m'];

		/**
		 * Mengaktifkan session berdasarkan $nis
		 */
		
		$_SESSION['s_nis'] = $nis;

		/**
		 * Mencari tingkat dari siswa $nis
		 */
		
		list($id_tingkat) = $db->result_row("SELECT B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '".$_SESSION['s_nis']."'");


		/**
		 * Load jenis bayar umum
		 */
		
		loadOptionSelectJenisBayarUmum($nis, $tahun, $bulan, $id_tingkat);
		
		break;
}
?>
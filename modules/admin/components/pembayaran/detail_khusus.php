<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include("globals/config.php");
		include("globals/functions.php");
		$db = new Database();
		$func = new Functions();
	}

	$proc 	= $_REQUEST['proc'];

	/**
	 * Prepare query substansial
	 */
						
	$tahun 	= $_SESSION['date_y'];
	$bulan 	= $_SESSION['date_m'];
	$nis 	= $_SESSION['s_nis'];
	
	list($id_tingkat) = $db->result_row("SELECT B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '$nis'");

	switch($proc){
		case "add":
			$id_jenis_bayar_khusus		= $_POST['id_jenis_bayar_khusus'];
			$nominal_detail_khusus		= $_POST['nominal_detail_khusus'];
			$status_dispensasi_khusus 	= ($_POST['status_dispensasi_khusus'] != '') ? $_POST['status_dispensasi_khusus'] : 'FALSE';

			list($angsuran_ke) = $db->result_row("SELECT COUNT(angsuran_ke) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE A.id_jenis_bayar = '$id_jenis_bayar_khusus' AND B.nis = '$nis'");
			$angsuran_ke++;

			$_SESSION['KHUSUS']['s_jenis_bayar'][] 		= $id_jenis_bayar_khusus;
			$_SESSION['KHUSUS']['s_nominal'][] 			= $nominal_detail_khusus;
			$_SESSION['KHUSUS']['s_status_dispensasi'][]= $status_dispensasi_khusus;
			$_SESSION['KHUSUS']['s_angsuran_ke'][] 		= $angsuran_ke;
		break;

		case "update":
			$posisi						= $_POST['posisi'];
			$id_jenis_bayar_khusus		= $_POST['id_jenis_bayar_khusus'];
			$nominal_detail_khusus 		= $_POST['nominal_detail_khusus'];
			$status_dispensasi_khusus	= ($_POST['status_dispensasi_khusus'] != '') ? $_POST['status_dispensasi_khusus'] : 'FALSE';

			$_SESSION['KHUSUS']['s_jenis_bayar'][$posisi]		= $id_jenis_bayar_khusus;
			$_SESSION['KHUSUS']['s_nominal'][$posisi] 			= $nominal_detail_khusus;
			$_SESSION['KHUSUS']['s_status_dispensasi'][$posisi] = $status_dispensasi_khusus;
		break;

		case "delete":
			$posisi = $_POST['posisi'];

			unset($_SESSION['KHUSUS']['s_jenis_bayar'][$posisi]);
			unset($_SESSION['KHUSUS']['s_nominal'][$posisi]);
			unset($_SESSION['KHUSUS']['s_status_dispensasi'][$posisi]);
			unset($_SESSION['KHUSUS']['s_angsuran_ke'][$posisi]);

			$_SESSION['KHUSUS']['s_jenis_bayar'] 		= array_values($_SESSION['KHUSUS']['s_jenis_bayar']);
			$_SESSION['KHUSUS']['s_nominal'] 			= array_values($_SESSION['KHUSUS']['s_nominal']);
			$_SESSION['KHUSUS']['s_status_dispensasi'] 	= array_values($_SESSION['KHUSUS']['s_status_dispensasi']);
			$_SESSION['KHUSUS']['s_angsuran_ke'] 		= array_values($_SESSION['KHUSUS']['s_angsuran_ke']);
		break;
	}
?>

<h4>2. List Pembayaran Khusus</h4>
<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th width="50%">Detail</th>
			<th>Dispensasi</th>
			<th width="20%">Nominal</th>
			<th width="20%">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
		<?php
			$no = 1; $i = 0;
			$total_nominal = 0;
			if(count($_SESSION['KHUSUS']['s_jenis_bayar']) > 0){
				foreach($_SESSION['KHUSUS']['s_jenis_bayar'] as $id_jenis_bayar_khusus){
					$nominal = ($_SESSION['KHUSUS']['s_nominal'][$i] == '') ? 0 : $_SESSION['KHUSUS']['s_nominal'][$i];
					$status_dispensasi = ($_SESSION['KHUSUS']['s_status_dispensasi'][$i] == 'FALSE') ? 'Tidak' : 'Ya';
					$angsuran_ke = $_SESSION['KHUSUS']['s_angsuran_ke'][$i];

					if($proc == 'edit' & $_REQUEST['posisi'] == $i){

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>";
						echo "		<select name='id_jenis_bayar_khusus_edit' id='id_jenis_bayar_khusus_edit' class='form-control input-sm' onchange=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=nominal_khusus&act=_edit&id_jenis_bayar='+this.value, 'load-nominal-khusus'); \">";
						$query_jenis_bayar_khusus = $db->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_khusus AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_khusus) WHERE C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE nominal <= (SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A1 INNER JOIN _pembayaran AS B1 ON(A1.id_pembayaran = B1.id_pembayaran) WHERE A1.id_jenis_bayar = C.id_jenis_bayar_khusus AND B1.nis = '$nis')) AND C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND status_dispensasi = 'TRUE' GROUP BY id_jenis_bayar)");
						while($result_jenis_bayar_khusus = $db->fetch_assoc($query_jenis_bayar_khusus)){

							echo "<option value='$result_jenis_bayar_khusus[id_jenis_bayar_khusus]' "; if($result_jenis_bayar_khusus['id_jenis_bayar'] == $id_jenis_bayar_khusus) echo "selected"; echo ">$result_jenis_bayar_khusus[jenis_bayar_khusus]</option>";

						}
						echo "		</select>";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<input type='checkbox' name='status_dispensasi_khusus_edit' id='status_dispensasi_khusus_edit' value='TRUE' title='Centang jika item tersebut sebagai dispensasi kepada siswa' />";
						echo "	</td>";
						echo "	<td>";
						echo "		<input type='number' name='nominal_detail_khusus_edit' id='nominal_detail_khusus_edit' placeholder='...' value='$nominal' class='form-control input-sm' style='text-align: right;'>";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_khusus&proc=update&posisi=$i&ajax=true&id_jenis_bayar_khusus='+document.getElementById('id_jenis_bayar_khusus_edit').value+'&nominal_detail_khusus='+document.getElementById('nominal_detail_khusus_edit').value+'&status_dispensasi_khusus='+((document.querySelector('input[name=status_dispensasi_khusus_edit]:checked')) ? document.querySelector('input[name=status_dispensasi_khusus_edit]:checked').value : 'FALSE'), 'detail_khusus'); \">&nbsp;<i class='fa fa-save'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}else{

						list($jenis_bayar) = $db->result_row("SELECT jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar_khusus'");

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>$jenis_bayar <i><b>(Angsuran ke $angsuran_ke)</b></i></td>";
						echo "	<td align='center'>$status_dispensasi</td>";
						echo "	<td align='right'>Rp. ".number_format($nominal,0,',','.')."</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-sm' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_khusus&proc=edit&ajax=true&posisi=$i', 'detail_khusus'); \">&nbsp;<i class='fa fa-edit'></i>&nbsp;</button>";
						echo "			<button type='button' class='btn btn-sm btn-danger btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_khusus&proc=delete&ajax=true&posisi=$i', 'detail_khusus'); \">&nbsp;<i class='fa fa-minus'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}

					$total_nominal += $nominal;

					$no++; $i++;
				}
			}
		?>
		<tr>
			<td align="center"><?php echo $no; ?></td>
			<td>
				<span id="load-nominal-khusus"></span>
				<select class="form-control input-sm" name="id_jenis_bayar_khusus" id="id_jenis_bayar_khusus" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=process&proc=nominal_khusus&id_jenis_bayar='+this.value, 'load-nominal-khusus'); ">
					<option value="">- Pilih jenis bayar-</option>
					<?php
						$query_jenis_bayar_khusus = $db->sql("SELECT * FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_khusus AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_khusus) WHERE C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE nominal <= (SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A1 INNER JOIN _pembayaran AS B1 ON(A1.id_pembayaran = B1.id_pembayaran) WHERE A1.id_jenis_bayar = C.id_jenis_bayar_khusus AND B1.nis = '$nis')) AND C.id_jenis_bayar_khusus NOT IN(SELECT id_jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$nis' AND status_dispensasi = 'TRUE' GROUP BY id_jenis_bayar)");
						while($result_jenis_bayar_khusus = $db->fetch_assoc($query_jenis_bayar_khusus)){

							echo "<option value='$result_jenis_bayar_khusus[id_jenis_bayar_khusus]'>$result_jenis_bayar_khusus[jenis_bayar_khusus]</option>";

						}
					?>
				</select>
			</td>
			<td align="center">
				<input type="checkbox" name="status_dispensasi_khusus" id="status_dispensasi_khusus" value="TRUE" title="Centang jika item tersebut sebagai dispensasi kepada siswa" />
			</td>
			<td>
				<input type="number" name="nominal_detail_khusus" id="nominal_detail_khusus" class="form-control input-sm" placeholder="..." style="text-align: right;" />
			</td>
			<td align="center">
				<button type="button" class="btn btn-sm btn-default btn-flat" onclick="javascript: ajaxCustom('content.php', 'module=admin&component=pembayaran&action=detail_khusus&proc=add&ajax=true&id_jenis_bayar_khusus='+document.getElementById('id_jenis_bayar_khusus').value+'&nominal_detail_khusus='+document.getElementById('nominal_detail_khusus').value+'&status_dispensasi_khusus='+((document.querySelector('input[name=status_dispensasi_khusus]:checked')) ? document.querySelector('input[name=status_dispensasi_khusus]:checked').value : 'FALSE'), 'detail_khusus'); ">&nbsp;<i class="fa fa-plus"></i>&nbsp;</button>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="right"><b>Total Nominal</b></td>
			<td align="right"><b>Rp. <?php echo number_format($total_nominal,0,',','.') ?></b></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php ob_start(); ?>
<?php
	include "globals/config.php";
	include "globals/functions.php";

	$db = new Database();
	$func = new Functions();

	$id_pembayaran = $_REQUEST['id_pembayaran'];

	$query = $db->sql("SELECT A.*, B.nama AS siswa, B.jenis_kelamin, C.nama AS kelas, C.id_tingkat FROM _pembayaran AS A INNER JOIN _siswa AS B ON(A.nis = B.nis) INNER JOIN _kelas AS C ON(B.id_kelas = C.id_kelas) WHERE id_pembayaran = '$id_pembayaran'");
	$result = $db->fetch_assoc($query);
?>

<page backcolor="#FFF" backtop="5mm" backbottom="5mm" backleft="5mm" backright="5mm" style="font-size: 12px;">

<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width: 20mm;" rowspan="2" valign="top">
			<qrcode value="<?php echo $id_pembayaran; ?>" ec="L" style="width: 15mm;"></qrcode>
		</td>
		<td style="width: 150mm; font-size: 16px;" align="center"><b>KUITANSI PEMBAYARAN UANG SEKOLAH</b></td>
		<td style="width: 20mm;" rowspan="2"></td>
	</tr>
	<tr>
		<td valign="top">
			<table cellspacing="0" cellpadding="0" style="margin-top: 5mm;">
				<tr>
					<td valign="top">
						<table cellspacing="0" cellpadding="0" class="header">
							<tr>
								<td style="width: 23mm;">Nama Sekolah</td>
								<td style="width: 1mm;">:</td>
								<td style="width: 70mm;">SMP Amanatul Ummah</td>
							</tr>
							<tr>
								<td>No. ID Siswa</td>
								<td>:</td>
								<td><?php echo $result['nis']; ?></td>
							</tr>
							<tr>
								<td>Nama Siswa</td>
								<td>:</td>
								<td><?php echo $result['siswa']; ?></td>
							</tr>
							<tr>
								<td>Kelas</td>
								<td>:</td>
								<td><?php echo $result['kelas']; ?></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table cellspacing="0" cellpadding="0" class="header">
							<tr>
								<td style="width: 20mm;">No. Bukti</td>
								<td style="width: 1mm;">:</td>
								<td style="width: 30mm;"><?php echo $id_pembayaran; ?></td>
							</tr>
							<tr>
								<td>Tahun Ajaran</td>
								<td>:</td>
								<td><?php echo substr($result['tanggal'],0,4)." / ".(substr($result['tanggal'],0,4)+1); ?></td>
							</tr>
							<tr>
								<td>Periode</td>
								<td>:</td>
								<td><?php echo $func->nama_bulan(substr($result['tanggal'],5,2))." ".substr($result['tanggal'],0,4); ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<style type="text/css">
	.header tr td{
		padding-top: 0.7mm;
		padding-bottom: 0.7mm;
	}
</style>



<style type="text/css">
	.table tr th{
		padding: 1mm;
		text-align: center;
		border-top: 0.5px solid #000;
		border-bottom: 0.5px solid #000;
		text-align: left;
		background: #F2F2F2;
	}
	.table tr td{
		padding: 1mm;
	}
</style>

<?php
	$no_list = 1;

	$query_umum = $db->sql("SELECT A.*, B.jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _jenis_bayar_umum AS B ON(A.id_jenis_bayar = B.id_jenis_bayar_umum) WHERE id_pembayaran = '$id_pembayaran' AND status_jenis_bayar = 'UMUM'");
	$jumlah_umum = $db->num_rows($query_umum);

	$query_khusus = $db->sql("SELECT A.*, B.jenis_bayar_khusus FROM _pembayaran_detail AS A INNER JOIN _jenis_bayar_khusus AS B ON(A.id_jenis_bayar = B.id_jenis_bayar_khusus) WHERE id_pembayaran = '$id_pembayaran' AND status_jenis_bayar = 'KHUSUS'");
	$jumlah_khusus = $db->num_rows($query_khusus);
?>

<?php
	if($jumlah_umum > 0){
?>

<h5><?php echo $no_list; ?>. List Pembayaran Umum</h5>
<table cellpadding="0" cellspacing="0" class="table">
	<tr>
		<th style="width: 10mm; text-align: center;">No.</th>
		<th style="width: 30mm;">Periode</th>
		<th style="width: 70mm;">Rincian</th>
		<th style="width: 15mm;">Dispensasi</th>
		<th style="width: 45mm; text-align: right;">Nominal</th>
	</tr>
	<tbody>
	<?php
		$no = 1;
		$total = 0;
		while($result_umum = $db->fetch_assoc($query_umum)){
			echo "<tr>";
			echo "	<td style='width: 10mm;' align='center'>$no.</td>";
			echo "	<td style='width: 30mm;'>$result_umum[tahun] ".$func->nama_bulan($result_umum['bulan'])."</td>";
			echo "	<td style='width: 70mm;'>$result_umum[jenis_bayar]</td>";
			echo "	<td style='width: 15mm;' align='left'>".(($result_umum['status_dispensasi'] == 'TRUE') ? 'Ya' : 'Tidak')."</td>";
			echo "	<td style='width: 45mm;' align='right'>Rp. ".number_format($result_umum['nominal_bayar'],0,',','.')."</td>";
			echo "</tr>";

			$total += $result_umum['nominal_bayar'];

			$no++;
		}
	?>
		<tr>
			<th colspan="3">&nbsp;</th>
			<th><b>Total</b></th>
			<th align="right"><b><?php echo number_format($total,0,',','.') ?></b></th>
		</tr>
	</tbody>
</table>
<?php $no_list++; } ?>


<?php
	if($jumlah_khusus > 0){
?>
<h5><?php echo $no_list; ?>. List Pembayaran Khusus</h5>
<table cellpadding="0" cellspacing="0" class="table">
	<tr>
		<th style="width: 10mm; text-align: center;">No.</th>
		<th style="width: 30mm;">Angsuran Ke</th>
		<th style="width: 70mm;">Rincian</th>
		<th style="width: 15mm;">Dispensasi</th>
		<th style="width: 45mm; text-align: right;">Nominal</th>
	</tr>
	<tbody>
	<?php
		$no = 1;
		$total = 0;
		while($result_khusus = $db->fetch_assoc($query_khusus)){
			echo "<tr>";
			echo "	<td style='width: 10mm;' align='center'>$no.</td>";
			echo "	<td style='width: 30mm;'>$result_khusus[angsuran_ke]</td>";
			echo "	<td style='width: 70mm;'>$result_khusus[jenis_bayar_khusus]</td>";
			echo "	<td style='width: 15mm;' align='left'>".(($result_khusus['status_dispensasi'] == 'TRUE') ? 'Ya' : 'Tidak')."</td>";
			echo "	<td style='width: 45mm;' align='right'>Rp. ".number_format($result_khusus['nominal_bayar'],0,',','.')."</td>";
			echo "</tr>";

			$total += $result_khusus['nominal_bayar'];

			$no++;
		}
	?>
		<tr>
			<th colspan="3">&nbsp;</th>
			<th><b>Total</b></th>
			<th align="right"><b><?php echo number_format($total,0,',','.') ?></b></th>
		</tr>
	</tbody>
</table>
<?php } ?>
</page>



<page backcolor="#FFF" backtop="5mm" backbottom="10mm" backleft="10mm" backright="5mm">

<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width: 50mm;">
			<qrcode value="<?php echo $id_pembayaran; ?>" ec="L" style="width: 20mm;"></qrcode>
		</td>
		<td style="width: 130mm;" align="center"><b>KUITANSI PEMBAYARAN UANG SEKOLAH</b></td>
		<td style="width: 50mm;"></td>
	</tr>
</table>

<style type="text/css">
	.header tr td{
		padding-top: 0.7mm;
		padding-bottom: 0.7mm;
	}
</style>

<table cellspacing="0" cellpadding="0" style="margin-top: 5mm;">
	<tr>
		<td style="width: 115mm;" valign="top">
			<table cellspacing="0" cellpadding="0" class="header">
				<tr>
					<td style="width: 30mm;">Nama Sekolah</td>
					<td style="width: 1mm;">:</td>
					<td style="width: 84mm;">SMP Amanatul Ummah</td>
				</tr>
				<tr>
					<td>No. ID Siswa</td>
					<td>:</td>
					<td><?php echo $result['nis']; ?></td>
				</tr>
				<tr>
					<td>Nama Siswa</td>
					<td>:</td>
					<td><?php echo $result['siswa']; ?></td>
				</tr>
				<tr>
					<td>Kelas</td>
					<td>:</td>
					<td><?php echo $result['kelas']; ?></td>
				</tr>
			</table>
		</td>
		<td style="width: 115mm;" valign="top">
			<table cellspacing="0" cellpadding="0" class="header">
				<tr>
					<td style="width: 30mm;">No. Bukti</td>
					<td style="width: 1mm;">:</td>
					<td style="width: 84mm;"><?php echo $id_pembayaran; ?></td>
				</tr>
				<tr>
					<td>Tahun Ajaran</td>
					<td>:</td>
					<td><?php echo substr($result['tanggal'],0,4)." / ".(substr($result['tanggal'],0,4)+1); ?></td>
				</tr>
				<tr>
					<td>Periode</td>
					<td>:</td>
					<td><?php echo $func->nama_bulan(substr($result['tanggal'],5,2))." ".substr($result['tanggal'],0,4); ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table cellspacing="0" cellpadding="0" style="margin-top: 10mm;">
	<tr>
		<td style="width: 160mm;"></td>
		<td style="width: 60mm;" align="center">Surabaya, <?php echo $func->report_date(date("Y-m-d")); ?></td>
	</tr>
	<tr>
		<td></td>
		<td align="center">Bagian Keuangan</td>
	</tr>
	<tr>
		<td colspan="2"><br><br><br><br><br></td>
	</tr>
	<tr>
		<td></td>
		<td align="center">Akhwan, S.Pd</td>
	</tr>
</table>

</page>

<?php
	$content = ob_get_clean();    
	require_once('includes/html2pdf/html2pdf.class.php');

	try{
		$width_in_mm = 210;
		$height_in_mm = 148;
		$html2pdf = new HTML2PDF('L', array($height_in_mm, $width_in_mm), 'en', true, 'UTF-8', array(0, 0, 0, 0));
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('print.pdf');
	}catch(HTML2PDF_exception $e) {
		echo $e;
		exit;
	}
	?>
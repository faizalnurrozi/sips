<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php
		$result = $db->result_assoc("SELECT A.*, B.nama siswa, B.jenis_kelamin, C.nama AS kelas FROM _pembayaran AS A INNER JOIN _siswa AS B ON(A.nis = B.nis) INNER JOIN _kelas AS C ON(B.id_kelas = C.id_kelas) WHERE id_pembayaran = '$id'");
		?>
		<table class="table">
			<tr>
				<td width="30%">Kode&nbsp;Pembayaran</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['id_pembayaran']; ?></td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->report_date($result['tanggal']); ?></td>
			</tr>
			<tr>
				<td>Siswa</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['siswa']." (".$result['nis'].")"; ?></td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['kelas']; ?></td>
			</tr>
		</table>

		<div class="row">
			<div class="col-xs-6">
				<h4>1. List Pembayaran Umum</h4>
				<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th width="1%">No.</th>
							<th width="20%">Periode</th>
							<th width="40%">Detail</th>
							<th>Dispensasi</th>
							<th width="20%">Nominal</th>
						</tr>
					</thead>

					<tbody>
						<?php
							$no = 1;
							$total = 0;
							$query_umum = $db->sql("SELECT A.*, B.jenis_bayar FROM _pembayaran_detail AS A INNER JOIN _jenis_bayar_umum AS B ON(A.id_jenis_bayar = B.id_jenis_bayar_umum) WHERE id_pembayaran = '$id' AND status_jenis_bayar = 'UMUM' ORDER BY tahun, bulan ASC");
							while($result_umum = $db->fetch_assoc($query_umum)){
								echo "<tr>";
								echo "	<td align='center'>$no.</td>";
								echo "	<td align='center' >$result_umum[tahun] ".$func->nama_bulan($result_umum['bulan'])."</td>";
								echo "	<td>$result_umum[jenis_bayar]</td>";
								echo "	<td align='center'>".(($result_umum['status_dispensasi'] == 'TRUE') ? 'Ya' : 'Tidak')."</td>";
								echo "	<td align='right'>Rp. ".number_format($result_umum['nominal_bayar'],0,',','.')."</td>";
								echo "</tr>";

								$total += $result_umum['nominal_bayar'];

								$no++;
							}
						?>
						<tr>
							<td colspan="3" align="right"><b>Total</b></td>
							<td align="right"><b><?php echo number_format($total,0,',','.') ?></b></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-xs-6">
				<h4>2. List Pembayaran Khusus</h4>
				<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th width="1%">No.</th>
							<th width="50%">Detail</th>
							<th>Dispensasi</th>
							<th width="20%">Nominal</th>
						</tr>
					</thead>

					<tbody>
						<?php
							$no = 1;
							$total = 0;
							$query_khusus = $db->sql("SELECT A.*, B.jenis_bayar_khusus FROM _pembayaran_detail AS A INNER JOIN _jenis_bayar_khusus AS B ON(A.id_jenis_bayar = B.id_jenis_bayar_khusus) WHERE id_pembayaran = '$id' AND status_jenis_bayar = 'KHUSUS'");
							while($result_khusus = $db->fetch_assoc($query_khusus)){
								echo "<tr>";
								echo "	<td align='center'>$no.</td>";
								echo "	<td>$result_khusus[jenis_bayar_khusus] <i>(Angsuran ke $result_khusus[angsuran_ke])</i></td>";
								echo "	<td align='center'>".(($result_khusus['status_dispensasi'] == 'TRUE') ? 'Ya' : 'Tidak')."</td>";
								echo "	<td align='right'>Rp. ".number_format($result_khusus['nominal_bayar'],0,',','.')."</td>";
								echo "</tr>";

								$total += $result_khusus['nominal_bayar'];

								$no++;
							}
						?>
						<tr>
							<td colspan="3" align="right"><b>Total</b></td>
							<td align="right"><b><?php echo number_format($total,0,',','.') ?></b></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>
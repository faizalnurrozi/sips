<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

unset($_SESSION['UMUM']);
unset($_SESSION['KHUSUS']);

unset($_SESSION['date_y']);
unset($_SESSION['date_m']);
unset($_SESSION['s_nis']);

$_SESSION['date_y'] = date("Y");
$_SESSION['date_m'] = date("m");
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>

		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal').datepicker(options);
			});
		</script>
	</head>
	<body>
		<?php
			list($id_terakhir) = $db->result_row("SELECT SUBSTRING(id_pembayaran, 10, 4) FROM _pembayaran ORDER BY id_pembayaran DESC");
			$id_terakhir = $id_terakhir + 1;

			$prefix = "BYR";
			$posisi_tanggal = date("ym");
			$urutan = $func->number_pad($id_terakhir, 4);
			$id_pembayaran = $prefix."-".$posisi_tanggal."-".$urutan;

			$nis 		= "";
			$nama_siswa = "";
			if(@$_REQUEST['id'] != ''){
				$qEditAdmin = "SELECT * FROM _pembayaran WHERE id_pembayaran = '$_REQUEST[id]'";
				$dataEdit = $db->sql($qEditAdmin);
				$resultEdit = $db->fetch_assoc($dataEdit);

				$id_pembayaran = $resultEdit['id_pembayaran'];

				$nis = $resultEdit['nis'];
				list($nama_siswa) = $db->result_row("SELECT nama FROM _siswa WHERE nis = '$nis'");

				$_SESSION['s_nis'] 	= $nis;

				$query_detail_umum = $db->sql("SELECT * FROM _pembayaran_detail WHERE id_pembayaran = '$_REQUEST[id]' AND status_jenis_bayar = 'UMUM'");
				while($result_detail_umum = $db->fetch_assoc($query_detail_umum)){
					$_SESSION['UMUM']['s_jenis_bayar'][] 		= $result_detail_umum['id_jenis_bayar'];
					$_SESSION['UMUM']['s_nominal'][] 			= $result_detail_umum['nominal_bayar'];
					$_SESSION['UMUM']['s_tahun'][] 				= $result_detail_umum['tahun'];
					$_SESSION['UMUM']['s_bulan'][] 				= $result_detail_umum['bulan'];
					$_SESSION['UMUM']['s_status_dispensasi'][] 	= $result_detail_umum['status_dispensasi'];
					$_SESSION['UMUM']['s_angsuran_ke'][] 		= $result_detail_umum['angsuran_ke'];
				}

				$query_detail_khusus = $db->sql("SELECT * FROM _pembayaran_detail WHERE id_pembayaran = '$_REQUEST[id]' AND status_jenis_bayar = 'KHUSUS'");
				while($result_detail_khusus = $db->fetch_assoc($query_detail_khusus)){
					$_SESSION['KHUSUS']['s_jenis_bayar'][] 		= $result_detail_khusus['id_jenis_bayar'];
					$_SESSION['KHUSUS']['s_nominal'][] 			= $result_detail_khusus['nominal_bayar'];
					$_SESSION['KHUSUS']['s_status_dispensasi'][] 	= $result_detail_khusus['status_dispensasi'];
					$_SESSION['KHUSUS']['s_angsuran_ke'][] 		= $result_detail_khusus['angsuran_ke'];
				}
			}
		?>		
		<div class="container-fluid">
		<form name="form_pembayaran" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<?php } ?>
			<div class="row">
				<div class="col-xs-4">
					<div class="row">
						<div class="form-group col-xs-5">
							<label>No. Pembayaran</label>
							<input type="text" name="id_pembayaran" id="id_pembayaran" class="form-control input-sm" value="<?php echo $id_pembayaran; ?>" readonly />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-6">
							<label>Tanggal</label>
							<span id="load-tanggal"></span>
							<div class="input-group col-xs-12">
								<input type="text" name="tanggal" id="tanggal" class="form-control input-sm" value="<?php if(@$resultEdit['id_pembayaran'] != '') echo $func->implode_date($resultEdit['tanggal']); else echo date("d/m/Y"); ?>" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 has-nis">
							<label>Siswa</label>
							<span id="load-siswa"></span>
							<div class="input-group col-xs-12">
								<input type="hidden" name="nis" id="nis" value="<?php echo $nis ?>" />
								<input type="text" name="nama_siswa" id="nama_siswa" class="form-control input-sm" placeholder="Pilih siswa ..." readonly value="<?php echo $nama_siswa ?>" />
								<span class="input-group-addon" style="cursor: pointer;" onclick="javascript: window.top.document.getElementById('iframe_choose').src='content.php?module=admin&component=pembayaran&action=siswa'; window.top.document.getElementById('bntModalChoose').click(); "><i class="fa fa-plus"></i></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12">
							<button class="btn btn-success" type="button" onclick="javascript:
							var obj = document.form_pembayaran;
							var err = '';

							if(obj.nis.value==''){ $('.has-nis').addClass('has-error').focus(); err+='<li>Siswa harus dipilih!</li>'; }

							if(err==''){
								obj.action='content.php?module=admin&component=pembayaran&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"><i class="fa fa-save"></i> Simpan transaksi</button>
						</div>
					</div>
				</div>
				
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-12" id="detail_umum">
							<?php include("detail_umum.php"); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" id="detail_khusus">
							<?php include("detail_khusus.php"); ?>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').hide();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->		
	</body>
</html>
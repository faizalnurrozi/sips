<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=pembayaran&add=".$_REQUEST['add']."&id=".$_REQUEST['id'];
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = @$_REQUEST['menu'];
	list($access) = $db->result_row("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			list($levelx, $menusx, $iconx, $linkx, $parentx) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i' ");
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$parent = $parentx;
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo $menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
		
	<section class="content">
		<div class="row">
			<?php if($_REQUEST['add'] == 'true'){ ?>
			<div class="col-xs-12">
				<div class="box">
					<iframe name="iframe_pembayaran" id="iframe_pembayaran" width="98%" height="550" frameborder="0" src="content.php?module=admin&component=pembayaran&action=add&id=<?php echo $_REQUEST['id']; ?>" style="margin-top: 20px;"></iframe>
				</div>
			</div>
			<?php }else{ ?>
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">&nbsp;</h3>
						<div class="box-tools">
							<div class="input-group">
								
								<select class="form-control input-sm" name="tahun" id="tahun" onchange="javascript: sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true&keyword='+document.getElementById('keyword').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&tanggal='+document.getElementById('tanggal').value, 'list', 'div');">
									<option value="">-Pilih Tahun-</option>
									<?php
										for($tahun = date("Y"); $tahun >= date("Y")-2; $tahun--){
											echo "<option value='$tahun'>$tahun</option>";
										}
									?>
								</select>

								<div class="input-group-btn"></div>

								<select class="form-control input-sm" name="bulan" id="bulan" onchange="javascript: sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true&keyword='+document.getElementById('keyword').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&tanggal='+document.getElementById('tanggal').value, 'list', 'div');">
									<option value="">-Pilih Bulan-</option>
									<?php
										for($bulan = 1; $bulan <= 12; $bulan++){
											echo "<option value='".$func->number_pad($bulan,2)."'>".$func->nama_bulan($bulan,2)."</option>";
										}
									?>
								</select>

								<div class="input-group-btn"></div>

								<select class="form-control input-sm" name="tanggal" id="tanggal" onchange="javascript: sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true&keyword='+document.getElementById('keyword').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&tanggal='+document.getElementById('tanggal').value, 'list', 'div');">
									<option value="">-Pilih Tgl-</option>
									<?php
										for($tanggal = 1; $tanggal <= 31; $tanggal++){
											echo "<option value='".$func->number_pad($tanggal,2)."'>".$func->number_pad($tanggal,2)."</option>";
										}
									?>
								</select>

								<div class="input-group-btn"></div>

								<input type="text" name="table_search" class="form-control input-sm pull-right" id="keyword" placeholder="Search" onkeyup="javascript: sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true&keyword='+document.getElementById('keyword').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&tanggal='+document.getElementById('tanggal').value, 'list', 'div');" />
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" onclick="javascript: sendRequest('content.php', 'module=admin&component=pembayaran&action=list&ajax=true&keyword='+document.getElementById('keyword').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value+'&tanggal='+document.getElementById('tanggal').value, 'list', 'div');"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
					
					<div id="list"><?php include "list.php"; ?></div>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>
	
	<input type="hidden" data-toggle="modal" data-target="#myModalChoose" id="bntModalChoose" />
	<div class="modal fade" id="myModalChoose" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Silahkan Pilih</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_choose" id="iframe_choose" width="98%" height="400" frameborder="0"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>
<?php
session_start();
include "globals/config.php";

foreach($_SESSION as $key => $value){
    if (strpos($key, _APP_) === 0){
		unset($_SESSION[$key]);
    }
}
?>
<script language="javascript">window.location.href='index.php';</script>
<?php
global $perusahaan;
?>
<div class="login-box">
	<div class="login-logo">
		<a href="index.php"><img src="<?php echo $logo; ?>" width="50" />&nbsp;<?php echo $header; ?></a>
	</div>
	<!-- /.login-logo -->
	
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>

	<div class="login-box-body" id="login-form">
		<?php include "form.php"; ?>
	</div>
	
	
	<div class="alert" style="opacity:0.5; text-align:center;">
		&copy; <?php echo date("Y"); ?> <?php echo $perusahaan; ?><br/>
		<span>All right reserved.</span>
	</div>
	
</div>
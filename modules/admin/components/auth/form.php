<!--<p class="login-box-msg">Sign in to start your session</p>-->
<form action="javascript: void(null);" method="post">
	<div class="form-group has-feedback">
		<input type="text" class="form-control" id="txtuser" onkeyup="javascript: if(event.keyCode==13){ if(this.value=='') alert('Username harus diisi !!!'); else document.getElementById('txtpass').focus();}" autocomplete="off" placeholder="Masukkan User..." />
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<div class="form-group has-feedback">
		<input type="password" class="form-control" id="txtpass" onkeyup="javascript: if(event.keyCode==13){ if(this.value=='') alert('Password harus diisi !!!'); else document.getElementById('txtkode').focus(); }" placeholder="Masukkan Password..." />
		<span class="glyphicon glyphicon-eye-open form-control-feedback eye-open" style="cursor:pointer; pointer-events: all;" onclick=" $('.eye-open').addClass('hide'); $('.eye-close').removeClass('hide'); document.getElementById('txtpass').type='text'; "></span>
		<span class="glyphicon glyphicon-eye-close form-control-feedback eye-close hide" style="cursor:pointer; pointer-events: all;" onclick=" $('.eye-open').removeClass('hide'); $('.eye-close').addClass('hide'); document.getElementById('txtpass').type='password'; "></span>
	</div>
	<div class="form-group has-feedback">
		<input type="text" id="txtkode" autocomplete="off" onkeyup="javascript: if(event.keyCode==13){ if(this.value=='') alert('Kode harus diisi !!!'); else sendRequest('content.php', 'module=admin&component=auth&action=process&proc=login&user='+document.getElementById('txtuser').value+'&pass='+document.getElementById('txtpass').value+'&kode='+document.getElementById('txtkode').value, 'login-form', 'div'); }" placeholder="Masukkan Kode Verifikasi..." class="form-control input-md" style="display:inline-block; width:70%;" />
		<span id="reload"><?php include "code.php"; ?></span>
		<a style="border-style:none; cursor:pointer; display:inline-block;" title="Reload Image" onClick="javascript: ajaxCustom('content.php', 'module=admin&component=auth&action=process&proc=reload_image', 'load-image'); "><i class="fa fa-refresh"></i></a>
	</div>
	<div class="row">
		<div class="col-xs-4 pull-left">
			<button type="button" class="btn btn-primary btn-block btn-flat" onclick="javascript: sendRequest('content.php', 'module=admin&component=auth&action=process&proc=login&user='+document.getElementById('txtuser').value+'&pass='+document.getElementById('txtpass').value+'&kode='+document.getElementById('txtkode').value, 'login-form', 'div'); "><i class="fa fa-sign-in"></i> Sign In</button>
		</div>
	</div>
</form>
<div id="load-image"></div>
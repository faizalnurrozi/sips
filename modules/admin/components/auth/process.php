<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	/* -- Login Process -- */
	case 'login' :
		$user		= @$_POST['user'];
		$pass_real 	= @$_POST['pass'];
		$pass		= $func->encrypt_md5(@$_POST['pass']);
		
		/* -- Geo Location -- */
		try{
			$json_string = "http://freegeoip.net/json/".$_SERVER['REMOTE_ADDR'];
			$jsondata = @file_get_contents($json_string);
			$geo = @json_decode($jsondata, true);
			$country 	= ($geo['country_name']==NULL) ? "localhost" : $geo['country_name'];
			$region		= ($geo['region_name']==NULL) ? "-" : $geo['region_name'];
			$city		= ($geo['city']==NULL) ? "-" : $geo['city'];
			$latitude	= ($geo['latitude']==NULL) ? "0" : $geo['latitude'];
			$longitude	= ($geo['longitude']==NULL) ? "0" : $geo['longitude'];
		}catch(Exception $e){}
		/* -- End Geo Location -- */
		
		if(isset($_SESSION['security_number']) && $_SESSION['security_number'] == @$_POST['kode']){
			$qData = $db->select("_admin", array("usernames", "icon", "id_admin_group", "nip"), array("passwords" => $pass, "usernames" => $user), "AND");
			$rData	= $db->num_rows($qData);
			$result = $db->fetch_assoc($qData);
			
			if($rData == 0){
				$_SESSION[_APP_.'s_message_error'] = "User & Password tidak sesuai !!!";
				$queryAdminLog = $db->insert("_admin_logs", 
					array(
						'tanggal' => date("Y-m-d H:i:s"), 
						'id_user' => $user, 
						'passwords' => $pass_real, 
						'ip_address' => $_SERVER['REMOTE_ADDR'], 
						'browser' => $_SERVER['HTTP_USER_AGENT'], 
						'status' => 'FAILED', 
						'deskripsi' => 'User/Pass Invalid',
						/* -- Geo JSON -- */
						'country' => $country,
						'region' => $region,
						'city' => $city,
						'latitude' => $latitude,
						'longitude' => $longitude
					)
				);
			}else{
				list($nama, $idjabatan, $jk) = $db->result_row("
					SELECT nama, id_jabatan, jenis_kelamin FROM _karyawan WHERE nip = '$result[nip]'
				");
				
				list($jabatan) = $db->result_row("SELECT nama FROM _jabatan WHERE id_jabatan = '$idjabatan' ");
				if(file_exists($result['icon']) && $result['icon']!=''){ 
					$icon = $result['icon'];
				}else{
					if($jk=='L') $icon = "images/icon-male.png";
					if($jk=='P') $icon = "images/icon-female.png";
				}
				
				$_SESSION[_APP_.'s_userAdmin'] 	= $result['usernames'];
				$_SESSION[_APP_.'s_namaAdmin'] 	= $nama;
				$_SESSION[_APP_.'s_fotoAdmin']	= $icon;
				$_SESSION[_APP_.'s_idGroupAdmin']	= $result['id_admin_group'];
				$_SESSION[_APP_.'s_idJabatan']	= $idjabatan;
				$_SESSION[_APP_.'s_namaJabatan']= $jabatan;
				$_SESSION[_APP_.'s_nipAdmin']= $result['nip'];
				
				echo "
				<div class='alert alert-success'>
					<button type='button' class='close' data-dismiss='alert'>x</button>
					<strong>Status : </strong> Login Success...
				</div>
				";
				$queryAdminLog = $db->insert("_admin_logs", 
					array(
						'tanggal' => date("Y-m-d H:i:s"), 
						'id_user' => $user, 
						'passwords' => $pass_real, 
						'ip_address' => $_SERVER['REMOTE_ADDR'], 
						'browser' => $_SERVER['HTTP_USER_AGENT'], 
						'status' => 'SUCCESS', 
						'deskripsi' => 'OK',
						/* -- Geo JSON -- */
						'country' => $country,
						'region' => $region,
						'city' => $city,
						'latitude' => $latitude,
						'longitude' => $longitude
					)
				);
			}
			
			echo "<script language='javascript'>window.location.href='Admin.phpx';</script>";
		}else{
			$_SESSION[_APP_.'s_message_error'] = "Kode Verifikasi Salah !!!";
			$queryAdminLog = $db->insert("_admin_logs", 
				array(
					'tanggal' => date("Y-m-d H:i:s"), 
					'id_user' => $user, 
					'passwords' => $pass_real, 
					'ip_address' => $_SERVER['REMOTE_ADDR'], 
					'browser' => $_SERVER['HTTP_USER_AGENT'], 
					'status' => 'FAILED', 
					'deskripsi' => 'Captcha Kode Invalid',
					/* -- Geo JSON -- */
					'country' => $country,
					'region' => $region,
					'city' => $city,
					'latitude' => $latitude,
					'longitude' => $longitude
				)
			);
			echo "<script language='javascript'>window.location.href='Admin.phpx';</script>";
		}		
		
	break;
	/* -- End Login Process -- */
	
	/* -- Reload Captcha Image -- */
	case "reload_image":
		echo "
			<script>
				document.getElementById('imgkode').src='includes/securicode/image.php?code=".rand(0,9999)."';
			</script>
		";
	break;
	/* -- End Reload Captcha Image -- */
}
?>
<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 		= strtoupper(uniqid());
		$jenis_bayar 	= @$_POST['txtjenis_bayar'];
		$nominal 	= @$_POST['txtnominal'];
		
		$data = array(
			"id_jenis_bayar_umum" => $id,
			"jenis_bayar" => $jenis_bayar,
			"nominal" => $nominal,
		);
		
		$hqData = $db->insert("_jenis_bayar_umum", $data);

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_jenis_bayar_umum", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_bayar_umum&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_bayar_umum&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 		= @$_POST['txtidx'];
		$jenis_bayar 	= @$_POST['txtjenis_bayar'];
		$nominal 	= @$_POST['txtnominal'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		$data = array(
			"jenis_bayar" => $jenis_bayar,
			"nominal" => $nominal,
		);
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_jenis_bayar_umum",$data , array('id_jenis_bayar_umum' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update(
			"_jenis_bayar_umum", 
			$data, 
			array('id_jenis_bayar_umum' => $idx)
		);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_bayar_umum&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_bayar_umum&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_jenis_bayar_umum", array('id_jenis_bayar_umum' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_jenis_bayar_umum", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=jenis_bayar_umum&action=list&ajax=true");
		
		#---------- * ----------#
		break;
}
?>
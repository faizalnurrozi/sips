<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php
		$query = $db->sql(" SELECT jenis_bayar, nominal FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id' ");
		$result = $db->fetch_assoc($query);
		?>
		<table class="table">
			<tr>
				<td width="20%">Jenis Bayar&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['jenis_bayar']; ?></td>
			</tr>
			<tr>
				<td>Nominal&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['nominal']; ?></td>
			</tr>
		</table>
	</body>
</html>
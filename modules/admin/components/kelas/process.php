<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 		= @$_POST['txtid'];
		$id_tingkat 	= @$_POST['id_tingkat'];
		$nama 		= @$_POST['txtnama'];
		
		$hqData = $db->insert("_kelas", array('id_kelas' => $id, 'id_tingkat' => $id_tingkat, 'nama' => $nama));

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_kelas", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=kelas&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=kelas&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 		= @$_POST['txtidx'];
		$id 		= @$_POST['txtid'];
		$nama 		= @$_POST['txtnama'];
		$id_tingkat 	= @$_POST['id_tingkat'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_kelas", array('id_kelas' => $id, 'nama' => $nama, 'id_tingkat' => $id_tingkat), array('id_kelas' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_kelas", array('id_kelas' => $id, 'nama' => $nama), array('id_kelas' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=kelas&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=kelas&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_kelas", array('id_kelas' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_kelas", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=kelas&action=list&ajax=true");
		
		#---------- * ----------#
		break;
}
?>
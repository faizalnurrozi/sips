<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";
		}
		</script>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _kelas WHERE id_kelas = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
		}
		?>		
		<div class="container-fluid">
		<form name="form_kelas" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo $resultEdit['id_kelas']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-8 has-id">
					<label>Kode&nbsp;Kelas</label>
					<input type="text" name="txtid" id="txtid" value="<?php echo $resultEdit['id_kelas']; ?>" autocomplete="off" class="form-control input-sm" style="width:150px" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-id">
					<label>Tingkat</label>
					<select name="id_tingkat" id="id_tingkat" class="form-control input-sm">
						<option value="">-- Pilih tingkat --</option>
						<?php
							$query_tingkat = $db->sql("SELECT * FROM _tingkat");
							while($result_tingkat = $db->fetch_assoc($query_tingkat)){
								echo "<option value='$result_tingkat[id_tingkat]' "; if($result_tingkat['id_tingkat'] == $resultEdit['id_tingkat']) echo "selected"; echo " >$result_tingkat[nama]</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-nama">
					<label>Nama&nbsp;Kelas</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo $resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" />
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_kelas;
							var err = '';
							if(obj.txtid.value==''){ $('.has-id').addClass('has-error').focus(); err+='<li>Kode Kelas harus di isi</li>'; }
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama kelas harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=kelas&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_kelas.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>
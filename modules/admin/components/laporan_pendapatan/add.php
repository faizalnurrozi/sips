<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>

		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal').datepicker(options);
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_laporan_pendapatan" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-4">
							<label>Kelas</label>
							<select name="id_kelas" id="id_kelas" class="form-control input-sm">
								<option value="">-- Pilih kelas --</option>
								<?php
									$query_kelas = $db->sql("SELECT * FROM _kelas");
									while($result_kelas = $db->fetch_assoc($query_kelas)){
										echo "<option value='$result_kelas[id_kelas]' "; if($result_kelas['id_kelas'] == $resultEdit['id_kelas']) echo "selected"; echo " >$result_kelas[nama]</option>";
									}
								?>
							</select>
						</div>
						<div class="form-group col-xs-4">
							<label>Jenis Bayar</label>
							<select class="form-control input-sm" name="id_jenis_bayar" id="id_jenis_bayar">
								<option value="">-Pilih jenis bayar-</option>
								<?php
									echo "<optgroup label='Umum'>";
									$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_umum AS id_jenis_bayar, jenis_bayar, nominal FROM _jenis_bayar_umum ORDER BY jenis_bayar ASC");
									while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
										echo "<option value='$result_jenis_bayar[id_jenis_bayar]'>   - $result_jenis_bayar[jenis_bayar]</option>";
									}
									echo "</optgroup>";

									echo "<optgroup label='Khusus'>";
									$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_khusus, jenis_bayar_khusus, nominal FROM _jenis_bayar_khusus ORDER BY jenis_bayar_khusus ASC");
									while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
										echo "<option value='$result_jenis_bayar[id_jenis_bayar_khusus]'>   - $result_jenis_bayar[jenis_bayar_khusus]</option>";
									}
									echo "</optgroup>";
								?>
							</select>
						</div>
						<div class="form-group col-xs-4">
							<label>Tahun Ajaran</label>
							<select name="tahun" id="tahun" class="form-control input-sm">
								<option>-Pilih tahun ajaran-</option>
								<?php
									$query_semester_ajaran = $db->sql("SELECT tahun_ajaran FROM _semester_ajaran GROUP BY tahun_ajaran DESC");
									while($result_semester_ajaran = $db->fetch_assoc($query_semester_ajaran)){
										echo "<option value='$result_semester_ajaran[tahun_ajaran]'>$result_semester_ajaran[tahun_ajaran] - ".($result_semester_ajaran['tahun_ajaran']+1)."</option>";
									}
								?>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<button class="btn btn-default" type="button" onclick="javascript: sendRequest('content.php', 'module=admin&component=laporan_pendapatan&action=detail&ajax=true&id_kelas='+document.getElementById('id_kelas').value+'&id_jenis_bayar='+document.getElementById('id_jenis_bayar').value+'&tahun='+document.getElementById('tahun').value, 'detail', 'div'); "><i class="fa fa-database"></i> Proses hitung</button>
							<button class="btn btn-success" type="button" onclick="javascript: window.open('content.php?module=admin&component=laporan_pendapatan&action=xls&ajax=true&id_kelas='+document.getElementById('id_kelas').value+'&id_jenis_bayar='+document.getElementById('id_jenis_bayar').value+'&tahun='+document.getElementById('tahun').value, '_blank'); "><i class="fa fa-database"></i> Export Excel (.xls)</button>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 table-responsive" id="detail">
							<?php include("detail.php"); ?>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').hide();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->		
	</body>
</html>
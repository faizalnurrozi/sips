<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";	
	$db = new Database();
	$func = new Functions();
}

$id_kelas 		= @$_REQUEST['id_kelas'];
$id_jenis_bayar = @$_REQUEST['id_jenis_bayar'];
$tahun 			= @$_REQUEST['tahun'];
$tahun_ganjil	= @$_REQUEST['tahun']."1";
$tahun_genap	= @$_REQUEST['tahun']."2";

list($id_tingkat)	= $db->result_row("SELECT id_tingkat FROM _kelas WHERE id_kelas = '$id_kelas'");

list($tgl_awal_ganjil, $tgl_akhir_ganjil) = $db->result_row("SELECT tanggal_awal, tanggal_akhir FROM _semester_ajaran WHERE id_semester_ajaran = '$tahun_ganjil'");
list($tgl_awal_genap, $tgl_akhir_genap) = $db->result_row("SELECT tanggal_awal, tanggal_akhir FROM _semester_ajaran WHERE id_semester_ajaran = '$tahun_genap'");

$jenis_bayar = "UMUM";
list($cekJenisBayar) = $db->result_row("SELECT COUNT(*) FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar'");
if($cekJenisBayar > 0) $jenis_bayar = "KHUSUS";

$bulan_mulai_1 = (int) substr($tgl_awal_ganjil,5,2);

if(substr($tgl_akhir_ganjil,0,4) != $tahun){
	$bulan_akhir_1 = 12;
	$bulan_mulai_2 = 1;
}else{
	$bulan_akhir_1 = (int) substr($tgl_akhir_ganjil,5,2);
	$bulan_mulai_2 = (int) substr($tgl_awal_genap,5,2);
}

$bulan_akhir_2 = (int) substr($tgl_akhir_genap,5,2);

if($tahun != ''){
?>
<table class="table table-bordered table-hover table-striped" style="font-size: 10px;">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th>Nama Siswa</th>
			<?php
				/**
				 * Looping bulan pada tahun pertama
				 */
				for($bulan = $bulan_mulai_1; $bulan <= $bulan_akhir_1; $bulan++){
					echo "<th>".$func->nama_bulan($func->number_pad($bulan, 2))."</th>";
				}

				/**
				 * Looping bulan pada tahun kedua
				 */
				for($bulan = $bulan_mulai_2; $bulan <= $bulan_akhir_2; $bulan++){
					echo "<th>".$func->nama_bulan($func->number_pad($bulan, 2))."</th>";
				}
			?>
			<th>Sisa</th>
		</tr>
		<?php
			$no = 1;
			$query_siswa	= $db->sql("SELECT nis, nama FROM _siswa WHERE id_kelas = '$id_kelas'");
			while($result_siswa = $db->fetch_assoc($query_siswa)){
				echo "<tr>";
				echo "	<td align='center'>$no.</td>";
				echo "	<td>$result_siswa[nama]</td>";

				$total_kewajiban_bayar 	= 0;
				$total_jumlah_bayar 	= 0;

				if($jenis_bayar == 'KHUSUS'){
					list($kewajiban_bayar_khusus) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE B.id_jenis_bayar = '$id_jenis_bayar'");

					$periode_awal_proses = $tahun.'-'.$func->number_pad($bulan_mulai_1, 2).'-01';

					list($jumlah_bayar_sebelum) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal < '$periode_awal_proses' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");

					$kewajiban_bayar_khusus -= $jumlah_bayar_sebelum;
				}

				for($bulan = $bulan_mulai_1; $bulan <= $bulan_akhir_1; $bulan++){
					$tgl_transaksi = $tahun."-".$func->number_pad($bulan, 2);

					list($jumlah_bayar) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal LIKE '$tgl_transaksi%' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");
					$total_jumlah_bayar += $jumlah_bayar;

					if($jenis_bayar == 'UMUM'){
						list($kewajiban_bayar) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE A.id_tingkat = '$id_tingkat' AND A.tahun = '$tahun' AND bulan = '".$func->number_pad($bulan, 2)."' AND B.id_jenis_bayar = '$id_jenis_bayar'");
						$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar-$jumlah_bayar;
					}

					echo "<td align='right'>".(($jumlah_bayar == 0) ? '-' : number_format($jumlah_bayar,0,',','.'))."</td>";
				}

				for($bulan = $bulan_mulai_2; $bulan <= $bulan_akhir_2; $bulan++){
					$tgl_transaksi = ($tahun+1)."-".$func->number_pad($bulan, 2);

					list($jumlah_bayar) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal LIKE '$tgl_transaksi%' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");
					$total_jumlah_bayar += $jumlah_bayar;

					if($jenis_bayar == 'UMUM'){
						list($kewajiban_bayar) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE A.id_tingkat = '$id_tingkat' AND A.tahun = '".($tahun+1)."' AND bulan = '".$func->number_pad($bulan, 2)."' AND B.id_jenis_bayar = '$id_jenis_bayar'");
						
						$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar-$jumlah_bayar;
					}

					echo "<td align='right'>".(($jumlah_bayar == 0) ? '-' : number_format($jumlah_bayar,0,',','.'))."</td>";
				}

				if($jenis_bayar == 'KHUSUS'){
					$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar_khusus-$total_jumlah_bayar;
				}
				
				echo "<td align='right'>".number_format($total_kewajiban_bayar,0,',','.')."</td>";

				echo "</tr>";

				$no++;
			}
		?>
	</thead>
</table>
<?php } ?>
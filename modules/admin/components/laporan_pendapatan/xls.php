<?php
error_reporting(E_ALL);
ini_set('max_execution_time', 300);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
require_once 'includes/phpexcel/PHPExcel.php';

$filex = "PendapatanPerkelas.xls";

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Amanatul Ummah")
							 ->setLastModifiedBy("Amanatul Ummah")
							 ->setTitle("Pendapatan Per Kelas")
							 ->setSubject("Pendapatan Per Kelas")
							 ->setDescription("Pendapatan Per Kelas")
							 ->setKeywords("Pendapatan Per Kelas")
							 ->setCategory("Laporan");

$bold = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
);

$normal = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	)
);

$rowHeader = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
);

$rowBody = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
);

$rowRight = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	),
);

$rowCenter = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
	),
);

$rowRight = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	),
);

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

for($i = 'A'; $i < 'Z'; $i++){
	$abjad[] = $i;
}


$id_kelas 		= @$_REQUEST['id_kelas'];
$id_jenis_bayar = @$_REQUEST['id_jenis_bayar'];
$tahun 			= @$_REQUEST['tahun'];
$tahun_ganjil	= @$_REQUEST['tahun']."1";
$tahun_genap	= @$_REQUEST['tahun']."2";

list($id_tingkat)	= $db->result_row("SELECT id_tingkat FROM _kelas WHERE id_kelas = '$id_kelas'");

list($namaKelas) = $db->result_row("SELECT nama FROM _kelas WHERE id_kelas = '$id_kelas'");

if($id_jenis_bayar != ''){
	list($jenis_bayar) = $db->result_row("SELECT jenis_bayar FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar'");
	if($jenis_bayar == ''){
		list($jenis_bayar) = $db->result_row("SELECT jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar'");
	}

	$jenis_bayar = strtoupper($jenis_bayar);
}else{
	$jenis_bayar = "PENDAPATAN";
}

list($tgl_awal_ganjil, $tgl_akhir_ganjil) = $db->result_row("SELECT tanggal_awal, tanggal_akhir FROM _semester_ajaran WHERE id_semester_ajaran = '$tahun_ganjil'");
list($tgl_awal_genap, $tgl_akhir_genap) = $db->result_row("SELECT tanggal_awal, tanggal_akhir FROM _semester_ajaran WHERE id_semester_ajaran = '$tahun_genap'");

$jenis_bayar = "UMUM";
list($cekJenisBayar) = $db->result_row("SELECT COUNT(*) FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar'");
if($cekJenisBayar > 0) $jenis_bayar = "KHUSUS";

$bulan_mulai_1 = (int) substr($tgl_awal_ganjil,5,2);

if(substr($tgl_akhir_ganjil,0,4) != $tahun){
	$bulan_akhir_1 = 12;
	$bulan_mulai_2 = 1;
}else{
	$bulan_akhir_1 = (int) substr($tgl_akhir_ganjil,5,2);
	$bulan_mulai_2 = (int) substr($tgl_awal_genap,5,2);
}

$bulan_akhir_2 = (int) substr($tgl_akhir_genap,5,2);

$x = 2;
for($row_bulan = $bulan_mulai_1; $row_bulan <= $bulan_akhir_1; $row_bulan++){ $x++; }
for($row_bulan = $bulan_mulai_2; $row_bulan <= $bulan_akhir_2; $row_bulan++){ $x++; }

/**
 * Title
 */

$objPHPExcel->getActiveSheet()->mergeCells("A1:".$abjad[($x)].'1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'PENERIMAAN '.$jenis_bayar.' SMP UNGGULAN AMANATUL UMMAH');

$objPHPExcel->getActiveSheet()->mergeCells("A2:".$abjad[($x)].'2');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'TAHUN AJARAN '.$tahun.' / '.($tahun+1));
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$abjad[($x)].'2')->applyFromArray($bold);

/*** Sheet Utama ***/
$objPHPExcel->getActiveSheet()->getStyle('A4:'.$abjad[($x)].'4')->applyFromArray($rowHeader);
cellColor('A4:'.$abjad[($x)].'4', '6ce0ab');

$objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Kelas : '.$namaKelas);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'No')->getColumnDimension('A')->setWidth(5);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', 'Nama Siswa')->getColumnDimension('B')->setWidth(35);

$x = 2;
for($row_bulan = $bulan_mulai_1; $row_bulan <= $bulan_akhir_1; $row_bulan++){
	$bulan = $func->number_pad($row_bulan,2);

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($abjad[$x].'4', $func->nama_bulan($bulan))->getColumnDimension($abjad[$x])->setWidth(15);

	$x++;
}

for($row_bulan = $bulan_mulai_2; $row_bulan <= $bulan_akhir_2; $row_bulan++){
	$bulan = $func->number_pad($row_bulan,2);

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($abjad[$x].'4', $func->nama_bulan($row_bulan))->getColumnDimension($abjad[$x])->setWidth(15);

	$x++;
}
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($abjad[$x]."4", 'Sisa')->getColumnDimension($abjad[$x])->setWidth(15);

$no = 1; $row = 5;
$query_siswa	= $db->sql("SELECT nis, nama FROM _siswa WHERE id_kelas = '$id_kelas'");
while($result_siswa = $db->fetch_assoc($query_siswa)){
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$row", $no);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$row", $result_siswa['nama']);


	$total_kewajiban_bayar 	= 0;
	$total_jumlah_bayar 	= 0;

	if($jenis_bayar == 'KHUSUS'){
		list($kewajiban_bayar_khusus) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE B.id_jenis_bayar = '$id_jenis_bayar'");

		$periode_awal_proses = $tahun.'-'.$func->number_pad($bulan_mulai_1, 2).'-01';

		list($jumlah_bayar_sebelum) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal < '$periode_awal_proses' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");

		$kewajiban_bayar_khusus -= $jumlah_bayar_sebelum;
	}

	$x = 2;
	for($bulan = $bulan_mulai_1; $bulan <= $bulan_akhir_1; $bulan++){
		$tgl_transaksi = $tahun."-".$func->number_pad($bulan, 2);

		list($jumlah_bayar) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal LIKE '$tgl_transaksi%' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");
		$total_jumlah_bayar += $jumlah_bayar;

		if($jenis_bayar == 'UMUM'){
			list($kewajiban_bayar) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE A.id_tingkat = '$id_tingkat' AND A.tahun = '$tahun' AND bulan = '".$func->number_pad($bulan, 2)."' AND B.id_jenis_bayar = '$id_jenis_bayar'");
			$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar-$jumlah_bayar;
		}

		$koordinat = $abjad[$x] . $row;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($koordinat, (($jumlah_bayar == 0) ? '0' : $jumlah_bayar));

		$x++;
	}

	for($bulan = $bulan_mulai_2; $bulan <= $bulan_akhir_2; $bulan++){
		$tgl_transaksi = ($tahun+1)."-".$func->number_pad($bulan, 2);

		list($jumlah_bayar) = $db->result_row("SELECT SUM(nominal_bayar) FROM _pembayaran_detail AS A INNER JOIN _pembayaran AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE B.nis = '$result_siswa[nis]' AND B.tanggal LIKE '$tgl_transaksi%' AND A.id_jenis_bayar LIKE '$id_jenis_bayar'");
		$total_jumlah_bayar += $jumlah_bayar;

		if($jenis_bayar == 'UMUM'){
			list($kewajiban_bayar) = $db->result_row("SELECT nominal FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) WHERE A.id_tingkat = '$id_tingkat' AND A.tahun = '".($tahun+1)."' AND bulan = '".$func->number_pad($bulan, 2)."' AND B.id_jenis_bayar = '$id_jenis_bayar'");
			
			$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar-$jumlah_bayar;
		}

		$koordinat = $abjad[$x] . $row;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($koordinat, (($jumlah_bayar == 0) ? '0' : $jumlah_bayar));

		$x++;
	}

	if($jenis_bayar == 'KHUSUS'){
		$total_kewajiban_bayar = $total_kewajiban_bayar+$kewajiban_bayar_khusus-$total_jumlah_bayar;
	}

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($abjad[$x] . $row, (($total_kewajiban_bayar == 0) ? '0' : $total_kewajiban_bayar));
	$x++;

	$no++; $row++;
}
$objPHPExcel->getActiveSheet()->getStyle("A5:".$abjad[($x-1)].($row-1))->applyFromArray($rowBody);
$objPHPExcel->getActiveSheet()->getStyle("A5:A".($row-1))->applyFromArray($rowCenter);
$objPHPExcel->getActiveSheet()->getStyle($abjad[2].'5'.":".$abjad[($x-1)].($row-1))->applyFromArray($rowRight);
$objPHPExcel->getActiveSheet()->getStyle($abjad[2].'5'.":".$abjad[($x-1)].($row-1))->getNumberFormat()->setFormatCode('#,##0');


$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$filex);
$objWriter->save('php://output');
?>

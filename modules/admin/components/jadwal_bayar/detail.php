<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";
		$db = new Database();
		$func = new Functions();
	}
	$proc = $_REQUEST['proc'];
	switch($proc){
		case "add":
			$id_jenis_bayar = $_POST['id_jenis_bayar'];
			$nominal_detail 	= $_POST['nominal_detail'];

			$_SESSION['s_jenis_bayar'][] 	= $id_jenis_bayar;
			$_SESSION['s_nominal'][] 		= $nominal_detail;
		break;

		case "update":
			$posisi				= $_POST['posisi'];
			$id_jenis_bayar = $_POST['id_jenis_bayar'];
			$nominal_detail 	= $_POST['nominal_detail'];

			$_SESSION['s_jenis_bayar'][$posisi] = $id_jenis_bayar;
			$_SESSION['s_nominal'][$posisi] 	= $nominal_detail;
		break;

		case "delete":
			$posisi = $_POST['posisi'];

			unset($_SESSION['s_jenis_bayar'][$posisi]);
			unset($_SESSION['s_nominal'][$posisi]);

			$_SESSION['s_jenis_bayar'] 	= array_values($_SESSION['s_jenis_bayar']);
			$_SESSION['s_nominal'] 		= array_values($_SESSION['s_nominal']);
		break;
	}
?>

<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th width="50%">Detail</th>
			<th>Nominal</th>
			<th width="20%">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
		<?php
			$no = 1; $i = 0;
			$total_nominal = 0;
			if(count($_SESSION['s_jenis_bayar']) > 0){
				foreach($_SESSION['s_jenis_bayar'] as $jenis_bayar){
					$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];

					if($proc == 'edit' & $_REQUEST['posisi'] == $i){

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>";
						echo "		<select class='form-control input-sm' name='id_jenis_bayar_edit' id='id_jenis_bayar_edit'>";
						echo "			<option value=''>- Pilih jenis bayar-</option>";
						echo "			<optgroup label='Umum'>";
										$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_umum, jenis_bayar, nominal FROM _jenis_bayar_umum ORDER BY jenis_bayar ASC");
										while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
											echo "<option value='$result_jenis_bayar[id_jenis_bayar_umum]' "; if($result_jenis_bayar['id_jenis_bayar_umum'] == $jenis_bayar) echo "selected"; echo " >   - $result_jenis_bayar[jenis_bayar]</option>";
										}
										echo "</optgroup>";

										echo "<optgroup label='Khusus'>";
										$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_khusus, jenis_bayar_khusus, nominal FROM _jenis_bayar_khusus ORDER BY jenis_bayar_khusus ASC");
										while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
											echo "<option value='$result_jenis_bayar[id_jenis_bayar_khusus]' "; if($result_jenis_bayar['id_jenis_bayar_khusus'] == $jenis_bayar) echo "selected"; echo " >   - $result_jenis_bayar[jenis_bayar_khusus]</option>";
										}
						echo "			</optgroup>";
						echo "		</select>";
						echo "	</td>";
						echo "	<td>";
						echo "		<input type='number' name='nominal_detail_edit' id='nominal_detail_edit' placeholder='...' value='$nominal' class='form-control input-sm' style='text-align: right;'>";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-sm' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&ajax=true&action=detail&proc=update&posisi=$i&id_jenis_bayar='+document.getElementById('id_jenis_bayar_edit').value+'&nominal_detail='+document.getElementById('nominal_detail_edit').value, 'detail'); \">&nbsp;<i class='fa fa-save'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}else{

						list($nama_jenis_bayar) = $db->result_row("SELECT jenis_bayar FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$jenis_bayar'");
						if($nama_jenis_bayar == '') list($nama_jenis_bayar) = $db->result_row("SELECT jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$jenis_bayar'");

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>$nama_jenis_bayar</td>";
						echo "	<td align='right'>Rp. ".number_format($nominal,0,',','.')."</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-sm' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=detail&ajax=true&proc=edit&posisi=$i', 'detail'); \">&nbsp;<i class='fa fa-edit'></i>&nbsp;</button>";
						echo "			<button type='button' class='btn btn-sm btn-danger btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&ajax=true&action=detail&proc=delete&posisi=$i', 'detail'); \">&nbsp;<i class='fa fa-minus'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}

					$total_nominal += $nominal;

					$no++; $i++;
				}
			}
		?>
		<tr>
			<td align="center"><?php echo $no; ?></td>
			<td>
				<span id="load-nominal"></span>
				<select class="form-control input-sm" name="id_jenis_bayar" id="id_jenis_bayar" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=process&proc=nominal&id_jenis_bayar='+this.value, 'load-nominal'); ">
					<option value="">- Pilih jenis bayar-</option>
					<?php
						echo "<optgroup label='Umum'>";
						$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_umum AS id_jenis_bayar, jenis_bayar, nominal FROM _jenis_bayar_umum ORDER BY jenis_bayar ASC");
						while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
							echo "<option value='$result_jenis_bayar[id_jenis_bayar]'>   - $result_jenis_bayar[jenis_bayar]</option>";
						}
						echo "</optgroup>";

						echo "<optgroup label='Khusus'>";
						$query_jenis_bayar = $db->sql("SELECT id_jenis_bayar_khusus, jenis_bayar_khusus, nominal FROM _jenis_bayar_khusus ORDER BY jenis_bayar_khusus ASC");
						while($result_jenis_bayar = $db->fetch_assoc($query_jenis_bayar)){
							echo "<option value='$result_jenis_bayar[id_jenis_bayar_khusus]'>   - $result_jenis_bayar[jenis_bayar_khusus]</option>";
						}
						echo "</optgroup>";
					?>
				</select>
			</td>
			<td>
				<input type="number" name="nominal_detail" id="nominal_detail" class="form-control input-sm" placeholder="..." style="text-align: right;" />
			</td>
			<td align="center">
				<button type="button" class="btn btn-sm btn-default btn-flat" onclick="javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=detail&ajax=true&proc=add&id_jenis_bayar='+document.getElementById('id_jenis_bayar').value+'&nominal_detail='+document.getElementById('nominal_detail').value, 'detail'); ">&nbsp;<i class="fa fa-plus"></i>&nbsp;</button>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><b>Total Nominal</b></td>
			<td align="right"><b>Rp. <?php echo number_format($total_nominal,0,',','.') ?></b></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";	
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<div class="box-body table-responsive">
	<!-- Alert Process -->
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>
	<!-- Alert Process -->
</div>

<?php
	/*Sorting*/
	if($_POST['sort']=='reset'){
		$_SESSION[_APP_.'s_field_jadwal_bayar'] = "id_jadwal_bayar";
		$_SESSION[_APP_.'s_sort_jadwal_bayar'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}
	switch($_POST['field']){
		case 'id_jadwal_bayar' : $_SESSION[_APP_.'s_field_jadwal_bayar'] = "id_jadwal_bayar"; break;
		case 'B.nama' : $_SESSION[_APP_.'s_field_jadwal_bayar'] = "B.nama"; break;
		case 'tahun' : $_SESSION[_APP_.'s_field_jadwal_bayar'] = "tahun"; break;
		case 'bulan' : $_SESSION[_APP_.'s_field_jadwal_bayar'] = "bulan"; break;
		case 'tanggal' : $_SESSION[_APP_.'s_field_jadwal_bayar'] = "tanggal"; break;
		default : 
			if(!isset($_SESSION[_APP_.'s_field_jadwal_bayar'])){
				$_SESSION[_APP_.'s_field_jadwal_bayar'] = "id_jadwal_bayar";
			}
			break;
	}
	if(!isset($_SESSION[_APP_.'s_sort_jadwal_bayar'])){
		$_SESSION[_APP_.'s_sort_jadwal_bayar'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
	}else{
		switch($_POST['act']){
			case 'sort' :
				if($_SESSION[_APP_.'s_sort_jadwal_bayar'] == "ASC"){
					$_SESSION[_APP_.'s_sort_jadwal_bayar'] = "DESC";
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}else if($_SESSION[_APP_.'s_sort_jadwal_bayar'] == "DESC"){
					$_SESSION[_APP_.'s_sort_jadwal_bayar'] = "ASC";
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}
				break;
			case 'paging' :
				if($_SESSION[_APP_.'s_sort_jadwal_bayar'] == "ASC"){
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}else if($_SESSION[_APP_.'s_sort_jadwal_bayar'] == "DESC"){
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}
				break;
		}
	}
	/*End Sorting*/
	
	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];
	
	$id_tingkat = @$_REQUEST['id_tingkat'];
	$tahun 		= @$_REQUEST['tahun'];
	$bulan 		= @$_REQUEST['bulan'];
	$qSQL 		= "SELECT A.*, B.nama AS tingkat FROM _jadwal_bayar AS A INNER JOIN _tingkat AS B ON(A.id_tingkat = B.id_tingkat) WHERE (A.id_tingkat LIKE :id_tingkat AND tahun LIKE :tahun AND bulan LIKE :bulan) ORDER BY ".$_SESSION[_APP_.'s_field_jadwal_bayar']." ".$_SESSION[_APP_.'s_sort_jadwal_bayar'];
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":id_tingkat", "%".$id_tingkat."%", "str");
	$db->bind($hqSQL, ":tahun", "%".$tahun."%", "str");
	$db->bind($hqSQL, ":bulan", "%".$bulan."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":id_tingkat", "%".$id_tingkat."%", "str");
	$db->bind($hqSQL, ":tahun", "%".$tahun."%", "str");
	$db->bind($hqSQL, ":bulan", "%".$bulan."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&act=sort&field=id_jadwal_bayar&id_tingkat=<?php echo $id_tingkat; ?>&tahun=<?php echo $tahun; ?>&bulan=<?php echo $bulan; ?>', 'list', 'div');">Nomor&nbsp;<?php if($_SESSION[_APP_.'s_field_jadwal_bayar'] == 'id_jadwal_bayar') echo $iconsort; ?></th>
				<th class="sort" width="20%" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&act=sort&field=B.nama&id_tingkat=<?php echo $id_tingkat; ?>&tahun=<?php echo $tahun; ?>&bulan=<?php echo $bulan; ?>', 'list', 'div');">Tingkat&nbsp;<?php if($_SESSION[_APP_.'s_field_jadwal_bayar'] == 'B.nama') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&act=sort&field=tahun&id_tingkat=<?php echo $id_tingkat; ?>&tahun=<?php echo $tahun; ?>&bulan=<?php echo $bulan; ?>', 'list', 'div');">Tahun&nbsp;<?php if($_SESSION[_APP_.'s_field_jadwal_bayar'] == 'tahun') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&act=sort&field=bulan&id_tingkat=<?php echo $id_tingkat; ?>&tahun=<?php echo $tahun; ?>&bulan=<?php echo $bulan; ?>', 'list', 'div');">Bulan&nbsp;<?php if($_SESSION[_APP_.'s_field_jadwal_bayar'] == 'bulan') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&act=sort&field=tanggal&id_tingkat=<?php echo $id_tingkat; ?>&tahun=<?php echo $tahun; ?>&bulan=<?php echo $bulan; ?>', 'list', 'div');">Tanggal&nbsp;<?php if($_SESSION[_APP_.'s_field_jadwal_bayar'] == 'tanggal') echo $iconsort; ?></th>
				<th width="80">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='7' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='center'>".$func->highlight($hasil['id_jadwal_bayar'], $keyword)."</td>";
					echo "<td align='center'>".$func->highlight($hasil['tingkat'], $keyword)." (".$func->terbilang($hasil['tingkat']).")"."</td>";
					echo "<td align='center'>".$func->highlight($hasil['tahun'], $keyword)."</td>";
					echo "<td align='center'>".$func->nama_bulan($hasil['bulan'],0,',','.')."</td>";
					echo "<td align='center'>".$func->highlight($hasil['tanggal'], $keyword)."</td>";
					
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
					
					/* -- Akses View */
					if($func->akses('R') == true){
						echo "<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modal-view' onclick=\"javascript:  document.getElementById('iframe_view').src='content.php?module=admin&component=jadwal_bayar&action=view&id=$hasil[id_jadwal_bayar]'; \" onmouseover=\"$(this).tooltip();\" title='View'><i class='fa fa-search'></i></button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i></button>";
					}
					/* -- End Akses View -- */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#myModal' onclick=\"javascript: document.getElementById('iframe_jadwal_bayar').src='content.php?module=admin&component=jadwal_bayar&action=add&id=$hasil[id_jadwal_bayar]'; \" onmouseover=\"$(this).tooltip();\" title='Edit'><i class='fa fa-edit'></i></button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i></button>";
					}
					/* End Akses menu edit */
					
					/* Akses menu Delete */
					if($func->akses('D') == true){
						echo "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#modal-delete' onclick=\"javascript:  document.getElementById('id_delete').value='$hasil[id_jadwal_bayar]'; \" onmouseover=\"$(this).tooltip();\" title='Delete'><i class='fa fa-trash'></i></button>";
					}else{
						echo "<button type='button' title='Delete' class='btn btn-default' disabled ><i class='fa fa-trash'></i></button>";
					}
					/* End Akses menu delete */
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=jadwal_bayar&action=list&ajax=true&id_tingkat=$id_tingkat&tahun=$tahun&bulan=$bulan&start=".($start-$limit)."', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=jadwal_bayar&action=list&id_tingkat=$id_tingkat&tahun=$tahun&bulan=$bulan&ajax=true&start=".($a*$limit)."', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=jadwal_bayar&action=list&id_tingkat=$id_tingkat&tahun=$tahun&bulan=$bulan&ajax=true&start=".($start+$limit)."', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>


<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300" frameborder="0"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="id_delete" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('id_delete').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->
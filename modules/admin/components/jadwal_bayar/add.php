<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

unset($_SESSION['s_jenis_bayar']);
unset($_SESSION['s_nominal']);
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";
		}
		</script>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _jadwal_bayar WHERE id_jadwal_bayar = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);

			$query_detail = $db->sql("SELECT * FROM _jadwal_bayar_detail WHERE id_jadwal_bayar = '$_REQUEST[id]'");
			while($result_detail = $db->fetch_assoc($query_detail)){
				$_SESSION['s_jenis_bayar'][] 	= $result_detail['id_jenis_bayar'];
				$_SESSION['s_nominal'][] 		= $result_detail['nominal'];
			}
		}
		?>		
		<div class="container-fluid">
		<form name="form_jadwal_bayar" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo $resultEdit['id_jadwal_bayar']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-3 has-id">
					<label>Nomor</label>
					<input type="text" name="id_jadwal_bayar" id="id_jadwal_bayar" value="<?php echo $resultEdit['id_jadwal_bayar']; ?>" autocomplete="off" class="form-control input-sm" readonly style="text-align: center; font-weight: bold;" />
				</div>

				<div class="form-group col-xs-3 has-tingkat">
					<label>Tingkat</label>
					<select class="form-control input-sm" name="id_tingkat" id="id_tingkat" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=process&proc=periode&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'load-periode'); " <?php if($resultEdit['id_jadwal_bayar'] != '') echo "disabled"; ?>>
						<?php
							$query_tingkat = $db->sql("SELECT * FROM _tingkat");
							while($result_tingkat = $db->fetch_assoc($query_tingkat)){
								echo "<option value='$result_tingkat[id_tingkat]' "; if($result_tingkat['id_tingkat'] == $resultEdit['id_tingkat']) echo "selected"; echo " >$result_tingkat[id_tingkat] (".$func->terbilang($result_tingkat['id_tingkat']).")</option>";
							}
						?>
					</select>
				</div>
			</div>

			<div class="row">
				<span id="load-periode"></span>
				<div class="form-group col-xs-3 has-tahun">
					<label>Periode</label>
					<select class="form-control input-sm" name="tahun" id="tahun" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=process&proc=periode&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'load-periode'); " <?php if($resultEdit['id_jadwal_bayar'] != '') echo "disabled"; ?>>
						<option value="">-Pilih Tahun-</option>
						<?php
							for($tahun = date("Y"); $tahun >= date("Y")-2; $tahun--){
								echo "<option value='$tahun' "; if($tahun == $resultEdit['tahun']) echo "selected"; echo " >$tahun</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group col-xs-3 has-bulan">
					<label>&nbsp;</label>
					<select class="form-control input-sm" name="bulan" id="bulan" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=jadwal_bayar&action=process&proc=periode&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'load-periode'); " <?php if($resultEdit['id_jadwal_bayar'] != '') echo "disabled"; ?>>
						<option value="">-Pilih Bulan-</option>
						<?php
							for($bulan = 1; $bulan <= 12; $bulan++){
								echo "<option value='".$func->number_pad($bulan,2)."' "; if($func->number_pad($bulan,2) == $resultEdit['bulan']) echo "selected"; echo " >".$func->nama_bulan($bulan,2)."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group col-xs-3 has-tanggal">
					<label>&nbsp;</label>
					<select class="form-control input-sm" name="tanggal" id="tanggal" <?php if($resultEdit['id_jadwal_bayar'] != '') echo "disabled"; ?>>
						<option value="">-Pilih Tgl-</option>
						<?php
							for($tanggal = 1; $tanggal <= 31; $tanggal++){
								echo "<option value='".$func->number_pad($tanggal,2)."' "; if($func->number_pad($tanggal,2) == $resultEdit['tanggal']) echo "selected"; echo " >".$func->number_pad($tanggal,2)."</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12" id="detail">
					<?php include("detail.php"); ?>
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_jadwal_bayar;
							var err = '';
							if(obj.tahun.value==''){ $('.has-tahun').addClass('has-error').focus(); err+='<li>Tahun harus di isi</li>'; }
							if(obj.bulan.value==''){ $('.has-bulan').addClass('has-error').focus(); err+='<li>Bulan harus di isi</li>'; }
							if(obj.tanggal.value==''){ $('.has-tanggal').addClass('has-error').focus(); err+='<li>Tanggal harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=jadwal_bayar&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_jadwal_bayar.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>
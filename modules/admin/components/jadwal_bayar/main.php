<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=jadwal_bayar&add=false";
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = @$_REQUEST['menu'];
	list($access) = $db->result_row("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			list($levelx, $menusx, $iconx, $linkx, $parentx) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i' ");
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$parent = $parentx;
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo $menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
		
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">&nbsp;</h3>
						<div class="box-tools">
							<div class="input-group">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal" onclick="javascript: document.getElementById('iframe_jadwal_bayar').src='content.php?module=admin&component=jadwal_bayar&action=add'; " <?php if($func->akses('C') == false){ echo "disabled"; } ?>><i class="fa fa-plus"></i> Tambah data</button>
								</div>
								<div class="input-group-btn"></div>
								<select class="form-control input-sm" name="id_tingkat" id="id_tingkat" onchange="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'list', 'div');">
									<option value="">-Pilih Tingkat-</option>
									<?php
										$query_tingkat = $db->sql("SELECT * FROM _tingkat");
										while($result_tingkat = $db->fetch_assoc($query_tingkat)){
											echo "<option value='$result_tingkat[id_tingkat]' "; if($result_tingkat['id_tingkat'] == $resultEdit['id_tingkat']) echo "selected"; echo " >$result_tingkat[id_tingkat] (".$func->terbilang($result_tingkat['id_tingkat']).")</option>";
										}
									?>
								</select>
								<div class="input-group-btn"></div>
								<select class="form-control input-sm" name="tahun" id="tahun" onchange="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'list', 'div');">
									<option value="">-Pilih Tahun-</option>
									<?php
										for($tahun = date("Y"); $tahun >= date("Y")-2; $tahun--){
											echo "<option value='$tahun' "; if($tahun == $resultEdit['tahun']) echo "selected"; echo " >$tahun</option>";
										}
									?>
								</select>
								<div class="input-group-btn"></div>
								<select class="form-control input-sm" name="bulan" id="bulan" onchange="javascript: sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true&id_tingkat='+document.getElementById('id_tingkat').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'list', 'div');">
									<option value="">-Pilih Bulan-</option>
									<?php
										for($bulan = 1; $bulan <= 12; $bulan++){
											echo "<option value='".$func->number_pad($bulan,2)."' "; if($func->number_pad($bulan,2) == $resultEdit['bulan']) echo "selected"; echo " >".$func->nama_bulan($bulan,2)."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</div>
					
					<div id="list"><?php include "list.php"; ?></div>
				</div>
			</div>
		</div>
	</section>
		
	<div class="modal fade" id="myModal" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Input <?php echo $menus; ?></h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_jadwal_bayar" id="iframe_jadwal_bayar" width="98%" height="500" frameborder="0"></iframe>
				</div>
				<div class="modal-footer">
					<label class="pull-left">
						<input type="checkbox" name="cbadd" id="cbadd" value="1" />
						Tambah data lagi.
					</label>
					<button class="btn btn-primary" onclick="javascript: 
					var iframe = document.getElementById('iframe_jadwal_bayar');
					iframe.contentDocument.getElementById('save').click();  
					"><i class="fa fa-save"></i> Simpan</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismiss"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<input type="hidden" data-toggle="modal" data-target="#myModalChoose" id="bntModalChoose" />
	<div class="modal fade" id="myModalChoose" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Silahkan Pilih</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_choose" id="iframe_choose" width="98%" height="80" frameborder="0"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>
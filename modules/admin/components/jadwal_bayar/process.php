<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_jadwal_bayar 	= @$_POST['id_jadwal_bayar'];
		$id_tingkat 		= @$_POST['id_tingkat'];
		$tahun 				= @$_POST['tahun'];
		$bulan 				= @$_POST['bulan'];
		$tanggal 			= @$_POST['tanggal'];
		
		$hqData = $db->insert("_jadwal_bayar", array('id_jadwal_bayar' => $id_jadwal_bayar, 'id_tingkat' => $id_tingkat, 'tahun' => $tahun, 'bulan' => $bulan, 'tanggal' => $tanggal));

		$i = 0;
		if(count($_SESSION['s_jenis_bayar']) > 0){
			foreach($_SESSION['s_jenis_bayar'] as $id_jenis_bayar){
				$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];
				$hqData = $db->insert("_jadwal_bayar_detail", array('id_jadwal_bayar' => $id_jadwal_bayar, 'id_jenis_bayar' => $id_jenis_bayar, 'nominal' => $nominal));

				$i++;
			}
		}

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_jadwal_bayar", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jadwal_bayar&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 				= @$_POST['txtidx'];
		$id_jadwal_bayar 	= @$_POST['id_jadwal_bayar'];
		
		
		# --------------#
		# Proses Update #
		# --------------#

		$db->delete("_jadwal_bayar_detail", array('id_jadwal_bayar' => $idx));

		$i = 0;
		if(count($_SESSION['s_jenis_bayar']) > 0){
			foreach($_SESSION['s_jenis_bayar'] as $id_jenis_bayar){
				$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];
				$hqData = $db->insert("_jadwal_bayar_detail", array('id_jadwal_bayar' => $id_jadwal_bayar, 'id_jenis_bayar' => $id_jenis_bayar, 'nominal' => $nominal));

				$i++;
			}
		}
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jadwal_bayar&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jadwal_bayar&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_jadwal_bayar", array('id_jadwal_bayar' => $idx));
		$hqData = $db->delete("_jadwal_bayar_detail", array('id_jadwal_bayar' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_jadwal_bayar", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=jadwal_bayar&action=list&ajax=true");
		
		#---------- * ----------#
		break;

	case "periode":
		$id_tingkat = $_REQUEST['id_tingkat'];
		$tahun 		= $_REQUEST['tahun'];
		$bulan 		= $_REQUEST['bulan'];

		list($check_periode) = $db->result_row("SELECT COUNT(id_jadwal_bayar) FROM _jadwal_bayar WHERE id_tingkat = '$id_tingkat' AND tahun = '$tahun' AND bulan = '$bulan'");

		if($check_periode > 0){
			echo "
				<script>
					document.getElementById('bulan').value='';
					document.getElementById('id_jadwal_bayar').value='$tahun';
					alert('Mohon maaf periode tahun $tahun dan bulan ".$func->nama_bulan($bulan)." untuk tingkat $id_tingkat sudah ada!');
				</script>
			";
		}else{
			echo "
				<script>
					document.getElementById('id_jadwal_bayar').value='$tahun$bulan$id_tingkat';
				</script>
			";
		}
		break;

	case "nominal":
		$id_jenis_bayar = $_REQUEST['id_jenis_bayar'];
		list($nominal) = $db->result_row("SELECT nominal FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$id_jenis_bayar'");

		if(@$nominal > 0){
			$nominal = $nominal;
		}else{
			list($nominal) = $db->result_row("SELECT nominal FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id_jenis_bayar'");
		}

		echo "<script>document.getElementById('nominal_detail').value='$nominal';</script>";
		break;
}
?>
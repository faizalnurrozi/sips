<?php
	$proc = $_REQUEST['proc'];
	switch($proc){
		case "add":
			$detail_jenis_bayar = $_POST['detail_jenis_bayar'];
			$nominal_detail 	= $_POST['nominal_detail'];

			$_SESSION['s_jenis_bayar'][] 	= $detail_jenis_bayar;
			$_SESSION['s_nominal'][] 		= $nominal_detail;
		break;

		case "update":
			$posisi				= $_POST['posisi'];
			$detail_jenis_bayar = $_POST['detail_jenis_bayar'];
			$nominal_detail 	= $_POST['nominal_detail'];

			$_SESSION['s_jenis_bayar'][$posisi] = $detail_jenis_bayar;
			$_SESSION['s_nominal'][$posisi] 	= $nominal_detail;
		break;

		case "delete":
			$posisi = $_POST['posisi'];

			unset($_SESSION['s_jenis_bayar'][$posisi]);
			unset($_SESSION['s_nominal'][$posisi]);

			$_SESSION['s_jenis_bayar'] 	= array_values($_SESSION['s_jenis_bayar']);
			$_SESSION['s_nominal'] 		= array_values($_SESSION['s_nominal']);
		break;
	}
?>

<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th width="50%">Detail</th>
			<th>Nominal</th>
			<th width="20%">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
		<?php
			$no = 1; $i = 0;
			$total_nominal = 0;
			if(count($_SESSION['s_jenis_bayar']) > 0){
				foreach($_SESSION['s_jenis_bayar'] as $jenis_bayar){
					$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];

					if($proc == 'edit' & $_REQUEST['posisi'] == $i){

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>";
						echo "		<input type='text' name='detail_jenis_bayar_edit' id='detail_jenis_bayar_edit' placeholder='Detail jenis bayar ...' value='$jenis_bayar' class='form-control input-sm'>";
						echo "	</td>";
						echo "	<td>";
						echo "		<input type='number' name='nominal_detail_edit' id='nominal_detail_edit' placeholder='...' value='$nominal' class='form-control input-sm' style='text-align: right;'>";
						echo "	</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-sm' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jenis_bayar_khusus&action=detail&proc=update&posisi=$i&detail_jenis_bayar='+document.getElementById('detail_jenis_bayar_edit').value+'&nominal_detail='+document.getElementById('nominal_detail_edit').value, 'detail'); \">&nbsp;<i class='fa fa-save'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}else{

						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>$jenis_bayar</td>";
						echo "	<td align='right'>Rp. ".number_format($nominal,0,',','.')."</td>";
						echo "	<td align='center'>";
						echo "		<div class='btn-group btn-group-sm' role='group' aria-label='...'>";
						echo "			<button type='button' class='btn btn-sm btn-default btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jenis_bayar_khusus&action=detail&proc=edit&posisi=$i', 'detail'); \">&nbsp;<i class='fa fa-edit'></i>&nbsp;</button>";
						echo "			<button type='button' class='btn btn-sm btn-danger btn-flat' onclick=\"javascript: ajaxCustom('content.php', 'module=admin&component=jenis_bayar_khusus&action=detail&proc=delete&posisi=$i', 'detail'); \">&nbsp;<i class='fa fa-minus'></i>&nbsp;</button>";
						echo "		<div>";
						echo "	</td>";
						echo "</tr>";

					}

					$total_nominal += $nominal;

					$no++; $i++;
				}
			}
		?>
		<tr>
			<td align="center"><?php echo $no; ?></td>
			<td>
				<input type="text" name="detail_jenis_bayar" id="detail_jenis_bayar" class="form-control input-sm" placeholder="Detail jenis bayar ..." />
			</td>
			<td>
				<input type="number" name="nominal_detail" id="nominal_detail" class="form-control input-sm" placeholder="..." style="text-align: right;" />
			</td>
			<td align="center">
				<button type="button" class="btn btn-sm btn-default btn-flat" onclick="javascript: ajaxCustom('content.php', 'module=admin&component=jenis_bayar_khusus&action=detail&proc=add&detail_jenis_bayar='+document.getElementById('detail_jenis_bayar').value+'&nominal_detail='+document.getElementById('nominal_detail').value, 'detail'); ">&nbsp;<i class="fa fa-plus"></i>&nbsp;</button>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><b>Total Nominal</b></td>
			<td align="right"><b>Rp. <?php echo number_format($total_nominal,0,',','.') ?></b></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
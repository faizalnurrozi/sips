<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php
		list($jenis_bayar_khusus) = $db->result_row(" SELECT jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$id' ");
		?>
		<table class="table" style="font-size: 12px;">
			<tr>
				<td width="20%">Jenis Bayar Khusus</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $jenis_bayar_khusus; ?></td>
			</tr>
		</table>

		<table class="table table-bordered table-hover table-striped" style="font-size: 13px;">
			<thead>
				<tr>
					<th width="1%">No.</th>
					<th width="50%">Detail</th>
					<th>Nominal</th>
				</tr>
			</thead>

			<tbody>
				<?php
					$total_nominal = 0;
					$no = 1;
					$query_detail = $db->sql("SELECT * FROM _jenis_bayar_khusus_detail WHERE id_jenis_bayar_khusus = '$_REQUEST[id]'");
					while($result_detail = $db->fetch_assoc($query_detail)){
						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>$result_detail[jenis_bayar]</td>";
						echo "	<td align='right'>Rp. ".number_format($result_detail['nominal'],0,',','.')."</td>";
						echo "</tr>";

						$total_nominal += $result_detail['nominal'];
						$no++;

					}
				?>

				<tr>
					<td colspan="2" align="right"><b>Total Nominal</b></td>
					<td align="right"><b>Rp. <?php echo number_format($total_nominal,0,',','.') ?></b></td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
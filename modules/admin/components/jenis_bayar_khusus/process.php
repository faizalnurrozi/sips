<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 	= strtoupper(uniqid());
		$nama 	= @$_POST['txtnama'];

		$i = 0;
		$total_nominal = 0;
		if(count($_SESSION['s_jenis_bayar']) > 0){
			foreach($_SESSION['s_jenis_bayar'] as $jenis_bayar){
				$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];
				$total_nominal += $nominal;
				$hqData = $db->insert("_jenis_bayar_khusus_detail", array('id_jenis_bayar_khusus' => $id, 'jenis_bayar' => $jenis_bayar, 'nominal' => $nominal));

				$i++;
			}
		}
		
		$hqData = $db->insert("_jenis_bayar_khusus", array('id_jenis_bayar_khusus' => $id, 'jenis_bayar_khusus' => $nama, 'nominal' => $total_nominal));

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_jenis_bayar_khusus", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_bayar_khusus&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_bayar_khusus&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 	= @$_POST['txtidx'];
		$nama 	= @$_POST['txtnama'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_jenis_bayar_khusus", array('jenis_bayar_khusus' => $nama, 'nominal' => $total_nominal), array('id_jenis_bayar_khusus' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#


		$i = 0;
		$total_nominal = 0;
		$db->delete("_jenis_bayar_khusus_detail", array('id_jenis_bayar_khusus' => $idx));
		if(count($_SESSION['s_jenis_bayar']) > 0){
			foreach($_SESSION['s_jenis_bayar'] as $jenis_bayar){
				$nominal = ($_SESSION['s_nominal'][$i] == '') ? 0 : $_SESSION['s_nominal'][$i];
				$total_nominal += $nominal;
				$hqData = $db->insert("_jenis_bayar_khusus_detail", array('id_jenis_bayar_khusus' => $idx, 'jenis_bayar' => $jenis_bayar, 'nominal' => $nominal));

				$i++;
			}
		}
		
		$hqData = $db->update("_jenis_bayar_khusus", array('jenis_bayar_khusus' => $nama, 'nominal' => $total_nominal), array('id_jenis_bayar_khusus' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_bayar_khusus&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_bayar_khusus&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_jenis_bayar_khusus", array('id_jenis_bayar_khusus' => $idx));
		$hqData = $db->delete("_jenis_bayar_khusus_detail", array('id_jenis_bayar_khusus' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_jenis_bayar_khusus", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=jenis_bayar_khusus&action=list&ajax=true");
		
		#---------- * ----------#
		break;
}
?>
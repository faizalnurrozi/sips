<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php
		$hasil = $db->result_assoc(" SELECT * FROM _semester_ajaran WHERE id_semester_ajaran = '$id' ");
		?>
		<table class="table">
			<tr>
				<td width="20%">Kode&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['id_semester_ajaran']; ?></td>
			</tr>
			<tr>
				<td>Th.&nbsp;Ajaran&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['tahun_ajaran']; ?></td>
			</tr>
			<tr>
				<td>Semester&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['semester']; ?></td>
			</tr>
			<tr>
				<td>Tgl.&nbsp;Pelaksanaan&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->implode_date($hasil['tanggal_awal']); ?> - <?php echo $func->implode_date($hasil['tanggal_akhir']); ?></td>
			</tr>
		</table>
	</body>
</html>
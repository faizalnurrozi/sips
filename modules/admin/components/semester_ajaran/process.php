<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 	= @$_POST['txtid'];
		$tahunajaran = @$_POST['txttahunajaran'];
		$semester	 = @$_POST['rdsemester'];
		$tglawal	 = $func->explode_date(@$_POST['txttanggalawal']);
		$tglakhir	 = $func->explode_date(@$_POST['txttanggalakhir']);
		
		$hqData = $db->insert("_semester_ajaran", 
			array(
				'id_semester_ajaran' => $id, 
				'tahun_ajaran' => $tahunajaran,
				'semester'	=>	$semester,
				'tanggal_awal'	=>	$tglawal,
				'tanggal_akhir'	=>	$tglakhir
			)
		);
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_semester_ajaran", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=semester_ajaran&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=semester_ajaran&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 	= @$_POST['txtidx'];
		$id 	= @$_POST['txtid'];
		$tahunajaran = @$_POST['txttahunajaran'];
		$semester	 = @$_POST['rdsemester'];
		$tglawal	 = $func->explode_date(@$_POST['txttanggalawal']);
		$tglakhir	 = $func->explode_date(@$_POST['txttanggalakhir']);
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_semester_ajaran", 
			array(
				'id_semester_ajaran' => $id, 
				'tahun_ajaran' => $tahunajaran,
				'semester'	=>	$semester,
				'tanggal_awal'	=>	$tglawal,
				'tanggal_akhir'	=>	$tglakhir
			), array('id_semester_ajaran' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_semester_ajaran", 
			array(
				'id_semester_ajaran' => $id, 
				'tahun_ajaran' => $tahunajaran,
				'semester'	=>	$semester,
				'tanggal_awal'	=>	$tglawal,
				'tanggal_akhir'	=>	$tglakhir
			), array('id_semester_ajaran' => $idx)
		);
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=semester_ajaran&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=semester_ajaran&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#	
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_semester_ajaran", array('id_semester_ajaran' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_semester_ajaran", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=semester_ajaran&action=list&ajax=true");
		
		#---------- * ----------#
		break;
}
?>
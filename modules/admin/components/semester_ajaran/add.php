<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<link href="includes/popup-calendar/dhtmlgoodies_calendar.css" rel="stylesheet">
		<script src="includes/popup-calendar/dhtmlgoodies_calendar.js"></script>
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";
		}
		</script>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _semester_ajaran WHERE id_semester_ajaran = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
		}
		?>		
		<div class="container-fluid">
		<form name="form_semester_ajaran" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo @$resultEdit['id_semester_ajaran']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-4 has-id">
					<label>Kode&nbsp;</label>
					<input type="text" name="txtid" id="txtid" value="<?php echo @$resultEdit['id_semester_ajaran']; ?>" autocomplete="off" class="form-control input-sm" style="width:150px" />
				</div>
				<div class="form-group col-xs-8 has-nama">
					<label>Th.&nbsp;Ajaran&nbsp;</label>
					<input type="text" name="txttahunajaran" id="txttahunajaran" value="<?php echo @$resultEdit['tahun_ajaran']; ?>" autocomplete="off" class="form-control input-sm" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-4 has-semester">
					<label>Semester&nbsp;</label>
					<br/>
					<input type="radio" name="rdsemester" id="rdsemesterganjil" value="GANJIL" <?php if(@$resultEdit['semester']=='GANJIL') echo "checked"; ?> />&nbsp;GANJIL
					<input type="radio" name="rdsemester" id="rdsemestergenap" value="GENAP" <?php if(@$resultEdit['semester']=='GENAP') echo "checked"; ?> />&nbsp;GENAP
				</div>
				<div class="form-group col-xs-3 has-tgl-awal">
					<label>Tgl.&nbsp;Awal&nbsp;</label>
					<input type="text" name="txttanggalawal" id="txttanggalawal" value="<?php if(@$_REQUEST['id']==''){ echo date("d/m/Y"); }else{ echo $func->implode_date(@$resultEdit['tanggal_awal']); } ?>" autocomplete="off" class="form-control input-sm" onfocus="javascript: displayCalendar(document.getElementById('txttanggalawal'), 'dd/mm/yyyy', this);" readonly />
				</div>
				<div class="form-group col-xs-3 has-tgl-akhir">
					<label>Tgl.&nbsp;Akhir&nbsp;</label>
					<input type="text" name="txttanggalakhir" id="txttanggalakhir" value="<?php if(@$_REQUEST['id']==''){ echo date("d/m/Y"); }else{ echo $func->implode_date(@$resultEdit['tanggal_akhir']); } ?>" autocomplete="off" class="form-control input-sm" onfocus="javascript: displayCalendar(document.getElementById('txttanggalakhir'), 'dd/mm/yyyy', this);" readonly />
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_semester_ajaran;
							var err = '';
							if(obj.txtid.value==''){ $('.has-id').addClass('has-error').focus(); err+='<li>Kode harus di isi</li>'; }
							if(obj.txttahunajaran.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Th. Ajaran harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=semester_ajaran&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_semester_ajaran.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>
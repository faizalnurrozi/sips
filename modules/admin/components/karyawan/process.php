<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	/* -- Add Data -- */
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$nip 		= @$_POST['txtnip'];
		$nama 		= @$_POST['txtnama'];
		$tempatlahir= @$_POST['txttempatlahir'];
		$tanggallahir= $func->explode_date(@$_POST['txttanggallahir']);
		$pass_temp	= str_replace('/', '', $_POST['txttanggallahir']);
		$password 	= md5(strrev($pass_temp));
		$jk 		= @$_POST['jk'];
		$alamat 	= @$_POST['txtalamat'];
		$agama 		= @$_POST['hdagama'];
		$txttelp 	= @$_POST['txttelp'];
		$jabatan	= @$_POST['hdjabatan'];
		
		$tujuan = "";
		switch(@$_POST['statusinput']){
			case 'file' :
				$asal 		= (@$_FILES['txtfoto']['tmp_name']);
				$tujuan 	= "./modules/admin/photos/".uniqid("img")."-".(@$_FILES['txtfoto']['name']);		
				move_uploaded_file($asal, $tujuan);
				break;
			case 'camera' :
				$tujuan	= @$_POST['isifoto'];
				break;
			default :
				$tujuan = "";
				break;
		}
				
		$txtpt = @$_POST['txtpt'];		
		#---------- * ----------#		
		
		
		# --------------#
		# Proses Insert #
		# --------------#
		$hqData = $db->insert("_karyawan",
			array(
				'nip'		=> 	$nip, 
				'nama'		=>	$nama, 
				'passwords'	=>	$password, 
				'tempat_lahir'	=>	$tempatlahir, 
				'tanggal_lahir'	=>	$tanggallahir, 
				'jenis_kelamin'	=>	$jk, 
				'id_agama'	=>	$agama, 
				'id_jabatan'=>	$jabatan, 
				'alamat'	=>	$alamat, 
				'telepon'	=>	$txttelp, 
				'foto'		=>	$tujuan, 
				'pendidikan_terakhir'	=> $txtpt, 
				'user_admin'	=>	'FALSE', 
				'status_aktif'	=>	'AKTIF'
			) 
		);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_karyawan", $nip, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=karyawan&action=add' />";
		
		#---------- * ----------#
	break;
	/* -- End Add data -- */
	
	/* -- Update Data -- */
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$nipx 		= @$_POST['txtidx'];
		$nip 		= @$_POST['txtnip'];
		$nama 		= @$_POST['txtnama'];
		$password 	= @$_POST['txtpass'];
		$tempatlahir= @$_POST['txttempatlahir'];
		$tanggallahir= $func->explode_date(@$_POST['txttanggallahir']);
		$jk 		= @$_POST['jk'];
		$alamat 	= @$_POST['txtalamat'];
		$agama 		= @$_POST['hdagama'];
		$txttelp 	= @$_POST['txttelp'];
		$jabatan	= @$_POST['hdjabatan'];
		$txtpt 		= @$_POST['txtpt'];
		$cekfoto	= @$_POST['cekfoto'];
		
		if($cekfoto=='1'){
			list($foto) = $db->result_row("SELECT foto FROM _karyawan WHERE nip = '$nipx'");
			if(file_exists($foto) && $foto != ''){
				unlink($foto);
			}
			$tujuan = "";
			switch(@$_POST['statusinput']){
				case 'file' :
					$asal 		= (@$_FILES['txtfoto']['tmp_name']);
					$tujuan 	= "modules/admin/photos/".uniqid("img")."-".(@$_FILES['txtfoto']['name']);		
					move_uploaded_file($asal, $tujuan);
					break;
				case 'camera' :
					$tujuan	= @$_POST['isifoto'];
					break;
			}
		}	
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_karyawan",
			array(
				'nip'		=>	@$nip,
				'nama'		=>	@$nama,
				'tempat_lahir'	=>	@$tempatlahir,
				'tanggal_lahir'	=>	@$tanggallahir,
				'jenis_kelamin'	=>	@$jk,
				'alamat'	=>	@$alamat,
				'id_agama'	=>	@$agama,
				'telepon'	=>	@$txttelp,
				'pendidikan_terakhir'	=>	@$txtpt,
				'id_jabatan'	=>	@$jabatan
			),
			array('nip' => @$nipx), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_karyawan", 
			array(
				'nip'	=>	$nip,
				'nama'	=>	$nama,
				'tempat_lahir'	=>	$tempatlahir,
				'tanggal_lahir'	=>	$tanggallahir,
				'jenis_kelamin'	=>	$jk,
				'alamat'		=>	$alamat,
				'id_agama'		=>	$agama,
				'telepon'		=>	$txttelp,
				'pendidikan_terakhir'	=>	$txtpt,
				'id_jabatan'	=>	$jabatan
			),
			array('nip' => $nipx)
		);
		if($password != ''){ 
			$hqData = $db->update("_karyawan", array('passwords' => $func->encrypt_md5($password)), array('nip' => $nipx)); 
		}
		if($cekfoto == '1'){
			$hqData = $db->update("_karyawan", array('foto' => $tujuan), array('nip' => $nipx));
		}
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=karyawan&action=add' />";
		
		#---------- * ----------#
	break;
	/* -- End Update Data -- */
	
	/* -- Delete data -- */
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#		
		$idx = @$_REQUEST['id'];		
		#---------- * ----------#
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		list($foto)	= $db->result_row("SELECT foto FROM _karyawan WHERE nip = '$idx'");		
		if(file_exists("./modules/admin/photos/".$foto) && $foto != ''){
			unlink("./modules/admin/photos/".$foto);
		}
		$hqData = $db->delete("_admin", array('nip' => $idx));
		$hqData = $db->delete("_karyawan", array('nip' => $idx));
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";		
		#---------- * ----------#		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_karyawan", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=karyawan&action=list&ajax=true");
		
		#---------- * ----------#
		
	break;
	/* -- End delete data -- */
	
	/* Tambahkan Sebagai User Admin */
	case 'add_admin' :
		$username	= @$_POST['txtuser'];
		$pass	 	= @$_POST['txtpass'];
		$nip		= @$_POST['txtnip'];
		$group		= @$_POST['hdgroup'];
		$superuser	= (@$_POST['cbsuperuser']=='TRUE') ? 'TRUE' : 'FALSE';
		
		$db->update("_karyawan", array('user_admin' => 'TRUE'), array('nip' => $nip));				
		
		list($fotoAdmin) = $db->result_row("SELECT foto FROM _karyawan WHERE nip = '$nip' ");
		$_SESSION[_APP_.'s_fotoAdmin'] = $fotoAdmin;
		
		$hqData = $db->insert("_admin", 
			array(
				'usernames' => $username, 
				'nip' => $nip, 
				'passwords' => $func->encrypt_md5($pass), 
				'superuser' => $superuser, 
				'icon' => $fotoAdmin,
				'id_admin_group' => $group
			)
		);
		
		$func->activity_logs_insert("_admin", $username, $_SESSION[_APP_.'s_userAdmin']);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Pengaktifan user system <b>$username</b> berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Pengaktifan user gagal";
		//echo "<script>window.parent.location.reload();</script>";
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true', 'list', 'div'); </script>";
		//echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin&action=add' />";
	break;
	/* End Tambahkan Sebagai User Admin */
	
	/* Update Sebagai User Admin */
	case 'update_admin' :
		$username	= @$_POST['txtuser'];
		$id 		= @$_POST['txtid'];
		$group		= @$_POST['hdgroup'];
		$cek		= @$_POST['cbcek'];
		$superuser	= (@$_POST['cbsuperuser']=='TRUE') ? 'TRUE' : 'FALSE';
		
		$hqData = $db->update("_admin", 
			array(
				'usernames' => $username,
				'id_admin_group' => $group, 
				'superuser' => $superuser
			), array('usernames' => $id)
		);
		if($cek == '1'){ 
			$hqData = $db->update("_admin", array('passwords' => $func->encrypt_md5(@$_POST['txtpass'])), array('usernames' => $id));
		}
		list($nip) = $db->result_row("SELECT nip FROM _admin WHERE usernames = '$id' ");
		$hqData = $db->update("_karyawan", array('user_admin' => 'TRUE'), array('nip' => $nip));

		$func->activity_logs_update("_admin",
			array(
				'usernames'	=> $username,
				'id_admin_group' => $group, 
				'superuser' => $superuser
			), array('usernames' => $id), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		$func->activity_logs_update("_karyawan",
			array(
				'user_admin' => 'TRUE'
			), array('nip' => $nip), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		//echo "<script>window.parent.location.reload();</script>";
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true', 'list', 'div'); </script>";
		//echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin&action=add' />";
	break;
	/* End Update Sebagai User Admin */
	
	/* Hapus Sebagai User Admin */
	case 'delete_admin' :	
		$nip = @$_REQUEST['id'];
		list($usernames) = $db->result_row("SELECT usernames FROM _admin WHERE nip = '$nip' ");
		$hqData = $db->delete("_admin", array('nip' => $nip));
		$hqData = $db->update("_karyawan", array('user_admin' => 'FALSE'), array('nip' => $nip));
		
		$func->activity_logs_delete("_admin", $nip, $_SESSION[_APP_.'s_userAdmin']);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "User sudah bukan Administrator";
		else $_SESSION[_APP_.'s_message_error'] = "Action gagal";
		header("location: content.php?module=admin&component=karyawan&action=list&ajax=true");
	break;
	/* End Hapus Sebagai User Admin */
	
	case 'cek_user' :
		$user = @$_REQUEST['user'];
		list($cUser) = $db->result_row("SELECT COUNT(*) FROM _admin WHERE usernames = '$user' ");
		if($cUser == '0'){ 
			echo "<i class='fa fa-check'></i>";
			echo "<script>document.getElementById('tombol').disabled=false;</script>";
		}else{
			echo "<i class='fa fa-remove'></i>";
			echo "<script>document.getElementById('tombol').disable=true;</script>";
		}
	break;
}
?>
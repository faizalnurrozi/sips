<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<div class="box-body table-responsive">
	<!-- Alert Process -->
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>
	<!-- Alert Process -->
</div>

<?php
	/*Sorting*/
	if($_POST['sort']=='reset'){
		$_SESSION[_APP_.'s_field_karyawan'] = "A.nip";
		$_SESSION[_APP_.'s_sort_karyawan'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}
	switch($_POST['field']){
		case 'A.nip' : $_SESSION[_APP_.'s_field_karyawan'] = "A.nip"; break;
		case 'A.nama' : $_SESSION[_APP_.'s_field_karyawan'] = "A.nama"; break;
		case 'B.nama' : $_SESSION[_APP_.'s_field_karyawan'] = "B.nama"; break;
		default : 
			if(!isset($_SESSION[_APP_.'s_field_karyawan'])){
				$_SESSION[_APP_.'s_field_karyawan'] = "A.nip";
			}
			break;
	}
	if(!isset($_SESSION[_APP_.'s_sort_karyawan'])){
		$_SESSION[_APP_.'s_sort_karyawan'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
	}else{
		switch($_POST['act']){
			case 'sort' :
				if($_SESSION[_APP_.'s_sort_karyawan'] == "ASC"){
					$_SESSION[_APP_.'s_sort_karyawan'] = "DESC";
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}else if($_SESSION[_APP_.'s_sort_karyawan'] == "DESC"){
					$_SESSION[_APP_.'s_sort_karyawan'] = "ASC";
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}
				break;
			case 'paging' :
				if($_SESSION[_APP_.'s_sort_karyawan'] == "ASC"){
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}else if($_SESSION[_APP_.'s_sort_karyawan'] == "DESC"){
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}
				break;
		}
	}
	/*End Sorting*/
	
	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];
	
	$keyword = @$_REQUEST['keyword'];	
	
	$qSQL = "
	SELECT A.nip, A.nama, B.nama AS jabatan, A.user_admin, A.jenis_kelamin
	FROM _karyawan AS A LEFT JOIN _jabatan AS B ON (A.id_jabatan=B.id_jabatan)
	WHERE ";
	$qSQL .= "(A.nama LIKE :key or A.nip LIKE :key or B.nama LIKE :key) 
	ORDER BY ".$_SESSION[_APP_.'s_field_karyawan']." ".$_SESSION[_APP_.'s_sort_karyawan'];
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th class="sort" width="10%" onclick="javascript: sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true&act=sort&field=A.nip', 'list', 'div');">NIP&nbsp;<?php if($_SESSION[_APP_.'s_field_karyawan'] == 'A.nip') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true&act=sort&field=A.nama', 'list', 'div');">Nama&nbsp;<?php if($_SESSION[_APP_.'s_field_karyawan'] == 'A.nama') echo $iconsort; ?></th>
				<th class="sort" width="20%" onclick="javascript: sendRequest('content.php', 'module=admin&component=karyawan&action=list&ajax=true&act=sort&field=B.nama', 'list', 'div');">Jabatan&nbsp;<?php if($_SESSION[_APP_.'s_field_karyawan'] == 'B.nama') echo $iconsort; ?></th>
				<th class="sort">JK&nbsp;</th>
				<th width="120">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='6' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='canter'>".$func->highlight($hasil['nip'], $keyword)."</td>";			
					echo "<td align='left'>".$func->highlight($hasil['nama'], $keyword)."</td>";			
					echo "<td align='left'>".(($hasil['jabatan']==NULL) ? '-' : $hasil['jabatan'])."</td>";
					switch($hasil['jenis_kelamin']){ 
						case 'L' : $gender = "Laki - laki"; break; 
						case 'P' : $gender = "Perempuan"; break; 
						default : $gender = ""; break;
					}
					echo "<td align='left'>".$gender."</td>";
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
					
					/* -- Akses View */
					if($func->akses('R') == true){
						echo "<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modalView' onclick=\"javascript:  document.getElementById('iframe_view').src='content.php?module=admin&component=karyawan&action=view&id=$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='View'><i class='fa fa-search'></i></button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i></button>";
					}
					/* -- End Akses View -- */
					
					/* -- Akses CRUD -- */
					if($func->akses('CRUD') == true){
						if($hasil['user_admin'] == 'FALSE'){
							/* -- Add Admin -- */
							echo "<a class='btn btn-mini btn-primary' data-toggle='modal' data-target='#myModalAdmin' onclick=\"javascript: document.getElementById('iframe_admin').src='content.php?module=admin&component=karyawan&action=admin&id=$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='Jadikan user'><i class='fa fa-user-md'></i></a>";
							echo "<a class='btn btn-mini btn-default' disabled onmouseover=\"$(this).tooltip();\" title='Hapus user'><i class='fa fa-unlock'></i></a>";
						}else{
							/* -- Edit Admin -- */
							echo "<a class='btn btn-mini btn-primary' data-toggle='modal' data-target='#myModalAdmin' onclick=\"javascript: document.getElementById('iframe_admin').src='content.php?module=admin&component=karyawan&action=admin&id=$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='Edit user' ><i class='fa fa-check'></i></a>";
							/* -- Hapus sebagai admin -- */
							echo "<a class='btn btn-mini btn-warning' data-toggle='modal' data-target='#modalRemove' onclick=\"javascript:  document.getElementById('idx').value='$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='Hapus user' ><i class='fa fa-unlock'></i></a>";
						}
					}else{
						echo "<button type='button' title='Jadikan user' class='btn btn-default' disabled ><i class='fa fa-user-md'></i></button>";
						echo "<button type='button' title='Hapus user' class='btn btn-default' disabled ><i class='fa fa-unlock'></i></button>";
					}
					/* -- End Akses CRUD -- */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#modalAdd' onclick=\"javascript: document.getElementById('iframe_karyawan').src='content.php?module=admin&component=karyawan&action=add&id=$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='Edit data'><i class='fa fa-edit'></i></button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i></button>";
					}
					/* End Akses menu edit */
						
					/* Akses menu Delete */
					if($func->akses('D') == true){
						echo "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#modalDelete' onclick=\"javascript:  document.getElementById('idx').value='$hasil[nip]'; \" onmouseover=\"$(this).tooltip();\" title='Hapus data'><i class='fa fa-trash'></i></button>";
					}else{
						echo "<button type='button' title='Delete' class='btn btn-default' disabled ><i class='fa fa-trash'></i></button>";
					}
					/* End Akses menu delete */
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=karyawan&action=list&ajax=true&start=".($start-$limit)."', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=karyawan&action=list&ajax=true&start=".($a*$limit)."', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=karyawan&action=list&ajax=true&start=".($start+$limit)."', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>


<!-- Modals -->
<input type="hidden" id="idx" value="" />
<div class="modal fade" id="modalDelete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=karyawan&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('idx').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalRemove" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Nonaktifkan user ini sebagai admin ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=karyawan&action=process&proc=delete_admin&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('idx').value, 'list', 'div');"><i class="fa fa-remove"></i> Proses</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
require_once 'includes/phpexcel/PHPExcel.php';

$filex = "template-master-data-karyawan.xls";

//echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

//echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Suwondo, S.Kom")
							 ->setLastModifiedBy("Suwondo")
							 ->setTitle("Templates")
							 ->setSubject("Templates")
							 ->setDescription("File Template.")
							 ->setKeywords("Template")
							 ->setCategory("Karyawan");
$bold = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Times'
	)
);			
$normal = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Times'
	)
);

$rowHeader = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Times'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
);

$rowBody = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Times'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
);

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

/*** Sheet Utama ***/
$objPHPExcel->setActiveSheetIndex(0)->setTitle('Data Karyawan');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:M1");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Template Master Data Karyawan.");

$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($rowHeader);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'NIP')->getColumnDimension('A')->setWidth(10); 
cellColor('A3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', 'NAMA')->getColumnDimension('B')->setWidth(30); 
cellColor('B3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'TMP.LAHIR')->getColumnDimension('C')->setWidth(15); 
cellColor('C3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'TGL.LAHIR')->getColumnDimension('D')->setWidth(15); 
cellColor('D3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', 'JNS.KELAMIN')->getColumnDimension('E')->setWidth(15); 
cellColor('E3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', 'AGAMA')->getColumnDimension('F')->setWidth(10); 
cellColor('F3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G3', 'ALAMAT')->getColumnDimension('G')->setWidth(30); 
cellColor('G3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H3', 'TELEPON')->getColumnDimension('H')->setWidth(10); 
cellColor('H3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I3', 'PEND.TERAKHIR')->getColumnDimension('I')->setWidth(30);
cellColor('I3', 'E8DFDA');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J3', 'JABATAN')->getColumnDimension('J')->setWidth(15); 
cellColor('J3', 'E8DFDA');

$objPHPExcel->getActiveSheet()->getStyle('D')->getNumberFormat()->setFormatCode('yyyy-mm-dd hh:mm:ss');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A4", "10371");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B4", "AFIYATUL DAWAIYAH");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C4", "BANJARMASIN");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D4", date("Y-m-d"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E4", "P");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F4", "1");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G4", "Jl. ANTASARI 17 BANJARMASIN");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H4", "0893473483748");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I4", "UNIV. LAMBUNG MANGKURAT");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J4", "SPV");

$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A5", "10372");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B5", "KEVIN APRILIO");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C5", "JAKARTA");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D5", date("Y-m-d"));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E5", "L");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F5", "2");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G5", "Jl. JAWA 16 BANJARMASIN");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H5", "08564646872");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I5", "UNIV. INDONESIA");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J5", "SPV");
$objPHPExcel->getActiveSheet()->getStyle('A1:J5')->applyFromArray($rowBody);
/*** End Sheet Utama ***/

/*** Sheet untuk Petunjuk Pengisian ***/
$objWorkSheet = $objPHPExcel->createSheet(1);
$objPHPExcel->setActiveSheetIndex(1)->setTitle('Petunjuk Pengisian');
$objPHPExcel->setActiveSheetIndex(1)->mergeCells("A1:M1");
$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', "Petunjuk pengisian Master Data Karyawan.");
/*Petunjuk 1.*/
$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A3', "1.   Kolom \"Jenis Kelamin\" diisi dengan P untuk Perempuan, L untuk Laki - laki.");
/*Petunjuk 2.*/
$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A4', "2.   Kolom \"Agama\" diisi dengan KODE AGAMA sebagai berikut :");

$objPHPExcel->setActiveSheetIndex(1)->setCellValue("B5", "KODE AGAMA"); cellColor("B5", "E8DFDA");
$objPHPExcel->setActiveSheetIndex(1)->setCellValue("C5", "AGAMA"); cellColor("C5", "E8DFDA");
$row1=6;
$qAgama = $db->sql("SELECT id_agama, nama FROM _agama ORDER BY id_agama ASC");
while($hasil = $db->fetch_assoc($qAgama)){	
	$objPHPExcel->setActiveSheetIndex(1)->setCellValue("B$row1", $hasil['id_agama']); 
	$objPHPExcel->setActiveSheetIndex(1)->setCellValue("C$row1", $hasil['nama']); 
	
	$row1++;
}

$objPHPExcel->setActiveSheetIndex(1)->getStyle("B5:C".($row1-1))->applyFromArray($rowBody);
/*Petunjuk 3.*/
$objPHPExcel->setActiveSheetIndex(1)->setCellValue("A".$row1, "3.   Untuk kolom telepon, supaya angka 0 didepan dapat tersimpan, tambahkan karakter ' (petik satu).");
/*Petunjuk 4.*/
$objPHPExcel->setActiveSheetIndex(1)->setCellValue("A".($row1+1), "4.   Kolom \"JABATAN\" diisi dengan KODE JABATAN sebagai berikut :");

$objPHPExcel->setActiveSheetIndex(1)->setCellValue("B".($row1+2), "KODE JABATAN"); cellColor("B".($row1+2), "E8DFDA");
$objPHPExcel->setActiveSheetIndex(1)->setCellValue("C".($row1+2), "JABATAN"); cellColor("C".($row1+2), "E8DFDA");
$row2=$row1+3;
$qAgama = $db->sql("SELECT id_jabatan, nama FROM _jabatan ORDER BY id_jabatan ASC");
while($hasil = $db->fetch_assoc($qAgama)){	
	$objPHPExcel->setActiveSheetIndex(1)->setCellValue("B$row2", "'".$hasil['id_jabatan']); 
	$objPHPExcel->setActiveSheetIndex(1)->setCellValue("C$row2", "'".$hasil['nama']); 
	
	$row2++;
}
$objPHPExcel->setActiveSheetIndex(1)->getStyle("B".($row1+2).":C".($row2-1))->applyFromArray($rowBody);
/*Petunjuk 5.*/
/*$objPHPExcel->setActiveSheetIndex(1)->setCellValue("A".$row2, "4.   Untuk kolom Status Karyawan digunakan untuk keperluan Report/Laporan mengelompokkan karyawan, default \"G\", \"S\" untuk kelompok Sales, \"M\" untuk kelompok Teknisi/Mekanik.");*/
/*** End Sheet untuk Petunjuk Pengisian ***/

$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$filex);
$objWriter->save('php://output');
?>
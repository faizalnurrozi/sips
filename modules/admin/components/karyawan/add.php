<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
		
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Typeahead.js -->
		<link rel="stylesheet" href="includes/bootstrap/css/typeahead.css">
		<script src="includes/bootstrap/js/bootstrap-typeahead.js"></script>
		<!-- End of Typeahead.js -->
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";			
		}
		
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>	
	</head>
	<body bgcolor="white">
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEdit = "SELECT * FROM _karyawan WHERE nip = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEdit);
			$resultEdit = $db->fetch_assoc($dataEdit);
			
			list($namaJabatan) = $db->result_row("SELECT nama FROM _jabatan WHERE id_jabatan = '$resultEdit[id_jabatan]'");
		}
		?>		
		<div class="container-fluid">
		<form name="form_karyawan" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if($_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo $resultEdit['nip']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-6 has-nip">
					<label>NIP</label>
					<input type="text" name="txtnip" id="txtnip" value="<?php echo $resultEdit['nip']; ?>" autocomplete="off" class="form-control input-sm" onkeyup="javascript: this.value=(this.value).toUpperCase();" maxlength="10" />
				</div>
				<div class="form-group col-xs-6 has-nama">
					<label>Nama Karyawan</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo $resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-pt">
					<label>Pend. Terakhir</label>
					<input type="text" name="txtpt" id="txtpt" value="<?php echo $resultEdit['pendidikan_terakhir']; ?>" autocomplete="off" class="form-control input-sm" />
				</div>
				<div class="form-group col-xs-6 has-ttl">
					<label class="col-xs-12" style="padding:0 !important;">Tempat, Tgl. Lahir</label>
					<div class="input-group col-xs-12">
						<span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
						<input type="text" name="txttempatlahir" id="txttempatlahir" value="<?php echo $resultEdit['tempat_lahir']; ?>" autocomplete="off" class="form-control input-sm col-xs-9" />
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="txttanggallahir" id="txttanggallahir" value="<?php  if($_REQUEST['id'] == '') echo "01/01/".(date("Y")-17); else echo $func->implode_date($resultEdit['tanggal_lahir']); ?>" class="form-control input-sm col-xs-3" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-jk">
					<label>Jenis Kelamin</label>
					<div class="input-group col-xs-12">
						<label class="label label-info"><input type="radio" name="jk" value="L" <?php if($resultEdit['jenis_kelamin']=='L' || false) echo "checked"; ?> checked />&nbsp;Laki - laki</label>&nbsp;&nbsp;
						<label class="label label-info"><input type="radio" name="jk" value="P" <?php if($resultEdit['jenis_kelamin']=='P') echo "checked"; ?> />&nbsp;Perempuan</label>
					</div>
				</div>
				<div class="form-group col-xs-6 has-jabatan">
					<label>Jabatan</label>
					<div class="input-group">
						<input id="hdjabatan" name="hdjabatan" type="text" class="hide" autocomplete="off" value="<?php echo $resultEdit['id_jabatan']; ?>" />
						<input id="txtjabatan" name="txtjabatan" type="text" class="col-md-12 form-control" placeholder="Pilih Jabatan ..." autocomplete="off" value="<?php echo $namaJabatan; ?>" onkeyup="javascript: autocompleteJabatan(); " />
						<span class="input-group-addon" style="cursor:pointer;" data-toggle="modal" data-target="#myModal" onclick="javascript: document.getElementById('iframe_jabatan').src='content.php?module=admin&component=karyawan&action=jabatan'; "><i class="fa fa-plus"></i></span>
					</div>

					<script>
						$(function() {
							function displayResult(item) {
								$('#hdjabatan').val(item.value);
							}
							$('#txtjabatan').typeahead({
								source: [
								<?php
									$query_jabatan = $db->sql("SELECT * FROM _jabatan");
									while($result_jabatan = $db->fetch_assoc($query_jabatan)){
										echo "{id: '$result_jabatan[id_jabatan]', name: '$result_jabatan[nama]'},";
									}
								?>
								],
								onSelect: displayResult
							});
						});
					</script>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-foto">
					<label>Foto</label>
					<div class="input-group col-xs-12">
						<?php
						if($_REQUEST['id']!=""){
							echo "<label><input type='checkbox' name='cekfoto' id='cekfoto' value='1' />&nbsp;Ganti Foto</label>";
						}
						?>
						<div class="btn-group btn-group-xs" role="group">							
							<button type="button" class="btn btn-primary" onclick="javascript: $('#txtfoto').click();"><b> <i class="fa fa-upload"></i> Browse</b></button>
							<button type="button" class="btn btn-warning" onclick="javascript: document.getElementById('statusinput').value='camera'; window.open('content.php?module=admin&component=karyawan&action=camera', '_blank', 'width=800, height=350, menubar=0, scrollbars=0');"> <b><i class="fa fa-camera"></i> Ambil Dari Webcam</b></button>
							
							
							<input type="hidden" name="statusinput" id="statusinput" value="0" />
							<input type="file" name="txtfoto" id="txtfoto" class="input-mini hide" style="width:150px;" style="display:inline-block;" onclick="javascript: document.getElementById('statusinput').value='file';" />
							<input type="hidden" name="isifoto" id="isifoto" />
							<input type="image" name="imgfoto" id="imgfoto" <?php if($resultEdit['foto']!=""){ echo "src='$resultEdit[foto]' value='$resultEdit[foto]' style='width:80px;'"; }else{ echo "style='display:none;width:80px;'"; } ?> />
						</div>
					</div>
				</div>
				<div class="form-group col-xs-6 has-foto">
					<label>Agama</label>
					<div class="input-group col-xs-12">
						<select class="form-control" name="hdagama" id="hdagama">
							<option option="">-- Pilih Agama --</option>
							<?php
								$query_agama = $db->sql("SELECT * FROM _agama");
								while($result_agama = $db->fetch_assoc($query_agama)){
							?>
								<option value="<?php echo $result_agama['id_agama']; ?>" <?php if($result_agama['id_agama'] == $resultEdit['id_agama']){ echo "selected"; } ?>><?php echo $result_agama['nama']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-alamat">
					<label>Alamat</label>
					<div class="input-group col-xs-12">
						<textarea name="txtalamat" id="txtalamat" class="form-control input-sm"><?php echo $resultEdit['alamat']; ?></textarea>
					</div>
				</div>
				<div class="form-group col-xs-6 has-telepon">
					<label>Telepon</label>
					<div class="input-group col-xs-12">
						<input type="text" name="txttelp" id="txttelp" class="form-control input-sm" value="<?php echo $resultEdit['telepon']; ?>" />
					</div>
				</div>
			</div>

			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_karyawan;
							var err = '';
							if(obj.txtnip.value==''){ $('.has-nip').addClass('has-error').focus(); err+='<li>NIP harus di isi</li>'; }
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama karyawan harus di isi</li>'; }
							if(obj.txttelp.value==''){ $('.has-telepon').addClass('has-error').focus(); err+='<li>Telp. harus di isi</li>'; }
							if(err==''){ obj.action='content.php?module=admin&component=karyawan&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_karyawan.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<div class="modal fade" id="myModal" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Jabatan</h4>
					</div>
					<div class="modal-body">
						<iframe name="iframe_jabatan" id="iframe_jabatan" width="98%" height="80" frameborder="0" onload="javascript: autoResize('iframe_jabatan')"></iframe>
					</div>
					<div class="modal-footer">
						<button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="dismissButton"><i class="fa fa-remove"></i> Batal</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
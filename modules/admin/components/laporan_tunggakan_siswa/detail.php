<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";	
		$db = new Database();
		$func = new Functions();
	}

	$nis 	= @$_REQUEST['nis'];
	$tahun	= @$_REQUEST['tahun'];
	$bulan 	= @$_REQUEST['bulan'];

	/**
	 * Query table siswa berdasarkan NIS yang dikirim
	 */

	$query_siswa = $db->sql("SELECT A.*, B.nama AS kelas, B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '$nis'");
	$result_siswa = $db->fetch_assoc($query_siswa);
?>

<?php
	if($nis != ''){
?>
<div class="box-body table-responsive col-xs-8">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<td style="width: 30%">NIS</td>
				<td style="width: 2%" align="center">:</td>
				<td><?php echo $result_siswa['nis']; ?></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td align="center">:</td>
				<td><?php echo $result_siswa['nama']; ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td align="center">:</td>
				<td><?php echo ($result_siswa['jenis_kelamin'] == 'L') ? 'Laki - laki' : 'Perempuan'; ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td align="center">:</td>
				<td><?php echo $result_siswa['kelas']; ?></td>
			</tr>
		</thead>
	</table>

	<br>
	<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th>Periode</th>
				<th>Detail</th>
				<th>Kewajiban Bayar</th>
				<th>Nominal Bayar</th>
				<th>Sisa Tunggakan</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$no 			= 1;
				$total 			= 0;
				$total_bayar 	= 0;
				$total_sisa 	= 0;
				$query_item_umum = $db->sql("SELECT A.tahun, A.bulan, B.nominal, C.jenis_bayar AS jenis_bayar, B.id_jenis_bayar FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_umum AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_umum) WHERE id_tingkat = '$result_siswa[id_tingkat]' AND tahun = '$tahun' AND bulan LIKE '%$bulan%' ORDER BY A.tahun, A.bulan");
				while($result_item_umum = $db->fetch_assoc($query_item_umum)){
					
					list($cekBayar, $nominal_bayar) = $db->result_row("SELECT COUNT(A.id_pembayaran), SUM(B.nominal_bayar) FROM _pembayaran AS A INNER JOIN _pembayaran_detail AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE A.nis = '$nis' AND tahun = '$result_item_umum[tahun]' AND bulan = '$result_item_umum[bulan]' AND id_jenis_bayar = '$result_item_umum[id_jenis_bayar]'");
					
					$status = false;
					if($cekBayar > 0) $status = true;

					$sisa = $result_item_umum['nominal']-$nominal_bayar;

					$total += $result_item_umum['nominal'];
					$total_bayar += $nominal_bayar;
					$total_sisa += $sisa;


					echo "<tr>";
					echo "	<td align='center'>$no.</td>";
					echo "	<td>$result_item_umum[tahun] ".$func->nama_bulan($result_item_umum['bulan'])."</td>";
					echo "	<td>$result_item_umum[jenis_bayar]</td>";
					echo "	<td align='right'>Rp. ".number_format($result_item_umum['nominal'],0,',','.')."</td>";
					echo "	<td align='right'>Rp. ".number_format($nominal_bayar,0,',','.')."</td>";
					echo "	<td align='right'>Rp. ".number_format($sisa,0,',','.')."</td>";
					echo "	<td align='center'>".(($status) ? "<b style='color: green;'>Sudah Lunas</b>" : "<b style='color: #C30;'>Belum Lunas</b>")."</td>";
					echo "</tr>";

					$no++;
				}

				$query_item_khusus = $db->sql("SELECT A.tahun, A.bulan, B.nominal, C.jenis_bayar_khusus AS jenis_bayar, B.id_jenis_bayar FROM _jadwal_bayar AS A INNER JOIN _jadwal_bayar_detail AS B ON(A.id_jadwal_bayar = B.id_jadwal_bayar) INNER JOIN _jenis_bayar_khusus AS C ON(B.id_jenis_bayar = C.id_jenis_bayar_khusus) WHERE id_tingkat = '$result_siswa[id_tingkat]' AND tahun = '$tahun' AND bulan LIKE '%$bulan%' ORDER BY A.tahun, A.bulan");
				while($result_item_khusus = $db->fetch_assoc($query_item_khusus)){
					
					list($nominal_bayar, $status_dispensasi) = $db->result_row("SELECT SUM(B.nominal_bayar), status_dispensasi FROM _pembayaran AS A INNER JOIN _pembayaran_detail AS B ON(A.id_pembayaran = B.id_pembayaran) WHERE A.nis = '$nis' AND id_jenis_bayar = '$result_item_khusus[id_jenis_bayar]'");
					
					$status = false;
					if($status_dispensasi == 'TRUE' || $nominal_bayar >= $result_item_khusus['nominal']) $status = true;

					if($status == false || ($result_item_khusus['tahun'] == $tahun && $result_item_khusus['bulan'] == $bulan)){
						
						$sisa = $result_item_khusus['nominal']-$nominal_bayar;

						$total += $result_item_khusus['nominal'];
						$total_bayar += $nominal_bayar;
						$total_sisa += $sisa;


						echo "<tr>";
						echo "	<td align='center'>$no.</td>";
						echo "	<td>$result_item_khusus[tahun] ".$func->nama_bulan($result_item_khusus['bulan'])."</td>";
						echo "	<td>$result_item_khusus[jenis_bayar]</td>";
						echo "	<td align='right'>Rp. ".number_format($result_item_khusus['nominal'],0,',','.')."</td>";
						echo "	<td align='right'>Rp. ".number_format($nominal_bayar,0,',','.')."</td>";
						echo "	<td align='right'>Rp. ".number_format($sisa,0,',','.')."</td>";
						echo "	<td align='center'>".(($status) ? "<b style='color: green;'>Sudah Lunas</b>" : "<b style='color: #C30;'>Belum Lunas</b>")."</td>";
						echo "</tr>";

						$no++;
					}
				}
			?>
			<tr>
				<td colspan="3" align="right"><b>Total</b></td>
				<td align="right"><b>Rp. <?php echo number_format($total,0,',','.') ?></b></td>
				<td align="right"><b>Rp. <?php echo number_format($total_bayar,0,',','.') ?></b></td>
				<td align="right"><b>Rp. <?php echo number_format($total_sisa,0,',','.') ?></b></td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
</div>
<?php }else{ ?>

<div class="box-body table-responsive col-xs-7">
	<div class="alert alert-warning">
		Pilih siswa terlebih dahulu
	</div>
</div>

<?php } ?>
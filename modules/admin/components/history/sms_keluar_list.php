<?php
if($_REQUEST['ajax'] == 'true'){
	session_start();
	include "configs/config.php";
	include "configs/functions.php";
}
?>
<?php if(isset($_SESSION['s_message_sentitems'])){ ?>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Status : </strong> <?php echo $_SESSION['s_message_sentitems']; unset($_SESSION['s_message_sentitems']); ?>
</div>
<?php } ?>

<!-- List Cabang -->
<div class="well">
	<table class="table">
	<?php
	/*Sorting*/
	if($_POST['sort']=='reset'){
		$_SESSION['s_field_history_sms_keluar'] = "SendingDateTime";
		$_SESSION['s_sort_history_sms_keluar'] = "DESC";
		$iconsort = "<img src='images/sortup.gif' />";
	}
	switch($_POST['field']){
		case 'SendingDateTime' : $_SESSION['s_field_history_sms_keluar'] = "SendingDateTime"; break;
		case 'UpdatedInDB' : $_SESSION['s_field_history_sms_keluar'] = "UpdatedInDB"; break;
		case 'DestinationNumber' : $_SESSION['s_field_history_sms_keluar'] = "DestinationNumber"; break;
		default : 
			if(!isset($_SESSION['s_field_history_sms_keluar'])){
				$_SESSION['s_field_history_sms_keluar'] = "SendingDateTime"; 
			}
			break;
	}
	if(!isset($_SESSION['s_sort_history_sms_keluar'])){
		$_SESSION['s_sort_history_sms_keluar'] = "DESC";
		$iconsort = "<img src='images/sortup.gif' />";
	}else{
		switch($_POST['act']){
			case 'sort' :
				if($_SESSION['s_sort_history_sms_keluar'] == "ASC"){
					$_SESSION['s_sort_history_sms_keluar'] = "DESC";
					$iconsort = "<img src='images/sortdown.gif' />";
				}else if($_SESSION['s_sort_history_sms_keluar'] == "DESC"){
					$_SESSION['s_sort_history_sms_keluar'] = "ASC";
					$iconsort = "<img src='images/sortup.gif' />";
				}
				break;
			case 'paging' :
				if($_SESSION['s_sort_history_sms_keluar'] == "ASC"){
					$iconsort = "<img src='images/sortup.gif' />";
				}else if($_SESSION['s_sort_history_sms_keluar'] == "DESC"){
					$iconsort = "<img src='images/sortdown.gif' />";
				}
				break;
		}
	}
	/*End Sorting*/
	?>
	<tr class="table-list-header">
		<th width="5%">No.</th>
		<th width="20%" class="sort" onclick="javascript: sendRequest('history_sms_keluar_list.php', 'ajax=true&act=sort&field=SendingDateTime', 'list_keluar', 'div');">Tanggal&nbsp;Kirim&nbsp;<?php if($_SESSION['s_field_history_sms_keluar'] == 'SendingDateTime') echo $iconsort; ?></th>
		<th width="20%" class="sort" onclick="javascript: sendRequest('history_sms_keluar_list.php', 'ajax=true&act=sort&field=UpdatedInDB', 'list_keluar', 'div');">Tanggal&nbsp;Terkirim&nbsp;<?php if($_SESSION['s_field_history_sms_keluar'] == 'UpdatedInDB') echo $iconsort; ?></th>
		<th width="15%" class="sort" onclick="javascript: sendRequest('history_sms_keluar_list.php', 'ajax=true&act=sort&field=DestinationNumber', 'list_keluar', 'div');">Ke&nbsp;<?php if($_SESSION['s_field_history_sms_keluar'] == 'DestinationNumber') echo $iconsort; ?></th>
		<th>Pesan</th>
	</tr>
	<?php
	if($_POST['start']=='') $start = 0; else $start = $_POST['start'];
	
	$keyword = $_POST['keyword'];
	$qSQL = "SELECT SendingDateTime, UpdatedInDB, DestinationNumber, TextDecoded FROM sentitems WHERE (DestinationNumber LIKE '%$keyword%' OR TextDecoded LIKE '%$keyword%') ORDER BY ".$_SESSION['s_field_history_sms_keluar']." ".$_SESSION['s_sort_history_sms_keluar'];
	$hqSQL = mysql_query($qSQL);
	$totalData = mysql_num_rows($hqSQL);
	$qSQL	.= " LIMIT $start, $limit";
	$hqSQL = mysql_query($qSQL);
	$totalLimit = mysql_num_rows($hqSQL);
	
	if($totalData=='0'){
		echo "<tr><td colspan='5' align='center'>Data belum ada</td></tr>";
	}else{
		$no = 1;
		while($hasil = mysql_fetch_array($hqSQL)){
			echo "<tr class='table-list-row'>";
			echo "<td align=\"center\" valign='top'>".$no.".</td>";
			echo "<td align='center' valign='top'>".ReportDateTime($hasil[0])."</td>";
			echo "<td align='center' valign='top'>".ReportDateTime($hasil[1])."</td>";
			list($nm) = mysql_fetch_row(mysql_query("SELECT Name FROM pbk WHERE Number = '".$hasil[2]."' OR Number = '+62".$hasil[2]."' OR Number = '0".$hasil[2]."'")); 
			if($nm == ""){ 
				$nama = "(Belum Ada Nama)";
				$ke = $hasil[2];
			}else{ 
				$nama = $nm;
				$ke = telepon($hasil[2])." (".$nm.") ";
			}
			echo "<td align='center' valign='top'>".HighLight($ke, $keyword)."</td>";
			echo "<td>".HighLight($hasil[3], $keyword)."</td>";			
			echo "</tr>";
			
			$no++;
		}
	}
	?>
</table>
	<input type="hidden" id="id_delete" value="" />
</div>
<div class="pagination">
    <ul>
	<?php
	if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=history&action=sms_keluar_list&ajax=true&start=".($start-$limit)."', 'list', 'div');\">Prev</a></li>";
	$jumlahPage = $totalData/$limit;
	for($a=0;$a<$jumlahPage;$a++){
		$x = $a * $limit;
		if($start==$a*$limit){
			echo "<li><span style='background-color:#eee;' >".($a+1)."</span></li>";
		}else{
			echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=history&action=sms_keluar_list&ajax=true&start=".($a*$limit)."', 'list', 'div');\">".($a+1)."</a></li>";
		}
	}
	 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=history&action=sms_keluar_list&ajax=true&start=".($start+$limit)."', 'list', 'div');\">Next</a></li>";
	?>
    </ul>
</div>
<?php 
echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
?>
<!-- End of list history_sms -->
<?php 
session_start(); 
include "globals/config.php";
include "globals/functions.php";
?>
<html>
	<head>
		<!-- JQuery dan AJAX-->
		<script type="text/javascript" src="includes/ajax.js"></script>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<link href="includes/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="includes/jquery-ui.min.js"></script>
		<!-- End of JQuery dan AJAX -->
		
		<!-- popup window -->
		<link rel="stylesheet" href="includes/popup-window/subModal.css" media="screen"></link>
		<script type="text/javascript" src="includes/popup-window/common.js"></script>
		<script type="text/javascript" src="includes/popup-window/subModal.js"></script>
		<!-- End of popup window -->
		
		<!-- Admin CSS -->
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/theme.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap-adds.css"></link>
		<style type="text/css">
			.notif{
				background-color:magenta; width:20px; text-align:center; display:inline-block; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; color:#fff;
			}
		</style>
		<!-- End of Admin CSS -->
	</head>
	<body onload="javascript: $('#tabs').tabs();" onmouseover="javascript: $('#message').fadeOut(3000);">
		<?php
		/*Cek apakah ada SMS baru*/
		list($inbox) = mysql_fetch_row(mysql_query("SELECT COUNT(ID) FROM inbox WHERE ID NOT IN(SELECT id_inbox FROM inbox_reply)"));
		list($outbox) = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM outbox"));
		?>
		<div id="tabs">
			<ul>
				<li><a href="#inbox"><label>SMS&nbsp;Masuk&nbsp;<?php if($inbox!=0){ echo "<div id='inbox_new' class='notif'>$inbox</div>"; } ?></label></a></li>
				<li><a href="#outbox"><label>SMS&nbsp;Pending&nbsp;<?php if($outbox!=0){ echo "<div id='outbox_new' class='notif'>$outbox</div>"; } ?></label></a></li>
				<li><a href="#received"><label>SMS&nbsp;Terkirim</label></a></li>
			</ul>
			<div id="inbox">				
				<h4>SMS MASUK</h4>
				<div class="search-well">
					<form class="form-inline" name="f_cari_masuk" action="javascript: void(null);">
						<input class="input-xlarge" placeholder="Pencarian data..." id="appendedInputButton" type="text" name="x_keyword" value="<?php if(isset($_POST['keyword'])){ echo $_POST['keyword']; } ?>" autocomplete="off" onkeyup="javascript: /*if(event.keyCode=='13')*/ sendRequest('content.php', 'module=admin&component=history&action=sms_masuk_list&ajax=true&keyword='+this.value, 'list_masuk', 'div');" style="width:300px;" />
                    <button class="btn" type="button" onClick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_masuk_list&ajax=true&keyword='+document.f_cari_masuk.x_keyword.value, 'list_masuk', 'div');"><i class="icon-search"></i> Go</button>
					<a style="text-decoration:none;" onclick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_masuk_list&ajax=true', 'list_masuk', 'div'); document.f_cari_masuk.x_keyword.value=''; document.f_cari_masuk.x_keyword.focus();">Reset</a>
					</form>
				</div>
				<p style="line-height:1px;margin-top:-5px;">
					<a class="btn" onclick="javascript: if(confirm('Bersihkan history sms masuk?')) sendRequest('content.php', 'module=admin&component=history&action=sms_process&proc=delete_inbox', 'list_masuk', 'div');"><i class="icon-remove"></i>&nbsp;Bersihkan&nbsp;history&nbsp;SMS&nbsp;masuk</a>
				</p>
				<p style="line-height:1px;margin-top:-5px;">&nbsp;</p>
				<div id="list_masuk"><?php include "sms_masuk_list.php"; ?></div>
				<p class="spacer">&nbsp;</p>
			</div>
			
			<div id="outbox">
				<h4>SMS PENDING</h4>
				<div class="search-well">
					<form class="form-inline" name="f_cari_pending" action="javascript: void(null);">
						<input class="input-xlarge" placeholder="Pencarian data..." id="appendedInputButton" type="text" name="x_keyword" value="<?php if(isset($_POST['keyword'])){ echo $_POST['keyword']; } ?>" autocomplete="off" onkeyup="javascript: /*if(event.keyCode=='13')*/ sendRequest('content.php', 'module=admin&component=history&action=sms_pending_list&ajax=true&keyword='+this.value, 'list_pending', 'div');" style="width:300px;" />
                    <button class="btn" type="button" onClick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_pending_list&ajax=true&keyword='+document.f_cari_pending.x_keyword.value, 'list_pending', 'div');"><i class="icon-search"></i> Go</button>
					<a style="text-decoration:none;" onclick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_pending_list&ajax=true', 'list_pending', 'div'); document.f_cari_pending.x_keyword.value=''; document.f_cari_pending.x_keyword.focus();">Reset</a>
					</form>
				</div>
				<p style="line-height:1px;margin-top:-5px;">
					<a class="btn" style="cursor:pointer" onclick="javascript: if(confirm('Bersihkan history sms pending?')) sendRequest('content.php', 'module=admin&component=history&action=sms_process&proc=delete_outbox', 'list_pending', 'div');"><i class="icon-remove"></i> &nbsp;Bersihkan&nbsp;history&nbsp;SMS&nbsp;pending</a>
				</p>
				<p style="line-height:1px;margin-top:-5px;">&nbsp;</p>
				<div id="list_pending"><?php include "sms_pending_list.php"; ?></div>
				<p class="spacer">&nbsp;</p>
			</div>
		
			<div id="received">
				<h4>SMS TERKIRIM</h4>
				<div class="search-well">
				<form class="form-inline" name="f_cari_keluar" action="javascript: void(null);">
					<input class="input-xlarge" placeholder="Pencarian data..." id="appendedInputButton" type="text" name="x_keyword" value="<?php if(isset($_POST['keyword'])){ echo $_POST['keyword']; } ?>" autocomplete="off" onkeyup="javascript: /*if(event.keyCode=='13')*/ sendRequest('content.php', 'module=admin&component=history&action=sms_keluar_list&ajax=true&keyword='+this.value, 'list_keluar', 'div');" style="width:300px;" />
                    <button class="btn" type="button" onClick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_keluar_list&ajax=true&keyword='+document.f_cari_keluar.x_keyword.value, 'list_keluar', 'div');"><i class="icon-search"></i> Go</button>
					<a style="text-decoration:none;" onclick="javascript: sendRequest('content.php', 'module=admin&component=history&action=sms_keluar_list&ajax=true', 'list_keluar', 'div'); document.f_cari_keluar.x_keyword.value=''; document.f_cari_keluar.x_keyword.focus();">Reset</a>
				</form>
				<p style="line-height:1px;margin-top:-5px;">
					<a class="btn" style="cursor:pointer" onclick="javascript: if(confirm('Bersihkan history sms terkirim?')) sendRequest('content.php', 'module=admin&component=history&action=sms_process&proc=delete_sentitems', 'list_keluar', 'div');"><i class="icon-remove"></i>&nbsp;Bersihkan&nbsp;history&nbsp;SMS&nbsp;terkirim</a>
				</p>
				<p style="line-height:1px;margin-top:-5px;">&nbsp;</p>
				<div id="list_keluar"><?php include "sms_keluar_list.php"; ?></div>
				<p class="spacer">&nbsp;</p>
			</div>
		</div>
	</body>
</html>
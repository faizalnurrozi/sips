<?php session_start(); ?>
<?php 	
include "globals/config.php";
include "globals/functions.php";

if(isset($_SESSION['s_userAdmin'])){
	$_SESSION['s_adminPage'] = "module=admin&component=history";
	?>
	<div class="header">           
		<h1 class="page-title">History SMS</h1>
	</div>
	<ul class="breadcrumb">
		<li><a onclick="javascript: sendRequest('content.php', 'module=admin&component=home', 'content', 'div');">Home</a> <span class="divider">/</span></li>
		<li class="active">History SMS</li>
	</ul>
	<div class="container-fluid">
		<div class="row-fluid">
			<div id="inputan">
				<iframe name="iframe_history" id="iframe_history" src="content.php?module=admin&component=history&action=sms<?php echo "#".$_REQUEST['href']; ?>" width="98%" height="500" frameborder="0" scrolling="auto"></iframe>
			</div>
			<p class="spacer">&nbsp;</p>
		</div>
	</div>
<?php 
}else{
	echo "<script language='javascript'>window.location.href='index.php';</script>";
}
?>
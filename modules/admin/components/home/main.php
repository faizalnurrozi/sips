<?php 	
include "globals/config.php";
include "globals/functions.php";

$db = new Database();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=home";
?>
<style type="text/css">
.rotate:hover{
	-webkit-transform: rotateZ(+360deg);	-ms-transform: rotateZ(+360deg);	transform: rotateZ(+360deg);
}	
.rotate{
	width:80px;		height:80px;	text-align:center;	-webkit-transition-duration: 0.2s;		
	-moz-transition-duration: 0.2s;	-o-transition-duration: 0.2s;	transition-duration: 0.2s;	
	-webkit-transition-property: -webkit-transform;	-moz-transition-property: -moz-transform;
	-o-transition-property: -o-transform;	transition-property: transform;	
	overflow:hidden;	/*background:#91EBB2;*/		margin:2px;
}
.rotate:active{
	transform: translateY(1px);
}
.rotate input[type=image]{
	margin-top:25px;	width:50px !important;	height:50px !important;	cursor:default !important;
}
h3{
	margin:0;	padding:10px;	margin-top:-15px;
}
</style>
<section class="content-header">
	<h1>Dashboard <small>Control panel</small></h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<?php
			$query = $db->sql("SELECT A.id_admin_icon_home, A.nama AS nama_shortcut, A.icon, B.nama AS nama_menu, B.link FROM _admin_icon_home AS A INNER JOIN _admin_menus AS B ON (A.id_admin_menus = B.id_admin_menus) WHERE A.id_admin_menus IN (SELECT id_admin_menus FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."') ORDER BY A.urutan ASC");
			while($result = $db->fetch_assoc($query)){
				if(file_exists($result['icon'])) $icon = $result['icon']; else $icon = "images/default.png";
		?>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 icon-home" onclick="<?php echo stripslashes($result['link']); ?>">
			<div class="icon-foto rotate">
				<img src="<?php echo $icon; ?>" title="<?php echo $result['nama_shortcut']; ?>" />
			</div>
			<div class="icon-nama">
				<a href="#" onclick="<?php echo stripslashes($result['link']); ?>"><?php echo $result['nama_shortcut']; ?></a>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>
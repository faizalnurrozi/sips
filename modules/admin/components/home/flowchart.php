<?php
include "globals/config.php";
include "globals/functions.php";
unset($_SESSION['s_imagesKendaraan']);
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;

			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}

			document.getElementById(id).height= (newheight) + "px";
			
		}
		
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>
	</head>
	<body bgcolor="white">
		<?php
		$qEditAdmin = "SELECT flowchart FROM _setting";
		$dataEdit = mysql_query($qEditAdmin);
		list($flowchart) = mysql_fetch_row($dataEdit);
		// echo $flowchart;
		// if(!file_exists($flowchart)) $flowchart = "images/default.png";
		?>
		<div class="container-fluid">
			<img src="<?php echo $flowchart; ?>" style="width:100%;" />
		</div>
		
	</body>
</html>
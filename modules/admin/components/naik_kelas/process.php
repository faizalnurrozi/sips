<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'pindah' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_kelas_asal 		= @$_POST['id_kelas_asal'];
		$id_kelas_tujuan 	= @$_POST['id_kelas_tujuan'];
		$data_siswa 		= explode('|', $_POST['data_siswa']);

		foreach($data_siswa as $siswa){
			if($siswa != ''){
				if($id_kelas_tujuan == 'LULUS'){
					$status = "LULUS";
					$db->update("_siswa", array("id_kelas" => "", "status" => "LULUS"), array("nis" => $siswa));
				}else{
					$status = "AKTIF";
					$db->update("_siswa", array("id_kelas" => $id_kelas_tujuan, "status" => "AKTIF"), array("nis" => $siswa));
				}
				$hqData = $db->insert("_siswa_history", array('nis' => $siswa, 'id_kelas_asal' => $id_kelas_asal, 'id_kelas_tujuan' => $id_kelas_tujuan, 'tahun' => date("Y"), 'status' => $status));
			}
		}

		$_SESSION[_APP_.'s_message_info'] = "Ubah data berhasil";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_siswa_history", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=naik_kelas&action=list&ajax=true&id_kelas=$id_kelas_tujuan&change=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=naik_kelas&action=add&id_kelas=$id_kelas_asal&data_siswa=$data_siswa' />";
		
		#---------- * ----------#
		
		break;
}
?>
<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";
		}
		</script>
	</head>
	<body>
		<?php
			$id_kelas	= $_REQUEST['id_kelas'];
			$data_siswa	= $_REQUEST['data_siswa'];

			$result_kelas = $db->result_assoc("SELECT * FROM _kelas WHERE id_kelas = '$id_kelas'");
		?>		
		<div class="container-fluid">
		<form name="form_siswa" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<input type="hidden" id="id_kelas_asal" name="id_kelas_asal" value="<?php echo $id_kelas; ?>" />
			<input type="hidden" id="data_siswa" name="data_siswa" value="<?php echo $data_siswa; ?>" />
			<input type="hidden" id="proc" name="proc" value="pindah" />
			<div class="row">
				<div class="form-group col-xs-8 has-id">
					<label>Kelas Sekarang</label>
					<input type="text" name="kelas_asal" id="kelas_asal" value="<?php echo ($result_kelas['nama'] == '') ? '-' : $result_kelas['nama']; ?>" readonly class="form-control input-sm" style="width:80px; text-align: center; font-weight: bold;" />
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-xs-8 has-kelas">
					<label>Naik ke Kelas/Pindah ke Kelas</label>
					<select class="form-control input-sm" id="id_kelas_tujuan" name="id_kelas_tujuan">
						<option value="">-Pilih Kelas-</option>
						<?php
							$query_kelas = $db->sql("SELECT * FROM _kelas ORDER BY nama ASC");
							while($result_kelas = $db->fetch_assoc($query_kelas)){
								echo "<option value='$result_kelas[id_kelas]'>$result_kelas[nama]</option>";
							}
						?>
						<option value="LULUS">* LULUS</option>
					</select>
				</div>
			</div>
			
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_siswa;
							var err = '';
							if(obj.id_kelas_tujuan.value==''){ $('.has-kelas').addClass('has-error').focus(); err+='<li>Kelas tujuan harus di isi</li>'; }

							if(err==''){
								if(confirm('Apakah anda yakin akan disimpan ?') == true){
									obj.action='content.php?module=admin&component=naik_kelas&action=process';
									obj.submit();
									
									if(window.top.document.getElementById('cbadd').checked == false){
										window.top.document.getElementById('dismiss').click();
									}
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_siswa.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>
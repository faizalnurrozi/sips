<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";	
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<div class="box-body table-responsive">
	<!-- Alert Process -->
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>
	<!-- Alert Process -->
</div>

<?php
	/*Sorting*/
	if($_POST['sort']=='reset'){
		$_SESSION[_APP_.'s_field_siswa'] = "nis";
		$_SESSION[_APP_.'s_sort_siswa'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}
	switch($_POST['field']){
		case 'A.nis' : $_SESSION[_APP_.'s_field_siswa'] = "A.nis"; break;
		case 'A.nama' : $_SESSION[_APP_.'s_field_siswa'] = "A.nama"; break;
		default : 
			if(!isset($_SESSION[_APP_.'s_field_siswa'])){
				$_SESSION[_APP_.'s_field_siswa'] = "nis";
			}
			break;
	}
	if(!isset($_SESSION[_APP_.'s_sort_siswa'])){
		$_SESSION[_APP_.'s_sort_siswa'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
	}else{
		switch($_POST['act']){
			case 'sort' :
				if($_SESSION[_APP_.'s_sort_siswa'] == "ASC"){
					$_SESSION[_APP_.'s_sort_siswa'] = "DESC";
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}else if($_SESSION[_APP_.'s_sort_siswa'] == "DESC"){
					$_SESSION[_APP_.'s_sort_siswa'] = "ASC";
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}
				break;
			case 'paging' :
				if($_SESSION[_APP_.'s_sort_siswa'] == "ASC"){
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}else if($_SESSION[_APP_.'s_sort_siswa'] == "DESC"){
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}
				break;
		}
	}
	/*End Sorting*/
	
	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];
	
	$keyword 	= @$_REQUEST['keyword'];
	$id_kelas 	= @$_REQUEST['id_kelas'];
	if($id_kelas == 'LULUS'){
		$qSQL = "SELECT * FROM _siswa AS A WHERE A.status = 'LULUS' AND (A.nis LIKE :key OR A.nama LIKE :key) ORDER BY ".$_SESSION[_APP_.'s_field_siswa']." ".$_SESSION[_APP_.'s_sort_siswa'];
	}else{
		$qSQL = "SELECT A.*, B.nama AS kelas FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE A.id_kelas = '$id_kelas' AND (A.nis LIKE :key OR A.nama LIKE :key OR B.nama LIKE :key) ORDER BY ".$_SESSION[_APP_.'s_field_siswa']." ".$_SESSION[_APP_.'s_sort_siswa'];
	}
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	/*$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);*/
?>

<?php 
	if(@$_REQUEST['change'] == 'true'){
		echo "<script>document.getElementById('id_kelas').value='$id_kelas';</script>";
	}
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" id="cball" onclick="javascript:
						var jumlahData = <?php echo $totalData; ?>;
						if(this.checked == true){
							for(i = 1; i <= jumlahData; i++){
								document.getElementById('cbnis'+i).checked=true;
							}
						}else{
							for(i = 1; i < jumlahData; i++){
								document.getElementById('cbnis'+i).checked=false;
							}
						}
					">
				</th>
				<th width="1%">No.</th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=naik_kelas&action=list&ajax=true&act=sort&field=A.nis&keyword=<?php echo $keyword; ?>', 'list', 'div');">NIS&nbsp;<?php if($_SESSION[_APP_.'s_field_siswa'] == 'nama') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=naik_kelas&action=list&ajax=true&act=sort&field=A.nama&keyword=<?php echo $keyword; ?>', 'list', 'div');">Siswa&nbsp;<?php if($_SESSION[_APP_.'s_field_siswa'] == 'nama') echo $iconsort; ?></th>
				<th>Kelas</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='5' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>";
					echo "	<input type='checkbox' name='cbnis' id='cbnis$no' value='".$hasil['nis']."'>";
					echo "</td>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['nis'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['nama'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight(($hasil['kelas'] == '') ? '-' : $hasil['kelas'], $keyword)."</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
	<?php if($id_kelas == ''){ ?>
	<button class="btn btn-success" onclick="javascript: alert('Kelas dipilih dahulu!'); "><i class="fa fa-refresh"></i> Pindah kelas/Naik Kelas</button>
	<?php }else{ ?>
	<button class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="javascript: 
		var data = '';
		for(i = 1; i < <?php echo $no; ?>; i++){
			if(document.getElementById('cbnis'+i).checked == true){
				data += document.getElementById('cbnis'+i).value+'|';
			}
		}
		document.getElementById('iframe_siswa').src='content.php?module=admin&component=naik_kelas&action=add&id_kelas='+document.getElementById('id_kelas').value+'&data_siswa='+data;
	"><i class="fa fa-refresh"></i> Pindah kelas/Naik Kelas</button>
	<?php } ?>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>1</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<!-- <div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=naik_kelas&action=list&ajax=true&keyword=$keyword&start=".($start-$limit)."', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=naik_kelas&action=list&keyword=$keyword&ajax=true&start=".($a*$limit)."', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=naik_kelas&action=list&keyword=$keyword&ajax=true&start=".($start+$limit)."', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div> -->
	</div>
</div>


<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300" frameborder="0" scrolling="no"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="id_delete" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=naik_kelas&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('id_delete').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->
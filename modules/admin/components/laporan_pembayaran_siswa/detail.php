<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";	
		$db = new Database();
		$func = new Functions();
	}

	$nis 	= @$_REQUEST['nis'];
	$tahun	= @$_REQUEST['tahun'];
	$bulan 	= @$_REQUEST['bulan'];

	/**
	 * Query table siswa berdasarkan NIS yang dikirim
	 */

	$query_siswa = $db->sql("SELECT A.*, B.nama AS kelas, B.id_tingkat FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE nis = '$nis'");
	$result_siswa = $db->fetch_assoc($query_siswa);
?>

<?php
	if($nis != ''){
?>
<div class="box-body table-responsive col-xs-8">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<td style="width: 30%">NIS</td>
				<td style="width: 2%" align="center">:</td>
				<td><?php echo $result_siswa['nis']; ?></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td align="center">:</td>
				<td><?php echo $result_siswa['nama']; ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td align="center">:</td>
				<td><?php echo ($result_siswa['jenis_kelamin'] == 'L') ? 'Laki - laki' : 'Perempuan'; ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td align="center">:</td>
				<td><?php echo $result_siswa['kelas']; ?></td>
			</tr>
		</thead>
	</table>

	<br>
	<table class="table table-bordered table-hover table-striped" style="font-size: 12px;">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th>Tanggal Bayar</th>
				<th>Detail Bayar</th>
				<th>Deskripsi</th>
				<th>Dispensasi</th>
				<th>Kewajiban Bayar</th>
				<th>Nominal Bayar</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$no 			= 1;
				$total_kewajiban= 0;
				$total_bayar 	= 0;

				$query_detail = $db->sql("SELECT * FROM _pembayaran AS A INNER JOIN _pembayaran_detail AS B ON(A.id_pembayaran = B.id_pembayaran)");
				while($resul_detail = $db->fetch_assoc($query_detail)){

					if($resul_detail['status_jenis_bayar'] == 'UMUM'){
						list($jenis_bayar) = $db->result_row("SELECT jenis_bayar FROM _jenis_bayar_umum WHERE id_jenis_bayar_umum = '$resul_detail[id_jenis_bayar]'");
						$deskripsi = $resul_detail['tahun'].' '.$func->nama_bulan($resul_detail['bulan']);
					}else{
						list($jenis_bayar) = $db->result_row("SELECT jenis_bayar_khusus FROM _jenis_bayar_khusus WHERE id_jenis_bayar_khusus = '$resul_detail[id_jenis_bayar]'");
						$deskripsi = "Angsuran ke $resul_detail[angsuran_ke]";
					}

					$total_kewajiban += $resul_detail['nominal_beban'];
					$total_bayar += $resul_detail['nominal_bayar'];


					echo "<tr>";
					echo "	<td align='center'>$no.</td>";
					echo "	<td>".$func->report_date($resul_detail['tanggal'])."</td>";
					echo "	<td>$jenis_bayar</td>";
					echo "	<td>$deskripsi</td>";
					echo "	<td align='center'>".(($resul_detail['status_dispensasi'] == 'TRUE') ? 'Ya' : 'Tidak')."</td>";
					echo "	<td align='right'>Rp. ".number_format($resul_detail['nominal_beban'],0,',','.')."</td>";
					echo "	<td align='right'>Rp. ".number_format($resul_detail['nominal_bayar'],0,',','.')."</td>";
					echo "</tr>";

					$no++;
				}
			?>
			<tr>
				<td colspan="5" align="right"><b>Total</b></td>
				<td align="right"><b>Rp. <?php echo number_format($total_kewajiban,0,',','.') ?></b></td>
				<td align="right"><b>Rp. <?php echo number_format($total_bayar,0,',','.') ?></b></td>
			</tr>
		</tbody>
	</table>
</div>
<?php }else{ ?>

<div class="box-body table-responsive col-xs-7">
	<div class="alert alert-warning">
		Pilih siswa terlebih dahulu
	</div>
</div>

<?php } ?>
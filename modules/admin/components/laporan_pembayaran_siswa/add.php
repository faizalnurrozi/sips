<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>

		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal').datepicker(options);
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_laporan_pembayaran_siswa" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-12 has-nis">
							<label>Siswa</label>
							<span id="load-siswa"></span>
							<div class="input-group col-xs-12">
								<input type="hidden" name="nis" id="nis" value="<?php echo $nis ?>" />
								<input type="text" name="nama_siswa" id="nama_siswa" class="form-control input-sm" placeholder="Pilih siswa ..." readonly value="<?php echo $nama_siswa ?>" />
								<span class="input-group-addon" style="cursor: pointer;" onclick="javascript: window.top.document.getElementById('iframe_choose').src='content.php?module=admin&component=laporan_pembayaran_siswa&action=siswa'; window.top.document.getElementById('bntModalChoose').click(); "><i class="fa fa-plus"></i></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-4">
							<label>Periode</label>
							<select class="form-control input-sm" name="tahun" id="tahun">
								<?php
									for($tahun_loop = date("Y"); $tahun_loop >= date("Y")-2; $tahun_loop--){
										echo "<option value='$tahun_loop' "; if($tahun_loop == $tahun) echo "selected"; echo " >$tahun_loop</option>";
									}
								?>
							</select>
						</div>
						<div class="form-group col-xs-4">
							<label>&nbsp;</label>
							<select class="form-control input-sm" name="bulan" id="bulan">
								<option value="">-Semua Bulan-</option>
								<?php
									for($bulan_loop = 1; $bulan_loop <= 12; $bulan_loop++){
										echo "<option value='".$func->number_pad($bulan_loop,2)."' "; if($func->number_pad($bulan_loop,2) == date("m")) echo "selected"; echo " >".$func->nama_bulan($bulan_loop,2)."</option>";
									}
								?>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<button class="btn btn-default" type="button" onclick="javascript: sendRequest('content.php', 'module=admin&component=laporan_pembayaran_siswa&action=detail&ajax=true&nis='+document.getElementById('nis').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'detail', 'div'); "><i class="fa fa-database"></i> Proses hitung</button>
							<!-- <button class="btn btn-success" type="button" onclick="javascript: window.open('content.php?module=admin&component=laporan_pembayaran_siswa&action=xls&ajax=true&id_kelas='+document.getElementById('id_kelas').value+'&id_jenis_bayar='+document.getElementById('id_jenis_bayar').value+'&tahun='+document.getElementById('tahun').value, '_blank'); "><i class="fa fa-file-o"></i> Export PDFs (.pdf)</button> -->
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12" id="detail">
							<?php include("detail.php"); ?>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').hide();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->		
	</body>
</html>
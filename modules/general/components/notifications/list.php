<?php
if($_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
?>

<div class="box-body table-responsive">
	<?php
	$qTanggal = $db->sql("SELECT DISTINCT DATE_FORMAT(tanggal, '%Y-%m-%d') FROM _notifikasi WHERE (usernames = '".$_SESSION[_APP_.'s_userAdmin']."' OR usernames = '".$_SESSION[_APP_.'s_userMahasiswa.']."' OR usernames = '".$_SESSION[_APP_.'s_userDosen']."') ORDER BY tanggal DESC");
	while(list($tanggal) = $db->fetch_row($qTanggal)){ 
	?>
	<legend style="margin:10px 0px 0px 10px;"><?php echo $func->search_day($tanggal).", ".$func->implode_date($tanggal); ?></legend>
	<table class="table table-bordered table-hover table-striped">	
		<tbody>
		<?php
		$qSQL = $db->sql("SELECT * FROM _notifikasi WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND (usernames = '".$_SESSION[_APP_.'s_userAdmin']."' OR usernames = '".$_SESSION[_APP_.'s_userMahasiswa.']."' OR usernames = '".$_SESSION[_APP_.'s_userDosen']."') ORDER BY id DESC");
		//$hqSQL = $db->fetch_assoc($qSQL);
		$totalData = $db->num_rows($qSQL);
		if($totalData=='0'){
			echo "<tr><td colspan='2' align='center'>Data belum ada</td></tr>";
		}else{
			$no = $start+1;
			while($hasil = $db->fetch_assoc($qSQL)){
				echo "<tr class='table-list-row'>";
				$warna = ($hasil['status_open']=='BELUM') ? "color:red;" : "";
				echo "<td align='center' width='20%' style='font-style:italic;font-size:10px;$warna'>".$func->search_day($hasil['tanggal']).", ".$func->implode_date($hasil['tanggal'])." ".substr($hasil['tanggal'],10).".</td>";
				echo "<td align='left'><a style='cursor:pointer;font-size:12px;$warna' onclick=\"javascript: sendRequest('content.php', 'module=general&component=notifications&action=process&proc=link&id=$hasil[id]', 'content', 'div');\">".$hasil['isi_notifikasi']."</a></td>";
				echo "</tr>";
				
				$no++;
			}
		}
		?>
		</tbody>
	</table>
	<?php
	}
	?>
</div>
<!-- End of Modals -->
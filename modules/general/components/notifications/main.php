<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
if(isset($_SESSION[_APP_.'s_userAdmin']) || isset($_SESSION[_APP_.'s_userMahasiswa']) || isset($_SESSION[_APP_.'s_userDosen'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=general&component=notifications&add=false";
	$_SESSION[_APP_.'s_mahasiswaPage'] = "module=general&component=notifications&add=false";
	$_SESSION[_APP_.'s_dosenPage'] = "module=general&component=notifications&add=false";
	?>
	
	<!-- Header & Breadcrumb -->
	<section class="content-header">
		<h1>Pemberitahuan</h1>
	</section>
	<!-- End Header & Breadcrumb -->
		
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">					
					<div id="list"><?php include "list.php"; ?></div>
				</div>
			</div>
		</div>
	</section>
<?php 
}else{
	include "modules/general/components/auth/timeout.php";
}
?>